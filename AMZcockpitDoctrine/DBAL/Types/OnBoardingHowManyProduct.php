<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class OnBoardingHowManyProduct extends AbstractEnumType
{
    const LT10 = 'LT10';
    const BETWEEN_10_50 = 'BETWEEN_10_50';
    const GT50 = 'GT50';

    protected static $choices = [
        self::LT10 => 'dbal.types.OnBoardingHowManyProduct.LT10',
        self::BETWEEN_10_50 => 'dbal.types.OnBoardingHowManyProduct.BETWEEN_10_50',
        self::GT50 => 'dbal.types.OnBoardingHowManyProduct.GT50'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
