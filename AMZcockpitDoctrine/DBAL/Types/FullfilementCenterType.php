<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class FullfilementCenterType extends AbstractEnumType
{
    protected static $choices = [
        'BER3' =>  'Amazon Brieselang Gmbh Havellandstr. 5 14656 Brieselang, Allemagne',
        'BER6' => 'Amazon Logistik AF München GmbH Am Borsigturm 100 13507 Berlin => Allemagne',
        'CGN1' => 'Amazon Koblenz GmbH Amazonstrasse 1 / Industriepark A61 56330 Kobern-Gondorf, Allemagne',
        'DTM1' => 'Amazon Logistik Werne GmbH Carl-Zeiss-Straße 3 59368 Werne, Allemagne',
        'DTM2' => 'Amazon Logistik Dortmund GmbH Kaltbandstraße 4 44145 Dortmund, Allemagne',
        'DUS2' => 'Amazon Fulfillment Germany GmbH Amazonstrasse 1 / Alte Landstrasse 47495 Rheinberg, Allemagne',
        'EDE4' => 'Amazon Logistik Werne GmbH Wahrbrink 25 59368 Werne, Allemagne',
        'EDE5' => 'Amazon Logistik Werne GmbH Wahrbrink 23 59368 Werne, Allemagne',
        'FRA1' => 'Amazon Logistik GmbH Am Schloss Eichhof 1 36251 Bad Hersfeld, Allemagne',
        'FRA3' => 'Amazon Logistik GmbH Amazonstrasse 1 Obere Kühnbach 36251 Bad hersfeld, Allemagne',
        'FRA7' => 'Amazon Logistik GmbH Frankenthal, Allemagne',
        'HAM2' => 'Amazon Logistik Winsen GmbH Borgwardstrasse 10 21423 Winsen an der Luhe , Allemagne',
        'LEJ1' => 'Amazon Distribution GmbH Amazonstrasse1  04347 Leipzig, Allemagne',
        'LEJ2' => 'Amazon Distribution GmbH Imaging Operations Friedrichshafner Straße 72a 04357 Leipzig, Allemagne',
        'MUC3' => 'Amazon Distribution GmbH Amazonstrasse1  / Zeppelinstrasse 2 86836 Graben, Allemagne',
        'STR1' => 'Amazon Pforzheim GmbH Amazonstrasse 1  (A8 Exit 44 PforzheimNord direction Bretten) 75177 Pforzheim, Allemagne' ,
        'XDEB' => 'XDEB DHL Aussenlager DHL Logistik-Center Ludwigsau Im Fuldatal 2 36251 Ludwigsau OT Mecklar, Allemagne',
        'XDEH' => 'XDEH Kühne + Nagel Außenlager Kühne + Nagel (AG & Co.) KG  Betriebsstätte Rennerod  Industriegebiet Alsberg  56477 Rennerod , Allemagne',
        'XDEI' => 'XDEI GEODIS Außenlager GEODIS Logistics Deutschland GmbH  Niederlassung Hamburg Bei der Lehmkuhle 2  21629 Neu WulmstorfMienenbuettel, Allemagne',
        'XDEJ' => 'XDEJ Baur Versand Außenlager  Baur Versand GmbH & Co KG  Siegfried-Lapawa-Straße 1 96242 Sonnefeld , Allemagne',
        'XDES' => 'Hermes Fulfilment GmbH Standort Loehne Schillenbrink 4 - 6 D-32584 Loehne , Allemagne',
        'XDET' => 'Amazon Deliveries XDET  Geodis Malsfeld NS 3PL Bornwiese 1 D-34323 Malsfeld, Allemagne',
        'XDEU' => 'Amazon Deliveries – XDEU DHL Solutions GmbH Barentsstrasse 24 53881 Euskirchen, Allemagne',
        'KTW1' => 'Amazon Fulfillment Poland sp. z o.o. ul. Inwestycyjna 19 Sosnowiec 41-208, Pologne',
        'POZ1' => 'POZ1 ul. Poznanska 1d 62 – 080 Sady, Pologne',
        'PRG2' => 'PRG2 K Amazonu 235 - 252 61  Dobrovíz', 'République Tchèque',
        'SZZ1' => 'Amazon Fulfillment Poland sp. z o.o. Kolbaskowo 156  72-001  Kolbaskowo, Pologne',
        'WRO1' => 'WRO1  ul. Czekoladowa 1 55 – 040 Bielany Wroclawskie, Pologne',
        'WRO2' => 'WRO2 ul. Logistyczna 6 55 – 040 Bielany Wroclawskie, Pologne',
        'WRO3' => 'WRO3  ul. Czekoladowa 1 55 – 040 Bielany Wroclawskie, Pologne',
        'BHX1' => 'Amazon Rugeley  - Goods In Towers Business Park Power Station Road Rugeley Staffordshire WS15 1NZ (SAT NAV : WS15 1PR), Royaume Uni',
        'BHX2' => 'Amazon Coalville -  Robson Way Ellistown COALVILLE LE67 1GQ, Royaume Uni',
        'BHX3' => 'Amazon UK Services Ltd - BHX3   Royal Oak Way North Daventry Northamptonshire NN118QL , Royaume Uni',
        'CWL1' => 'Amazon Swansea - Goods In Fford Amazon Crymlyn Burrows Swansea SA1 8QX, Royaume Uni',
        'EDI4' => 'Amazon Dunfermline - Goods In Amazon Way Dunfermline  KY11 8XT, Royaume Uni',
        'EUK5' => 'Amazon Peterborough - Goods In Phase 2 Kingston Park Flaxley Road Peterborough PE2 9EN, Royaume Uni',
        'GLA1' => 'Amazon Gourock - Goods In 2 Cloch Road Faulds Park Gourock Inverclyde PA19 1BQ, Royaume Uni',
        'LBA1' => 'Amazon Doncaster (LBA1) - Goods In Unit 1 Balby Carr Bank Balby Doncaster DN4 5JS, Royaume Uni',
        'LBA2' => 'Amazon  Unit 1 Iport Avenue New Rossington Doncaster DN11 0BG, Royaume Uni',
        'LBA3' => 'Amazon Doncaster (LBA3) - Goods In Unit 3 Water Vole Way Balby Doncaster DN4 5JP, Royaume Uni',
        'LCY1' => 'Amazon London (LCY1) - Goods In Unit B – Prologis Park Twelvetrees Crescent London E3 3JG, Royaume Uni',
        'LCY2' => 'Amazon Distribution Depot Unit 2 London Distribution Park Windrush Road Tilbury RM18 7AN, Royaume Uni',
        'LTN1' => 'Amazon Milton Keynes  - Goods In Marston Gate Fulfulfilment Centre Ridgemont Bedfordshire MK43 0ZA, Royaume Uni',
        'LTN2' => 'Amazon Hemel Hempstead - Goods In Boundary Way Hemel Hempstead Hertfordshire HP2 7LF, Royaume Uni',
        'LTN4' => 'Amazon Dunstable - Goods In Unit DC1 (Prologis)  Boscombe Road Dunstable  LU5 4FE, Royaume Uni',
        'MAN1' => 'Amazon Manchester -  6 Sunbank Lane  Airport City Manchester  Altrincham  M90 5AA, Royaume Uni',
        'MAN2' => 'Omega Plot 7c Orion Boulevard Great Sankey Warrington WA5 3XA, Royaume Uni',
        'XUKA' => 'XUKA - DHL Preston Brook (XUKA) Whitehouse Industrial Estate Aston Lane North Runcorn Cheshire WA7 3BN, Royaume Uni',
        'XUKC' => 'XUKC - Yusen Logistics UK Vendorflex  Rutherford Drive  Park Farm South  Wellingborough  NN8 6AQ, Royaume Uni',
        'XUKD' => 'XUKD - XPO Logistics UK Unit A Daventry Distribution Centre Royal Oak Way North Daventry NN11 8LR, Royaume Uni',
        'XUKK' => 'XUKK - Kuehne & Nagel Merlin Park II  Wood Lane  Birmingham B24 9QJ, Royaume Uni',
        'BVA1' => 'Amazon.fr Logistique SAS 1  Avenue du Superbe Orénoque ZAC Jules Vernes – Secteur Est 80440 BOVES, France',
        'LIL1' => 'Amazon EU SARL c/o Amazon.fr Logistique SAS 1 rue Amazon 59553 Lauwin Planque, France',
        'LYS1' => 'Amazon EU SARL  c/o Amazon.fr Logistique SAS  Distripole Chalon  ZAC Val de Bourgogne - 1 rue Amazon  71100 Sevrey, France',
        'MRS1' => 'AMAZON EU SARL Building II ZAC Les Portes de Provence 26200 MONTELIMAR, France',
        'ORY1' => 'AMAZON EU SARL Pole 45 1401 rue du Champ Rouge 45770 SARAN, France',
        'XFRE' => 'Geodis Logistics Satolas Parc des Chênes Nord 135 Rue du Brisson ZI St Quentin Fallavier Bâtiment "IDC9" 38290 Satolas-et-Bonce, France',
        'XFRF' => 'Entrepôt KUEHNE NAGEL Livraisons Amazon - XFRF Avenue Louis Renault Bâtiment 3 ZAC du Val Bréon 77610 Châtres en Brie, France',
        'XFRG' => 'Entrepôt XPO Logistics Livraisons Amazon - XFRG ZAC du Moulin 45410 Artenay, France',
        'XFRH' => 'Entrepôt XPO Logistics Livraisons Amazon - XFRH Parc d’activité de Chanteloup Bâtiment 8A 77550 Moissy Cramayel , France',
        'FCO1' => 'Amazon Italia Logistica S.R.L.  Via della Meccanica 4 Passo Corese 02032, Italie',
        'MXP3' => 'Amazon Italia Logistica S.R.L.  Via Rita Levi Montalcini 2 13100 Vercelli, Italie',
        'MXP5' => 'Amazon EU Sarl c/o Amazon Italia Logistica Srl Strada Dogana Po 2U 29015 Castel San Giovanni, Italie',
        'XITC' => 'Amazon XITC - Geodis Logistics SpA Via Aldo Moro 4  Francolino di Carpiano (MI) 20080, Italie',
        'XITD' => ' Amazon XITD - GEODIS Logistics Viale Maestri del Lavoro 990 45031 Arquà Polesine - RO, Italie',
        'BCN1' => 'Amazon Spain Fulfillment  S.L.  Avigunda De les Garrigues 6 - 8 El Prat de Llobregat  08820 Barcelona, Espagne',
        'BCN2' => 'Amazon Spain Fulfillment  S.L. Carrer De La Vernada  22 08107 Martorelles, Espagne',
        'BCN3' => 'Amazon Spain Fulfillment S.L Carrer Ferro 12  08755 Castellbisbal Barcelona, Espagne',
        'MAD4' => 'AMAZON EU SARL C/O Amazon Spain Fulfillment  S.L  Avenida De Astronomía 24 28830  San Fernando De Henares  Madrid , Espagne',
        'XESA' => 'XPO LOGISTICS  Avenida Río Henares 16  19208 Alovera Guadalajara, Espagne',
        'XESC' => 'Kuehne + Nagel Warehouse Amazon Deliveries – XESC Avenida de las Puntas 10  43120 Constantí – Tarragona, Espagne',
        'LGA9' => '2170 NJ-27, Edison, NJ 08817, États-Unis (USA)',
        'MDW7' => '6605 W Monee Manhattan Rd, Monee, IL 60449, États-Unis (USA)',
        'PHX3' => 'Phoenix, Arizona (USA)',
        'PHX5' => 'Phoenix, Arizona (USA)',
        'PHX6' => 'Phoenix, Arizona (USA)',
        'PHX7' => 'Phoenix, Arizona (USA)',
        'PHX8' => 'Phoenix, Arizona (USA)',
        'PHX9' => 'Phoenix, Arizona (USA)',
        'OAK3' => 'Oakland, Napa (USA)',
        'OAK4' => 'Oakland, Napa (USA)',
        'OAK5' => 'Oakland, Napa (USA)',
        'OAK6' => 'Oakland, Napa (USA)',
        'OAK7' => 'Oakland, Napa (USA)',
        'ONT2' => 'Ontario, Napa (USA)',
        'ONT3' => 'Ontario, Napa (USA)',
        'ONT4' => 'Ontario, Napa (USA)',
        'ONT5' => 'Ontario, Napa (USA)',
        'ONT6' => 'Ontario, Napa (USA)',
        'ONT8' => 'Ontario, Napa (USA)',
        'ONT9' => 'Ontario, Napa (USA)',
        'BDL1' => 'Windsor Locks, Connecticut (USA)',
        'PHL1' => 'Philadelphia, Mississippi (USA)',
        'PHL7' => 'Philadelphia, Mississippi (USA)',
        'PHL9' => 'Philadelphia, Mississippi (USA)',
        'MCO5' => 'Orlando, Florida (USA)',
        'MIA5' => 'Miami, Florida (USA)',
        'TPA1' => 'Tampa, Florida (USA)',
        'TPA2' => 'Tampa, Florida (USA)',
        'ATL6' => 'Atlanta, Georgia (USA)',
        'IND1' => 'Indianapolis, Indiana (USA)',
        'IND2' => 'Indianapolis, Indiana (USA)',
        'IND3' => 'Indianapolis, Indiana (USA)',
        'IND4' => 'Indianapolis, Indiana (USA)',
        'IND5' => 'Indianapolis, Indiana (USA)',
        'SDF8' => 'Louisville, Kentucky (USA)',
        'MCI5' => 'Kansas City, Kansas (USA)',
        'CVG1' => 'Cincinnati, Ohio (USA)',
        'CVG2' => 'Cincinnati, Ohio (USA)',
        'CVG3' => 'Cincinnati, Ohio (USA)',
        'CVG5' => 'Cincinnati, Ohio (USA)',
        'CVG7' => 'Cincinnati, Ohio (USA)',
        'CVG8' => 'Cincinnati, Ohio (USA)',
        'LEX1' => 'Lexington, Kentucky (USA)',
        'LEX2' => 'Lexington, Kentucky (USA)',
        'SDF1' => 'Louisville, Kentucky (USA)',
        'SDF2' => 'Louisville, Kentucky (USA)',
        'SDF4' => 'Louisville, Kentucky (USA)',
        'SDF6' => 'Louisville, Kentucky (USA)',
        'SDF7' => 'Louisville, Kentucky (USA)',
        'SDF9' => 'Louisville, Kentucky (USA)',
        'BOS5' => 'Boston, Massachusetts (USA)',
        'BWI2' => 'Baltimore, Maryland (USA)',
        'BWI5' => 'Baltimore, Maryland (USA)',
        'MSP5' => 'Minneapolis, Minnesota (USA)',
        'CLT5' => 'Charlotte, North Carolina (USA)',
        'BOS1' => 'Boston, Massachusetts (USA)',
        'ACY5' => 'Atlantic City, New Jersey (USA)',
        'EWR4' => 'Newark, Delaware (USA)',
        'EWR5' => 'Newark, Delaware (USA)',
        'EWR6' => 'Newark, Delaware (USA)',
        'EWR7' => 'Newark, Delaware (USA)',
        'EWR8' => 'Newark, Delaware (USA)',
        'LAS2' => 'Las Vegas, Nevada (USA)',
        'RNO4' => 'Reno, Nevada (USA)',
        'JFK7' => 'New York, New York (USA)',
        'ABE2' => 'Allentown, Pennsylvania (USA)',
        'ABE3' => 'Allentown, Pennsylvania (USA)',
        'MDT1' => 'Harrisburg, Illinois (USA)',
        'PHL4' => 'Philadelphia, Mississippi (USA)',
        'PHL5' => 'Philadelphia, Mississippi (USA)',
        'PHL6' => 'Philadelphia, Mississippi (USA)',
        'PIT5' => 'Pittsburgh, Pennsylvania (USA)',
        'AVP1' => 'Avoca, Pennsylvania (USA)',
        'CAE1' => 'Columbia, Maryland (USA)',
        'GSP1' => 'Greenville, Alabama (USA)',
        'BNA1' => 'Nashville, Indiana (USA)',
        'BNA2' => 'Nashville, Indiana (USA)',
        'BNA3' => 'Nashville, Indiana (USA)',
        'BNA5' => 'Nashville, Indiana (USA)',
        'CHA1' => 'Chattanooga, Tennessee (USA)',
        'CHA2' => 'Chattanooga, Tennessee (USA)',
        'DFW6' => 'Dallas, Texas (USA)',
        'DFW7' => 'Dallas, Texas (USA)',
        'DFW8' => 'Dallas, Texas (USA)',
        'DFW9' => 'Dallas, Texas (USA)',
        'HOU1' => 'Houston, Texas (USA)',
        'SAT1' => 'San Antonio, Texas (USA)',
        'BWI1' => 'Baltimore, Maryland (USA)',
        'RIC1' => 'Richmond, Napa (USA)',
        'RIC2' => 'Richmond, Napa (USA)',
        'BFI1' => 'Seattle, Washington (USA)',
        'BFI3' => 'Seattle, Washington (USA)',
        'BFI5' => 'Seattle, Washington (USA)',
        'BFIX' => 'Seattle, Washington (USA)',
        'SEA6' => 'Seattle, Washington (USA)',
        'SEA8' => 'Seattle, Washington (USA)',
        'MKE1' => 'Milwaukee, Wisconsin (USA)',
        'MKE5' => 'Milwaukee, Wisconsin (USA)',
        'MKE7' => 'Milwaukee, Wisconsin (USA)'
    ];

    protected static $details = [
        'BER3' => ['Amazon Brieselang Gmbh Havellandstr. 5 14656 Brieselang','Allemagne','DE'],
        'BER6' => ['Amazon Logistik AF München GmbH Am Borsigturm 100 13507 Berlin', 'Allemagne', 'DE'],
        'CGN1' => ['Amazon Koblenz GmbH Amazonstrasse 1 / Industriepark A61 56330 Kobern-Gondorf', 'Allemagne', 'DE'],
        'DTM1' => ['Amazon Logistik Werne GmbH Carl-Zeiss-Straße 3 59368 Werne', 'Allemagne', 'DE'],
        'DTM2' => ['Amazon Logistik Dortmund GmbH Kaltbandstraße 4 44145 Dortmund', 'Allemagne', 'DE'],
        'DUS2' => ['Amazon Fulfillment Germany GmbH Amazonstrasse 1 / Alte Landstrasse 47495 Rheinberg', 'Allemagne', 'DE'],
        'EDE4' => ['Amazon Logistik Werne GmbH Wahrbrink 25 59368 Werne', 'Allemagne', 'DE'],
        'EDE5' => ['Amazon Logistik Werne GmbH Wahrbrink 23 59368 Werne', 'Allemagne', 'DE'],
        'FRA1' => ['Amazon Logistik GmbH Am Schloss Eichhof 1 36251 Bad Hersfeld', 'Allemagne', 'DE'],
        'FRA3' => ['Amazon Logistik GmbH Amazonstrasse 1 Obere Kühnbach 36251 Bad hersfeld', 'Allemagne', 'DE'],
        'FRA7' => ['Amazon Logistik Frankenthal GmbH', 'Allemagne', 'DE'],
        'HAM2' => ['Amazon Logistik Winsen GmbH Borgwardstrasse 10 21423 Winsen an der Luhe ', 'Allemagne', 'DE'],
        'LEJ1' => ['Amazon Distribution GmbH Amazonstrasse1  04347 Leipzig', 'Allemagne', 'DE'],
        'LEJ2' => ['Amazon Distribution GmbH Imaging Operations Friedrichshafner Straße 72a 04357 Leipzig', 'Allemagne', 'DE'],
        'MUC3' => ['Amazon Distribution GmbH Amazonstrasse1  / Zeppelinstrasse 2 86836 Graben', 'Allemagne', 'DE'],
        'STR1' => ['Amazon Pforzheim GmbH Amazonstrasse 1  (A8 Exit 44 PforzheimNord direction Bretten) 75177 Pforzheim', 'Allemagne' ,'DE'],
        'XDEB' => ['XDEB DHL Aussenlager DHL Logistik-Center Ludwigsau Im Fuldatal 2 36251 Ludwigsau OT Mecklar', 'Allemagne', 'DE'],
        'XDEH' => ['XDEH Kühne + Nagel Außenlager Kühne + Nagel (AG & Co.) KG  Betriebsstätte Rennerod  Industriegebiet Alsberg  56477 Rennerod ', 'Allemagne', 'DE'],
        'XDEI' => ['XDEI GEODIS Außenlager GEODIS Logistics Deutschland GmbH  Niederlassung Hamburg Bei der Lehmkuhle 2  21629 Neu WulmstorfMienenbuettel', 'Allemagne', 'DE'],
        'XDEJ' => ['XDEJ Baur Versand Außenlager  Baur Versand GmbH & Co KG  Siegfried-Lapawa-Straße 1 96242 Sonnefeld ', 'Allemagne', 'DE'],
        'XDES' => ['Hermes Fulfilment GmbH Standort Loehne Schillenbrink 4 - 6 D-32584 Loehne ', 'Allemagne', 'DE'],
        'XDET' => ['Amazon Deliveries XDET  Geodis Malsfeld NS 3PL Bornwiese 1 D-34323 Malsfeld', 'Allemagne', 'DE'],
        'XDEU' => ['Amazon Deliveries – XDEU DHL Solutions GmbH Barentsstrasse 24 53881 Euskirchen', 'Allemagne', 'DE'],
        'KTW1' => ['Amazon Fulfillment Poland sp. z o.o. ul. Inwestycyjna 19 Sosnowiec 41-208', 'Pologne', 'PL'],
        'POZ1' => ['POZ1 ul. Poznanska 1d 62 – 080 Sady', 'Pologne', 'PL'],
        'PRG2' => ['PRG2 K Amazonu 235 - 252 61  Dobrovíz', 'République Tchèque', 'CZ'],
        'SZZ1' => ['Amazon Fulfillment Poland sp. z o.o. Kolbaskowo 156  72-001  Kolbaskowo', 'Pologne', 'PL'],
        'WRO1' => ['WRO1  ul. Czekoladowa 1 55 – 040 Bielany Wroclawskie', 'Pologne', 'PL'],
        'WRO2' => ['WRO2 ul. Logistyczna 6 55 – 040 Bielany Wroclawskie', 'Pologne', 'PL'],
        'WRO3' => ['WRO3  ul. Czekoladowa 1 55 – 040 Bielany Wroclawskie', 'Pologne', 'PL'],
        'BHX1' => ['Amazon Rugeley  - Goods In Towers Business Park Power Station Road Rugeley Staffordshire WS15 1NZ (SAT NAV : WS15 1PR)', 'Royaume Uni', 'UK'],
        'BHX2' => ['Amazon Coalville -  Robson Way Ellistown COALVILLE LE67 1GQ', 'Royaume Uni', 'UK'],
        'BHX3' => ['Amazon UK Services Ltd - BHX3   Royal Oak Way North Daventry Northamptonshire NN118QL ', 'Royaume Uni', 'UK'],
        'CWL1' => ['Amazon Swansea - Goods In Fford Amazon Crymlyn Burrows Swansea SA1 8QX', 'Royaume Uni', 'UK'],
        'EDI4' => ['Amazon Dunfermline - Goods In Amazon Way Dunfermline  KY11 8XT', 'Royaume Uni', 'UK'],
        'EUK5' => ['Amazon Peterborough - Goods In Phase 2 Kingston Park Flaxley Road Peterborough PE2 9EN', 'Royaume Uni', 'UK'],
        'GLA1' => ['Amazon Gourock - Goods In 2 Cloch Road Faulds Park Gourock Inverclyde PA19 1BQ', 'Royaume Uni', 'UK'],
        'LBA1' => ['Amazon Doncaster (LBA1) - Goods In Unit 1 Balby Carr Bank Balby Doncaster DN4 5JS', 'Royaume Uni', 'UK'],
        'LBA2' => ['Amazon  Unit 1 Iport Avenue New Rossington Doncaster DN11 0BG', 'Royaume Uni', 'UK'],
        'LBA3' => ['Amazon Doncaster (LBA3) - Goods In Unit 3 Water Vole Way Balby Doncaster DN4 5JP', 'Royaume Uni', 'UK'],
        'LCY1' => ['Amazon London (LCY1) - Goods In Unit B – Prologis Park Twelvetrees Crescent London E3 3JG', 'Royaume Uni', 'UK'],
        'LCY2' => ['Amazon Distribution Depot Unit 2 London Distribution Park Windrush Road Tilbury RM18 7AN', 'Royaume Uni', 'UK'],
        'LTN1' => ['Amazon Milton Keynes  - Goods In Marston Gate Fulfulfilment Centre Ridgemont Bedfordshire MK43 0ZA', 'Royaume Uni', 'UK'],
        'LTN2' => ['Amazon Hemel Hempstead - Goods In Boundary Way Hemel Hempstead Hertfordshire HP2 7LF', 'Royaume Uni', 'UK'],
        'LTN4' => ['Amazon Dunstable - Goods In Unit DC1 (Prologis)  Boscombe Road Dunstable  LU5 4FE', 'Royaume Uni', 'UK'],
        'MAN1' => ['Amazon Manchester -  6 Sunbank Lane  Airport City Manchester  Altrincham  M90 5AA', 'Royaume Uni', 'UK'],
        'MAN2' => ['Omega Plot 7c Orion Boulevard Great Sankey Warrington WA5 3XA', 'Royaume Uni', 'UK'],
        'XUKA' => ['XUKA - DHL Preston Brook (XUKA) Whitehouse Industrial Estate Aston Lane North Runcorn Cheshire WA7 3BN', 'Royaume Uni', 'UK'],
        'XUKC' => ['XUKC - Yusen Logistics UK Vendorflex  Rutherford Drive  Park Farm South  Wellingborough  NN8 6AQ', 'Royaume Uni', 'UK'],
        'XUKD' => ['XUKD - XPO Logistics UK Unit A Daventry Distribution Centre Royal Oak Way North Daventry NN11 8LR', 'Royaume Uni', 'UK'],
        'XUKK' => ['XUKK - Kuehne & Nagel Merlin Park II  Wood Lane  Birmingham B24 9QJ', 'Royaume Uni', 'UK'],
        'BVA1' => ['Amazon.fr Logistique SAS 1  Avenue du Superbe Orénoque ZAC Jules Vernes – Secteur Est 80440 BOVES', 'France', 'FR'],
        'LIL1' => ['Amazon EU SARL c/o Amazon.fr Logistique SAS 1 rue Amazon 59553 Lauwin Planque', 'France', 'FR'],
        'LYS1' => ['Amazon EU SARL  c/o Amazon.fr Logistique SAS  Distripole Chalon  ZAC Val de Bourgogne - 1 rue Amazon  71100 Sevrey', 'France', 'FR'],
        'MRS1' => ['AMAZON EU SARL Building II ZAC Les Portes de Provence 26200 MONTELIMAR', 'France', 'FR'],
        'ORY1' => ['AMAZON EU SARL Pole 45 1401 rue du Champ Rouge 45770 SARAN', 'France', 'FR'],
        'XFRE' => ['Geodis Logistics Satolas Parc des Chênes Nord 135 Rue du Brisson ZI St Quentin Fallavier Bâtiment "IDC9" 38290 Satolas-et-Bonce', 'France', 'FR'],
        'XFRF' => ['Entrepôt KUEHNE NAGEL Livraisons Amazon - XFRF Avenue Louis Renault Bâtiment 3 ZAC du Val Bréon 77610 Châtres en Brie', 'France', 'FR'],
        'XFRG' => ['Entrepôt XPO Logistics Livraisons Amazon - XFRG ZAC du Moulin 45410 Artenay', 'France', 'FR'],
        'XFRH' => ['Entrepôt XPO Logistics Livraisons Amazon - XFRH Parc d’activité de Chanteloup Bâtiment 8A 77550 Moissy Cramayel ', 'France', 'FR'],
        'FCO1' => ['Amazon Italia Logistica S.R.L.  Via della Meccanica 4 Passo Corese 02032', 'Italie', 'IT'],
        'MXP3' => ['Amazon Italia Logistica S.R.L.  Via Rita Levi Montalcini 2 13100 Vercelli', 'Italie', 'IT'],
        'MXP5' => ['Amazon EU Sarl c/o Amazon Italia Logistica Srl Strada Dogana Po 2U 29015 Castel San Giovanni', 'Italie', 'IT'],
        'XITC' => ['Amazon XITC - Geodis Logistics SpA Via Aldo Moro 4  Francolino di Carpiano (MI) 20080', 'Italie', 'IT'],
        'XITD' => [' Amazon XITD - GEODIS Logistics Viale Maestri del Lavoro 990 45031 Arquà Polesine - RO', 'Italie', 'IT'],
        'BCN1' => ['Amazon Spain Fulfillment  S.L.  Avigunda De les Garrigues 6 - 8 El Prat de Llobregat  08820 Barcelona', 'Espagne', 'ES'],
        'BCN2' => ['Amazon Spain Fulfillment  S.L. Carrer De La Vernada  22 08107 Martorelles', 'Espagne', 'ES'],
        'BCN3' => ['Amazon Spain Fulfillment S.L Carrer Ferro 12  08755 Castellbisbal Barcelona', 'Espagne', 'ES'],
        'MAD4' => ['AMAZON EU SARL C/O Amazon Spain Fulfillment  S.L  Avenida De Astronomía 24 28830  San Fernando De Henares  Madrid ', 'Espagne', 'ES'],
        'XESA' => ['XPO LOGISTICS  Avenida Río Henares 16  19208 Alovera Guadalajara', 'Espagne', 'ES'],
        'XESC' => ['Kuehne + Nagel Warehouse Amazon Deliveries – XESC Avenida de las Puntas 10  43120 Constantí – Tarragona', 'Espagne', 'ES'],
        'LGA9' => ['Edison', 'New Jersey  (USA)', 'US'],
        'MDW7' => ['Monee', 'illinois (USA)', 'US'],
        'PHX3' => ['Phoenix', 'Arizona (USA)', 'US'],
        'PHX5' => ['Phoenix', 'Arizona (USA)', 'US'],
        'PHX6' => ['Phoenix', 'Arizona (USA)', 'US'],
        'PHX7' => ['Phoenix', 'Arizona (USA)', 'US'],
        'PHX8' => ['Phoenix', 'Arizona (USA)', 'US'],
        'PHX9' => ['Phoenix', 'Arizona (USA)', 'US'],
        'OAK3' => ['Oakland', 'Napa (USA)', 'US'],
        'OAK4' => ['Oakland', 'Napa (USA)', 'US'],
        'OAK5' => ['Oakland', 'Napa (USA)', 'US'],
        'OAK6' => ['Oakland', 'Napa (USA)', 'US'],
        'OAK7' => ['Oakland', 'Napa (USA)', 'US'],
        'ONT2' => ['Ontario', 'Napa (USA)', 'US'],
        'ONT3' => ['Ontario', 'Napa (USA)', 'US'],
        'ONT4' => ['Ontario', 'Napa (USA)', 'US'],
        'ONT5' => ['Ontario', 'Napa (USA)', 'US'],
        'ONT6' => ['Ontario', 'Napa (USA)', 'US'],
        'ONT8' => ['Ontario', 'Napa (USA)', 'US'],
        'ONT9' => ['Ontario', 'Napa (USA)', 'US'],
        'BDL1' => ['Windsor Locks', 'Connecticut (USA)', 'US'],
        'PHL1' => ['Philadelphia', 'Mississippi (USA)', 'US'],
        'PHL7' => ['Philadelphia', 'Mississippi (USA)', 'US'],
        'PHL9' => ['Philadelphia', 'Mississippi (USA)', 'US'],
        'MCO5' => ['Orlando', 'Florida (USA)', 'US'],
        'MIA5' => ['Miami', 'Florida (USA)', 'US'],
        'TPA1' => ['Tampa', 'Florida (USA)', 'US'],
        'TPA2' => ['Tampa', 'Florida (USA)', 'US'],
        'ATL6' => ['Atlanta', 'Georgia (USA)', 'US'],
        'IND1' => ['Indianapolis', 'Indiana (USA)', 'US'],
        'IND2' => ['Indianapolis', 'Indiana (USA)', 'US'],
        'IND3' => ['Indianapolis', 'Indiana (USA)', 'US'],
        'IND4' => ['Indianapolis', 'Indiana (USA)', 'US'],
        'IND5' => ['Indianapolis', 'Indiana (USA)', 'US'],
        'SDF8' => ['Louisville', 'Kentucky (USA)', 'US'],
        'MCI5' => ['Kansas City', 'Kansas (USA)', 'US'],
        'CVG1' => ['Cincinnati', 'Ohio (USA)', 'US'],
        'CVG2' => ['Cincinnati', 'Ohio (USA)', 'US'],
        'CVG3' => ['Cincinnati', 'Ohio (USA)', 'US'],
        'CVG5' => ['Cincinnati', 'Ohio (USA)', 'US'],
        'CVG7' => ['Cincinnati', 'Ohio (USA)', 'US'],
        'CVG8' => ['Cincinnati', 'Ohio (USA)', 'US'],
        'LEX1' => ['Lexington', 'Kentucky (USA)', 'US'],
        'LEX2' => ['Lexington', 'Kentucky (USA)', 'US'],
        'SDF1' => ['Louisville', 'Kentucky (USA)', 'US'],
        'SDF2' => ['Louisville', 'Kentucky (USA)', 'US'],
        'SDF4' => ['Louisville', 'Kentucky (USA)', 'US'],
        'SDF6' => ['Louisville', 'Kentucky (USA)', 'US'],
        'SDF7' => ['Louisville', 'Kentucky (USA)', 'US'],
        'SDF9' => ['Louisville', 'Kentucky (USA)', 'US'],
        'BOS5' => ['Boston', 'Massachusetts (USA)', 'US'],
        'BWI2' => ['Baltimore', 'Maryland (USA)', 'US'],
        'BWI5' => ['Baltimore', 'Maryland (USA)', 'US'],
        'MSP5' => ['Minneapolis', 'Minnesota (USA)', 'US'],
        'CLT5' => ['Charlotte', 'North Carolina (USA)', 'US'],
        'BOS1' => ['Boston', 'Massachusetts (USA)', 'US'],
        'ACY5' => ['Atlantic City', 'New Jersey (USA)', 'US'],
        'EWR4' => ['Newark', 'Delaware (USA)', 'US'],
        'EWR5' => ['Newark', 'Delaware (USA)', 'US'],
        'EWR6' => ['Newark', 'Delaware (USA)', 'US'],
        'EWR7' => ['Newark', 'Delaware (USA)', 'US'],
        'EWR8' => ['Newark', 'Delaware (USA)', 'US'],
        'LAS2' => ['Las Vegas', 'Nevada (USA)', 'US'],
        'RNO4' => ['Reno', 'Nevada (USA)', 'US'],
        'JFK7' => ['New York', 'New York (USA)', 'US'],
        'ABE2' => ['Allentown', 'Pennsylvania (USA)', 'US'],
        'ABE3' => ['Allentown', 'Pennsylvania (USA)', 'US'],
        'MDT1' => ['Harrisburg', 'Illinois (USA)', 'US'],
        'PHL4' => ['Philadelphia', 'Mississippi (USA)', 'US'],
        'PHL5' => ['Philadelphia', 'Mississippi (USA)', 'US'],
        'PHL6' => ['Philadelphia', 'Mississippi (USA)', 'US'],
        'PIT5' => ['Pittsburgh', 'Pennsylvania (USA)', 'US'],
        'AVP1' => ['Avoca', 'Pennsylvania (USA)', 'US'],
        'CAE1' => ['Columbia', 'Maryland (USA)', 'US'],
        'GSP1' => ['Greenville', 'Alabama (USA)', 'US'],
        'BNA1' => ['Nashville', 'Indiana (USA)', 'US'],
        'BNA2' => ['Nashville', 'Indiana (USA)', 'US'],
        'BNA3' => ['Nashville', 'Indiana (USA)', 'US'],
        'BNA5' => ['Nashville', 'Indiana (USA)', 'US'],
        'CHA1' => ['Chattanooga', 'Tennessee (USA)', 'US'],
        'CHA2' => ['Chattanooga', 'Tennessee (USA)', 'US'],
        'DFW6' => ['Dallas', 'Texas (USA)', 'US'],
        'DFW7' => ['Dallas', 'Texas (USA)', 'US'],
        'DFW8' => ['Dallas', 'Texas (USA)', 'US'],
        'DFW9' => ['Dallas', 'Texas (USA)', 'US'],
        'HOU1' => ['Houston', 'Texas (USA)', 'US'],
        'SAT1' => ['San Antonio', 'Texas (USA)', 'US'],
        'BWI1' => ['Baltimore', 'Maryland (USA)', 'US'],
        'RIC1' => ['Richmond', 'Napa (USA)', 'US'],
        'RIC2' => ['Richmond', 'Napa (USA)', 'US'],
        'BFI1' => ['Seattle', 'Washington (USA)', 'US'],
        'BFI3' => ['Seattle', 'Washington (USA)', 'US'],
        'BFI5' => ['Seattle', 'Washington (USA)', 'US'],
        'BFIX' => ['Seattle', 'Washington (USA)', 'US'],
        'SEA6' => ['Seattle', 'Washington (USA)', 'US'],
        'SEA8' => ['Seattle', 'Washington (USA)', 'US'],
        'MKE1' => ['Milwaukee', 'Wisconsin (USA)', 'US'],
        'MKE5' => ['Milwaukee', 'Wisconsin (USA)', 'US'],
        'MKE7' => ['Milwaukee', 'Wisconsin (USA)', 'US']
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }

    public static function getCountryCode($fullfilementCenterId)
    {
        if(isset(self::$details[$fullfilementCenterId])){
            return self::$details[$fullfilementCenterId][2];
        }
        return null;
    }
}
