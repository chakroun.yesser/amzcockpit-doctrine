<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class SupportTicketStatutType extends AbstractEnumType
{
    const WAITING = 'WAITING';
    const WORKING = 'WORKING';
    const DONE = 'DONE';
    const ARCHIVED = 'ARCHIVED';

    protected static $choices = [
        self::WAITING => 'dbal.types.SupportTicketStatutType.WAITING',
        self::WORKING => 'dbal.types.SupportTicketStatutType.WORKING',
        self::DONE => 'dbal.types.SupportTicketStatutType.DONE',
        self::ARCHIVED => 'dbal.types.SupportTicketStatutType.ARCHIVED'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
