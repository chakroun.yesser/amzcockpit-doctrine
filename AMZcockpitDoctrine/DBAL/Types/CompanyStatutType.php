<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class CompanyStatutType extends AbstractEnumType
{
    const Independant = 'independant';
    const Societe = 'company';
    const Liberale = 'liberale';
    const SOLE_TRADER = 'sole_trader';
    const PARTNERSHIP = 'partnership';
    const CORPORATION = 'corporation';
    const PTY_LTD_COMPANY = 'pty_ltd_company';
    const GENERAL_CORPORATION = 'general_corporation';
    const CLOSE_CORPORATION = 'close_corporation';
    const S_CORPORATION = 's_corporation';
    const LIMITED_LIABILITY_COMPANY = 'llc';
    const SOLE_PROPRIETOR = 'sole_prioprietor';
    const Particulier = 'particular';
    const Association = 'association';
    const SA = 'SA';
    const SRL = 'SRL';
    const SNC = 'SNC';
    const SCS = 'SCS';
    const SA_2 = 'SA_2';
    const SAS = 'SAS';
    const SNC_2 = 'SNC_2';
    const SCS_2 = 'SCS_2';
    const SCA = 'SCA';
    const SASU = 'SASU';
    const EURL = 'EURL';
    const SARL = 'SARL';
    const EIRL = 'EIRL';

    protected static $choices = [
        self::SA_2 => 'dbal.types.CompanyStatutType.SA_2',
        self::SAS => 'dbal.types.CompanyStatutType.SAS',
        self::SNC_2 => 'dbal.types.CompanyStatutType.SNC_2',
        self::SCS_2 => 'dbal.types.CompanyStatutType.SCS_2',
        self::SCA => 'dbal.types.CompanyStatutType.SCA',
        self::SASU => 'dbal.types.CompanyStatutType.SASU',
        self::EURL => 'dbal.types.CompanyStatutType.EURL',
        self::SARL => 'dbal.types.CompanyStatutType.SARL',
        self::EIRL => 'dbal.types.CompanyStatutType.EIRL',
        self::Particulier => 'dbal.types.CompanyStatutType.particular',
        self::Association => 'dbal.types.CompanyStatutType.association',
        self::Independant => 'dbal.types.CompanyStatutType.independant',
        self::Societe => 'dbal.types.CompanyStatutType.company',
        self::Liberale => 'dbal.types.CompanyStatutType.liberale',
        self::SOLE_TRADER => 'dbal.types.CompanyStatutType.SOLE_TRADER',
        self::PARTNERSHIP => 'dbal.types.CompanyStatutType.PARTNERSHIP',
        self::CORPORATION => 'dbal.types.CompanyStatutType.CORPORATION',
        self::PTY_LTD_COMPANY => 'dbal.types.CompanyStatutType.PTY_LTD_COMPANY',
        self::GENERAL_CORPORATION => 'dbal.types.CompanyStatutType.GENERAL_CORPORATION',
        self::CLOSE_CORPORATION => 'dbal.types.CompanyStatutType.CLOSE_CORPORATION',
        self::S_CORPORATION => 'dbal.types.CompanyStatutType.S_CORPORATION',
        self::LIMITED_LIABILITY_COMPANY => 'dbal.types.CompanyStatutType.LIMITED_LIABILITY_COMPANY',
        self::SOLE_PROPRIETOR => 'dbal.types.CompanyStatutType.SOLE_PROPRIETOR',
        self::SA => 'dbal.types.CompanyStatutType.SA',
        self::SRL => 'dbal.types.CompanyStatutType.SRL',
        self::SNC => 'dbal.types.CompanyStatutType.SNC',
        self::SCS => 'dbal.types.CompanyStatutType.SCS'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }

    public static function getChoicesByMarketplaceName($marketplaceName)
    {
        switch ($marketplaceName) {
            case 'Amazon.fr':
            case 'Amazon.co.uk':
            case 'Amazon.de':
            case 'Amazon.es':
            case 'Amazon.it':
            default:
                $choices = [
                    self::SA_2 => 'dbal.types.CompanyStatutType.SA_2',
                    self::SAS => 'dbal.types.CompanyStatutType.SAS',
                    self::SNC_2 => 'dbal.types.CompanyStatutType.SNC_2',
                    self::SCS_2 => 'dbal.types.CompanyStatutType.SCS_2',
                    self::SCA => 'dbal.types.CompanyStatutType.SCA',
                    self::SASU => 'dbal.types.CompanyStatutType.SASU',
                    self::EURL => 'dbal.types.CompanyStatutType.EURL',
                    self::SARL => 'dbal.types.CompanyStatutType.SARL',
                    self::EIRL => 'dbal.types.CompanyStatutType.EIRL',
                    self::Particulier => 'dbal.types.CompanyStatutType.particular',
                    self::Association => 'dbal.types.CompanyStatutType.association',
                    self::Independant => 'dbal.types.CompanyStatutType.independant',
                    self::Societe => 'dbal.types.CompanyStatutType.company',
                    self::Liberale => 'dbal.types.CompanyStatutType.liberale'
                ];
                break;
            case 'Amazon.ca':
                $choices = [
                    self::SOLE_TRADER => 'dbal.types.CompanyStatutType.SOLE_TRADER',
                    self::PARTNERSHIP => 'dbal.types.CompanyStatutType.PARTNERSHIP',
                    self::CORPORATION => 'dbal.types.CompanyStatutType.CORPORATION'
                ];
                break;
            case 'Amazon.com.mx':
                $choices = [
                    self::SA => 'dbal.types.CompanyStatutType.SA',
                    self::SRL => 'dbal.types.CompanyStatutType.SRL',
                    self::SNC => 'dbal.types.CompanyStatutType.SNC',
                    self::SCS => 'dbal.types.CompanyStatutType.SCS'
                ];
                break;
            case 'Amazon.jp':
            case 'Amazon.com':
                $choices = [
                    self::GENERAL_CORPORATION => 'dbal.types.CompanyStatutType.GENERAL_CORPORATION',
                    self::CLOSE_CORPORATION => 'dbal.types.CompanyStatutType.CLOSE_CORPORATION',
                    self::S_CORPORATION => 'dbal.types.CompanyStatutType.S_CORPORATION',
                    self::LIMITED_LIABILITY_COMPANY => 'dbal.types.CompanyStatutType.LIMITED_LIABILITY_COMPANY',
                    self::SOLE_PROPRIETOR => 'dbal.types.CompanyStatutType.SOLE_PROPRIETOR'
                ];
                break;
            case 'Amazon.au':
                $choices = [
                    self::SOLE_TRADER => 'dbal.types.CompanyStatutType.SOLE_TRADER',
                    self::PARTNERSHIP => 'dbal.types.CompanyStatutType.PARTNERSHIP',
                    self::CORPORATION => 'dbal.types.CompanyStatutType.CORPORATION',
                    self::PARTNERSHIP => 'dbal.types.CompanyStatutType.PARTNERSHIP',
                    self::PTY_LTD_COMPANY => 'dbal.types.CompanyStatutType.PTY_LTD_COMPANY'
                ];
                break;
        }
        return array_flip($choices);
    }
}
