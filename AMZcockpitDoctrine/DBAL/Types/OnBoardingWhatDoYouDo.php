<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class OnBoardingWhatDoYouDo extends AbstractEnumType
{
    const PRIVATE_LABEL = 'PRIVATE_LABEL';
    const RETAIL_ARBITRAGE = 'RETAIL_ARBITRAGE';
    const ONLINE_ARBITRAGE = 'ONLINE_ARBITRAGE';
    const DROPSHIPPING = 'DROPSHIPPING';
    const WHOLESALE_DISCOUNTER = 'WHOLESALE_DISCOUNTER';

    protected static $choices = [
        self::PRIVATE_LABEL => 'dbal.types.OnBoardingWhatDoYouDo.PRIVATE_LABEL',
        self::RETAIL_ARBITRAGE => 'dbal.types.OnBoardingWhatDoYouDo.RETAIL_ARBITRAGE',
        self::ONLINE_ARBITRAGE => 'dbal.types.OnBoardingWhatDoYouDo.ONLINE_ARBITRAGE',
        self::DROPSHIPPING => 'dbal.types.OnBoardingWhatDoYouDo.DROPSHIPPING',
        self::WHOLESALE_DISCOUNTER => 'dbal.types.OnBoardingWhatDoYouDo.WHOLESALE_DISCOUNTER'
    ];

    protected static $titles = [
        self::PRIVATE_LABEL => 'Vous commercialisez vos produits sous le nom de votre propre marque ou le nom de votre boutique.',
        self::RETAIL_ARBITRAGE => 'Pratique de vente consistant à tirer profit des différences de prix entre deux ou plusieurs établissement commerciaux.',
        self::ONLINE_ARBITRAGE => 'Pratique de vente consistant à tirer profit des différences de prix entre deux ou plusieurs place de marché sur Internet.',
        self::DROPSHIPPING => 'Vente de produits encore en stock chez votre fournisseur. Ce dernier sera alors chargé d&#39;effectuer la livraison, généralement en lieu et place du vendeur.',
        self::WHOLESALE_DISCOUNTER => 'Commerce de gros et « discounter »'
    ];

    public static function getTitles()
    {
        return static::$titles;
    }

    public static function getTitleByKey($key)
    {
        return static::$titles[$key];
    }

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
