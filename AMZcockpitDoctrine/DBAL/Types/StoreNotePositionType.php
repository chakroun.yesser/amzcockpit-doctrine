<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class StoreNotePositionType extends AbstractEnumType
{
    const HEADER = 'HEADER';
    const FOOTER = 'FOOTER';

    protected static $choices = [
        self::HEADER => 'dbal.types.StoreNotePositionType.HEADER',
        self::FOOTER => 'dbal.types.StoreNotePositionType.FOOTER',
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
