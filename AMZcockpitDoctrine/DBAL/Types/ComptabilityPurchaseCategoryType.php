<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class ComptabilityPurchaseCategoryType extends AbstractEnumType
{
    const SOFTWARE_SUBSCRIBTION = 'SOFTWARE_SUBSCRIBTION';
    const PRODUCTS_PURCHASE = 'PRODUCTS_PURCHASE';
    const ALIMENTARY = 'ALIMENTARY';
    const INSURANCE = 'INSURANCE';
    const OTHERS = 'OTHERS';
    const BANK = 'BANK';
    const SHIFTING = 'SHIFTING';
    const ELECTRICITY = 'ELECTRICITY';
    const FISCALITY = 'FISCALITY';
    const SUPPLY = 'SUPPLY';
    const POSTAl_CHARGES = 'POSTAl_CHARGES';
    const INFORMATIQUE = 'INFORMATIQUE';
    const INTERNET = 'INTERNET';
    const RENT = 'RENT';
    const MATERIALS = 'MATERIALS';
    const PRIMARY_MATERIALS = 'PRIMARY_MATERIALS';
    const MUTUELLE = 'MUTUELLE';
    const ADS = 'ADS';
    const PUBLICITY = 'PUBLICITY';
    const SALARY = 'SALARY';
    const PHONES = 'PHONES';
    const CLOTHES = 'CLOTHES';

    protected static $choices = [
        self::SOFTWARE_SUBSCRIBTION => 'dbal.types.ComptabilityPurchaseCategoryType.SOFTWARE_SUBSCRIBTION',
        self::PRODUCTS_PURCHASE => 'dbal.types.ComptabilityPurchaseCategoryType.PRODUCTS_PURCHASE',
        self::ALIMENTARY => 'dbal.types.ComptabilityPurchaseCategoryType.ALIMENTARY',
        self::INSURANCE => 'dbal.types.ComptabilityPurchaseCategoryType.INSURANCE',
        self::OTHERS => 'dbal.types.ComptabilityPurchaseCategoryType.OTHERS',
        self::BANK => 'dbal.types.ComptabilityPurchaseCategoryType.BANK',
        self::SHIFTING => 'dbal.types.ComptabilityPurchaseCategoryType.SHIFTING',
        self::ELECTRICITY => 'dbal.types.ComptabilityPurchaseCategoryType.ELECTRICITY',
        self::FISCALITY => 'dbal.types.ComptabilityPurchaseCategoryType.FISCALITY',
        self::SUPPLY => 'dbal.types.ComptabilityPurchaseCategoryType.SUPPLY',
        self::POSTAl_CHARGES => 'dbal.types.ComptabilityPurchaseCategoryType.POSTAl_CHARGES',
        self::INFORMATIQUE => 'dbal.types.ComptabilityPurchaseCategoryType.INFORMATIQUE',
        self::INTERNET => 'dbal.types.ComptabilityPurchaseCategoryType.INTERNET',
        self::RENT => 'dbal.types.ComptabilityPurchaseCategoryType.RENT',
        self::MATERIALS => 'dbal.types.ComptabilityPurchaseCategoryType.MATERIALS',
        self::PRIMARY_MATERIALS => 'dbal.types.ComptabilityPurchaseCategoryType.PRIMARY_MATERIALS',
        self::MUTUELLE => 'dbal.types.ComptabilityPurchaseCategoryType.MUTUELLE',
        self::ADS => 'dbal.types.ComptabilityPurchaseCategoryType.ADS',
        self::PUBLICITY => 'dbal.types.ComptabilityPurchaseCategoryType.PUBLICITY',
        self::SALARY => 'dbal.types.ComptabilityPurchaseCategoryType.SALARY',
        self::PHONES => 'dbal.types.ComptabilityPurchaseCategoryType.PHONES',
        self::CLOTHES => 'dbal.types.ComptabilityPurchaseCategoryType.CLOTHES'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
