<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class EmailBroadcastStatutType extends AbstractEnumType
{
    const DRAFT = 'draft';
    const SENT = 'sent';
    const TO_SEND = 'to_send';
    const WORKING_SEND = 'working_send';
    const RESENT = 'resent';
    const ARCHIVED = 'archived';


    protected static $choices = [
        self::DRAFT => 'dbal.types.EmailBroadcastStatutType.DRAFT',
        self::SENT => 'dbal.types.EmailBroadcastStatutType.SENT',
        self::TO_SEND => 'dbal.types.EmailBroadcastStatutType.TO_SEND',
        self::WORKING_SEND => 'dbal.types.EmailBroadcastStatutType.WORKING_SEND',
        self::RESENT => 'dbal.types.EmailBroadcastStatutType.RESENT',
        self::ARCHIVED => 'dbal.types.EmailBroadcastStatutType.ARCHIVED'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
