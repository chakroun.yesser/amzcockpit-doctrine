<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class PaymentStatutType extends AbstractEnumType
{
    const PAID = 'PAID';
    const UNPAID = 'UNPAID';
    const INFORMED = 'INFORMED';
    const TRAILING = 'TRAILING';

    protected static $choices = [
        self::PAID => 'dbal.types.PaymentStatutType.PAID',
        self::UNPAID => 'dbal.types.PaymentStatutType.UNPAID',
        self::INFORMED => 'dbal.types.PaymentStatutType.INFORMED',
        self::TRAILING => 'dbal.types.PaymentStatutType.TRAILING'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
