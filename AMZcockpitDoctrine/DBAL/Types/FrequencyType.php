<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class FrequencyType extends AbstractEnumType
{
    const NONE = 'NONE';
    const DAILY = 'DAILY';
    const MONTHLY = 'MONTHLY';
    const ANNUALLY = 'ANNUALLY';

    protected static $choices = [
        self::NONE => 'dbal.types.FrequencyType.NONE',
        self::DAILY => 'dbal.types.FrequencyType.DAILY',
        self::MONTHLY => 'dbal.types.FrequencyType.MONTHLY',
        self::ANNUALLY => 'dbal.types.FrequencyType.ANNUALLY'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
