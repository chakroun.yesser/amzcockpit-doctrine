<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class PaymentMethodType extends AbstractEnumType
{
    const WIRE_BANK = 'WIRE_BANK';
    const SEPA = 'SEPA';

    protected static $choices = [
        self::WIRE_BANK => 'dbal.types.PaymentMethodType.WIRE_BANK',
        self::SEPA => 'dbal.types.PaymentMethodType.SEPA',
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
