<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class PaymentStatusType extends AbstractEnumType
{
    const SUCCESS = 'SUCCESS';
    const FAILED = 'FAILED';
    const PENDING = 'PENDING';

    protected static $choices = [
        self::SUCCESS => 'dbal.types.PaymentMethodType.SUCCESS',
        self::FAILED => 'dbal.types.PaymentMethodType.FAILED',
        self::PENDING => 'dbal.types.PaymentMethodType.PENDING'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
