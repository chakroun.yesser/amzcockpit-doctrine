<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class SupportHelpdeskCategoryType extends AbstractEnumType
{
    const GENERAL = 'general';
    const DASHBOARD = 'dashboard';
    const PRODUCTS = 'products';
    const ORDERS = 'orders';
    const INVOICES = 'invoices';
    const EXPORT_INVOICES = 'export_invoices';
    const CLIENTS = 'clients';
    const REVIEWS = 'reviews';
    const FEEDBACKS = 'feedbacks';
    const EMAILS = 'emails';
    const EMAILS_BROADCAST = 'emails_broadcast';
    const FISCALITY = 'fiscality';
    const SUBSCRIPTION = 'subscription';
    const MY_STORES = 'my_stores';
    const MY_PROFIL = 'my_profil';
    const SUBSCRIPTION_OPTIONS = 'subscription_options';

    protected static $choices = [
        self::GENERAL => 'dbal.types.SupportHelpdeskCategoryType.general',
        self::DASHBOARD => 'dbal.types.SupportHelpdeskCategoryType.dashboard',
        self::PRODUCTS => 'dbal.types.SupportHelpdeskCategoryType.products',
        self::ORDERS => 'dbal.types.SupportHelpdeskCategoryType.orders',
        self::INVOICES => 'dbal.types.SupportHelpdeskCategoryType.invoices',
        self::EXPORT_INVOICES => 'dbal.types.SupportHelpdeskCategoryType.export_invoices',
        self::CLIENTS => 'dbal.types.SupportHelpdeskCategoryType.clients',
        self::REVIEWS => 'dbal.types.SupportHelpdeskCategoryType.reviews',
        self::FEEDBACKS => 'dbal.types.SupportHelpdeskCategoryType.feedbacks',
        self::EMAILS => 'dbal.types.SupportHelpdeskCategoryType.emails',
        self::EMAILS_BROADCAST => 'dbal.types.SupportHelpdeskCategoryType.emails_broadcast',
        self::FISCALITY => 'dbal.types.SupportHelpdeskCategoryType.fiscality',
        self::SUBSCRIPTION => 'dbal.types.SupportHelpdeskCategoryType.subscription',
        self::MY_STORES => 'dbal.types.SupportHelpdeskCategoryType.my_stores',
        self::MY_PROFIL => 'dbal.types.SupportHelpdeskCategoryType.my_profil',
        self::SUBSCRIPTION_OPTIONS => 'dbal.types.SupportHelpdeskCategoryType.subscription_options'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }

    public static function getPageRoutes()
    {
        return [
            self::GENERAL => [],
            self::DASHBOARD => ['backend_homepage'],
            self::PRODUCTS => ["products_list",'product_view'],
            self::ORDERS => ['orders_list','order_view'],
            self::INVOICES => ['invoices_list','invoice_view'],
            self::EXPORT_INVOICES => ['invoices_zip_requests_list'],
            self::CLIENTS => ['customers_list','customer_view'],
            self::REVIEWS => ['reviews_list','review_view'],
            self::FEEDBACKS => ['reviews_feedbacks','feedback_view'],
            self::EMAILS => ['emails_list','email_edit','email_view','email_stat','emails_variables','emails_queue','email_queue_delete','emails_custom_variables','emails_custom_variable','emails_view_custom_variable','email_job_view'],
            self::EMAILS_BROADCAST => 'dbal.types.SupportHelpdeskCategoryType.emails_broadcast',
            self::FISCALITY => ['fiscality'],
            self::SUBSCRIPTION => ['subscription','add_card','subscription_thanks'],
            self::MY_STORES => ['stores_list','store_add','store_edit'],
            self::MY_PROFIL => ['profil_informations','invoices_settings']
        ];
    }
}
