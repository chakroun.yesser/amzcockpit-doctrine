<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class AudienceStatutType extends AbstractEnumType
{
    const DRAFT = 'DRAFT';
    const SAVED = 'SAVED';

    protected static $choices = [
        self::DRAFT => 'dbal.types.AudienceStatutType.DRAFT',
        self::SAVED => 'dbal.types.AudienceStatutType.SAVED',
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
