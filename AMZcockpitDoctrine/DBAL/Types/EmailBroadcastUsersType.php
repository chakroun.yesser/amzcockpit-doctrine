<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class EmailBroadcastUsersType extends AbstractEnumType
{
    const ALL = 'ALL';
    const PAYANTS = 'PAYANTS';
    const ACTIFS = 'ACTIFS';
    const INACTIFS = 'INACTIFS';

    protected static $choices = [
        self::ALL => 'dbal.types.EmailBroadcastUsersType.ALL',
        self::PAYANTS => 'dbal.types.EmailBroadcastUsersType.PAYANTS',
        self::ACTIFS => 'dbal.types.EmailBroadcastUsersType.ACTIFS',
        self::INACTIFS => 'dbal.types.EmailBroadcastUsersType.INACTIFS'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
