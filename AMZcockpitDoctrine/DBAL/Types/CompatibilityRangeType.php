<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class CompatibilityRangeType extends AbstractEnumType
{
    const TODAY = 'TODAY';
    const YESTERDAY = 'YESTERDAY';
    const SEVEN_DAYS = 'SEVEN_DAYS';
    const THIRTY_DAYS = 'THIRTY_DAYS';
    const THREE_MONTH = 'THREE_MONTH';
    const SIX_MONTH = 'SIX_MONTH';
    const ONE_YEAR = 'ONE_YEAR';
    const CURRENT_MONTH = 'CURRENT_MONTH';
    const CURRENT_YEAR = 'CURRENT_YEAR';
    const CUSTOM = 'CUSTOM';

    protected static $choices = [
        self::TODAY => 'dbal.types.CompatibilityRangeType.TODAY',
        self::YESTERDAY => 'dbal.types.CompatibilityRangeType.YESTERDAY',
        self::SEVEN_DAYS => 'dbal.types.CompatibilityRangeType.SEVEN_DAYS',
        self::THIRTY_DAYS => 'dbal.types.CompatibilityRangeType.THIRTY_DAYS',
        self::THREE_MONTH => 'dbal.types.CompatibilityRangeType.THREE_MONTH',
        self::SIX_MONTH => 'dbal.types.CompatibilityRangeType.SIX_MONTH',
        self::ONE_YEAR => 'dbal.types.CompatibilityRangeType.ONE_YEAR',
        self::CURRENT_MONTH => 'dbal.types.CompatibilityRangeType.CURRENT_MONTH',
        self::CURRENT_YEAR => 'dbal.types.CompatibilityRangeType.CURRENT_YEAR',
        self::CUSTOM => 'dbal.types.CompatibilityRangeType.CUSTOM'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
