<?php

namespace AMZcockpitDoctrine\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class OnBoardingWhenStartAmazon extends AbstractEnumType
{
    const LT3M = 'LT3M';
    const GT6M = 'GT6M';
    const GT1Y = 'GT1Y';
    const GT2Y = 'GT2Y';

    protected static $choices = [
        self::LT3M => 'dbal.types.OnBoardingWhenStartAmazon.LT3M',
        self::GT6M => 'dbal.types.OnBoardingWhenStartAmazon.GT6M',
        self::GT1Y => 'dbal.types.OnBoardingWhenStartAmazon.GT1Y',
        self::GT2Y => 'dbal.types.OnBoardingWhenStartAmazon.GT2Y'
    ];

    public static function getArrayChoices()
    {
        return static::$choices;
    }
}
