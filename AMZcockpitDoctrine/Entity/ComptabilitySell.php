<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\FrequencyType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * ComptabilitySell
 *
 * @ORM\Table(name="comptability_sells")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\ComptabilitySellRepository")
 */
class ComptabilitySell
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomIdGenerator")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateEnd;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\ComptabilityClient", inversedBy="sells")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="purchases")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\ComptabilitySellLine", mappedBy="comptabilitySell", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $lines;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="amount_ht", type="string")
     */
    private $amountHT;

    /**
     * @ORM\Column(name="amount", type="string")
     */
    private $amount;

    /**
     * @ORM\Column(name="tva_amount", type="string")
     */
    private $TVAamount;

    /**
     * @ORM\Column(name="discount", type="float")
     */
    private $discount;

    /**
     * @ORM\Column(name="discount_amount", type="float")
     */
    private $discountAmount;

    /**
     * @ORM\Column(name="discount_type", type="string", length=10)
     */
    private $discountType;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\FrequencyType")
     * @ORM\Column(name="frequency", type="FrequencyType", length=255, nullable=true)
     */
    private $frequency;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", nullable=true, length=4)
     */
    private $tva;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mentionTva;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasCustomMention;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $mentionCustomText;

    public function __construct()
    {
        $this->frequency = FrequencyType::NONE;
        $this->discount = 0;
        $this->discountType = "euro";
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ComptabilitySell
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ComptabilitySell
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ComptabilitySell
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set frequency
     *
     * @param FrequencyType $frequency
     *
     * @return ComptabilitySell
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return FrequencyType
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ComptabilitySell
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return ComptabilitySell
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEnd.
     *
     * @param \DateTime|null $dateEnd
     *
     * @return ComptabilitySell
     */
    public function setDateEnd($dateEnd = null)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd.
     *
     * @return \DateTime|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set client.
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilityClient|null $client
     *
     * @return ComptabilitySell
     */
    public function setClient(\AMZcockpitDoctrine\Entity\ComptabilityClient $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return \AMZcockpitDoctrine\Entity\ComptabilityClient|null
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set reference.
     *
     * @param string|null $reference
     *
     * @return ComptabilitySell
     */
    public function setReference($reference = null)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string|null
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set amountHT.
     *
     * @param string $amountHT
     *
     * @return ComptabilitySell
     */
    public function setAmountHT($amountHT)
    {
        $this->amountHT = $amountHT;

        return $this;
    }

    /**
     * Get amountHT.
     *
     * @return string
     */
    public function getAmountHT()
    {
        return $this->amountHT;
    }

    /**
     * Set discount.
     *
     * @param float $discount
     *
     * @return ComptabilitySell
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Add line.
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilitySellLine $line
     *
     * @return ComptabilitySell
     */
    public function addLine(\AMZcockpitDoctrine\Entity\ComptabilitySellLine $line)
    {
        $line->setComptabilitySell($this);
        $this->lines[] = $line;

        return $this;
    }

    /**
     * Remove line.
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilitySellLine $line
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLine(\AMZcockpitDoctrine\Entity\ComptabilitySellLine $line)
    {
        return $this->lines->removeElement($line);
    }

    /**
     * Get lines.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * Set discountType.
     *
     * @param string $discountType
     *
     * @return ComptabilitySell
     */
    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * Get discountType.
     *
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    public function getUnitsCount()
    {
        $return = 0;
        /** @var ComptabilitySellLine $line */
        foreach ($this->getLines() as $line) {
            $return += $line->getQuantity();
        }
        return $return;
    }

    /**
     * Set tVAamount.
     *
     * @param string $tVAamount
     *
     * @return ComptabilitySell
     */
    public function setTVAamount($tVAamount)
    {
        $this->TVAamount = $tVAamount;

        return $this;
    }

    /**
     * Get tVAamount.
     *
     * @return string
     */
    public function getTVAamount()
    {
        return $this->TVAamount;
    }

    /**
     * Set tva
     *
     * @param string $tva
     *
     * @return ComptabilitySell
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return string
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set mentionTva
     *
     * @param boolean $mentionTva
     *
     * @return ComptabilitySell
     */
    public function setMentionTva($mentionTva)
    {
        $this->mentionTva = $mentionTva;

        return $this;
    }

    /**
     * Get mentionTva
     *
     * @return boolean
     */
    public function getMentionTva()
    {
        return $this->mentionTva;
    }

    /**
     * Set mentionCustomText
     *
     * @param string $mentionCustomText
     *
     * @return ComptabilitySell
     */
    public function setMentionCustomText($mentionCustomText)
    {
        $this->mentionCustomText = $mentionCustomText;

        return $this;
    }

    /**
     * Get mentionCustomText
     *
     * @return string
     */
    public function getMentionCustomText()
    {
        return $this->mentionCustomText;
    }

    /**
     * Set hasCustomMention
     *
     * @param boolean $hasCustomMention
     *
     * @return ComptabilitySell
     */
    public function setHasCustomMention($hasCustomMention)
    {
        $this->hasCustomMention = $hasCustomMention;

        return $this;
    }

    /**
     * Get hasCustomMention
     *
     * @return boolean
     */
    public function getHasCustomMention()
    {
        return $this->hasCustomMention;
    }

    /**
     * Set discountAmount
     *
     * @param float $discountAmount
     *
     * @return ComptabilitySell
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }
}
