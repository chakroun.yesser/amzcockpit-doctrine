<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Email
 *
 * @ORM\Table(name="emails", options={"collate":"utf8mb4_general_ci", "charset":"utf8mb4"})
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailRepository")
 */
class Email
{
    const DRAFT     = 'draft';
    const WORKING   = 'working';

    const EVENT_CONFIRMED        = 'Confirmed';
    const EVENT_SHIPPED          = 'Shipped';
    const EVENT_DELIVERED        = 'Delivered';
    const EVENT_RETURNED         = 'Returned';
    const EVENT_REFUNDED         = 'Refunded';

    const EVENT_ONE_STAR                = 'one_star';
    const EVENT_FIVE_STARS              = 'five_star';

    const EVENT_TWO_STARS_BELOW         = 'two_stars_below';
    const EVENT_THREE_STARS_BELOW       = 'three_star_below';
    const EVENT_FOUR_STARS_BELOW        = 'four_star_below';

    const EVENT_TWO_STARS_ABOVE         = 'two_star_above';
    const EVENT_THREE_STARS_ABOVE       = 'three_star_above';
    const EVENT_FOUR_STARS_ABOVE        = 'four_star_above';

    const EVENT_STARTS = [
        self::EVENT_ONE_STAR,
        self::EVENT_FIVE_STARS,
        self::EVENT_TWO_STARS_ABOVE,
        self::EVENT_THREE_STARS_BELOW,
        self::EVENT_FOUR_STARS_BELOW,
        self::EVENT_TWO_STARS_ABOVE,
        self::EVENT_THREE_STARS_ABOVE,
        self::EVENT_FOUR_STARS_ABOVE
    ];

    const EVENT_PROMOTION                   = 'promotion';

    const EVENT_POSITIF_FEEDBACK            = 'positive_seller_feedback';
    const EVENT_FIVE_STARS_FEEDBACK         = 'seller_feedback_5_star';

    const EVENT_FEEDBACKS = [
        self::EVENT_POSITIF_FEEDBACK,
        self::EVENT_FIVE_STARS_FEEDBACK
    ];

    const EVENTS_FBM = [
        'Confirmed'                     => 'Confirmed',
        'Shipped'                       => 'Shipped'
    ];

    const EVENTS_FBA = [
        'Shipped'                       => 'Shipped',
        'Delivered'                     => 'Delivered',
        'Returned'                      => 'Returned',
        'Refunded'                      => 'Refunded'
    ];

    const EVENTS = [
        'Confirmed'                     => 'Confirmed',
        'Shipped'                       => 'Shipped',
        'Delivered'                     => 'Delivered',
        'Returned'                      => 'Returned',
        'Refunded'                      => 'Refunded',
//        '★ Review'                      => 'one_star',
//        '★★ or Below Review'            => 'two_stars_below',
//        '★★★ or Below Review'                   => 'three_star_below',
//        '★★★★ or Below Review'                  => 'four_star_below',
//        '★★ or Above Review'                    => 'two_star_above',
//        '★★★ or Above Review'                   => 'three_star_above',
//        '★★★★ or Above Review'                  => 'four_star_above',
//        '★★★★★ Review'                          => 'five_star',
//        'Order Used Promotion'                  => 'promotion',
//        'Postive Seller Feedback (4 or 5★)'     => 'positive_seller_feedback',
//        'Negative (3★ or under) Seller Feedback'=> 'negative_seller_feedback',
//        '5★ Seller Feedback'                    => 'seller_feedback_5_star',
    ];

    const EVENTS_INVERS = [
        'Confirmed'                     => 'Confirmed',
        'Shipped'                       => 'Shipped',
        'Delivered'                     => 'Delivered',
        'Returned'                      => 'Returned',
        'Refunded'                      => 'Refunded',
//        'one_star'                      => '★ Review',
//        'two_stars_below'               => '★★ or Below Review',
//        'three_star_below'                   => '★★★ or Below Review',
//        'four_star_below'                  => '★★★★ or Below Review',
//        'two_star_above'                    => '★★ or Above Review',
//        'three_star_above'                   => '★★★ or Above Review',
//        'four_star_above'                  => '★★★★ or Above Review',
//        'five_star'                          => '★★★★★ Review',
//        'promotion'                  => 'Order Used Promotion',
//        'positive_seller_feedback'     => 'Postive Seller Feedback (4 or 5★)',
//        'negative_seller_feedback' => 'Negative (3★ or under) Seller Feedback',
//        'seller_feedback_5_star'                    => '5★ Seller Feedback',
    ];

    /** Meaning */
    const EVENTS_TRANS = [
        'Confirmed'                     => 'email.triggers.confirmed',
        'Shipped'                       => 'email.triggers.shipped',
        'Delivered'                     => 'email.triggers.delivered',
        'Returned'                      => 'email.triggers.returned',
        'Refunded'                      => 'email.triggers.refunded',
//        'one_star'                      => 'email.triggers.one_star',
//        'two_stars_below'               => 'email.triggers.two_stars_below',
//        'three_star_below'              => 'email.triggers.three_star_below',
//        'four_star_below'               => 'email.triggers.four_star_below',
//        'two_star_above'                => 'email.triggers.two_star_above',
//        'three_star_above'              => 'email.triggers.three_star_above',
//        'four_star_above'               => 'email.triggers.four_star_above',
//        'five_star'                     => 'email.triggers.five_star',
//        'promotion'                     => 'email.triggers.promotion',
//        'positive_seller_feedback'      => 'email.triggers.positive_seller_feedback',
//        'negative_seller_feedback'      => 'email.triggers.negative_seller_feedback',
//        'seller_feedback_5_star'        => 'email.triggers.seller_feedback_5_star',
    ];

    const STOP_REVIEWS  = [
        'No, Keep Sending'                                  => 'ignore',
        'Stop on Any Review'                                =>  'any',
        'Stop if Returned'                                  =>  'returned',
        'Stop if Refunded'                                  =>  'refunded',
        'Stop On ★'                                         =>  'one_star' ,
        'Stop On ★★ or Below'                               =>  'two_star_below',
        'Stop On ★★★ or Below'                              =>  'three_star_below',
        'Stop On ★★★★ or Below'                             =>  'four_star_below',
        'Stop On ★★ or Above'                               =>  'two_star_above',
        'Stop On ★★★ or Above'                              =>  'three_star_above',
        'Stop On ★★★★ or Above'                             =>  'four_star_above',
        'Stop On ★★★★★'                                     =>  'five_star',
        'Stop on Any Seller Feedback'                       =>  'any_feedback',
        'Order Used Promotion'                              => 'promotion',
        'Stop On Postive Seller Feedback (4 or 5★)'         =>  'positive_seller_feedback',
        'Stop On Negative (3★ or under) Seller Feedback'    =>  'negative_seller_feedback'
    ];

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $title;

    /**
     * @ORM\Column(name="predefined", type="boolean")
     */
    private $predefined = false;

    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = false;

    /**
     * @ORM\Column(name="important", type="boolean")
     */
    private $important = false;

    /**
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @ORM\Column(name="subject", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $subject;

    /**
     * @ORM\Column(name="marketplaces", type="array", nullable=true)
     */
    private $marketPlaces;

    /**
     * @ORM\Column(name="channel", type="string", nullable=true)
     */
    private $channel;

    /**
     * @ORM\Column(name="delivery_value", type="string", nullable=true)
     */
    private $deliveryValue;

    /**
     * @ORM\Column(name="delivery_unit", type="string", nullable=true)
     */
    private $deliveryUnit;

    /**
     * @ORM\Column(name="delivery_event", type="string", nullable=true)
     */
    private $deliveryEvent;

    /**
     * @ORM\Column(name="delivery_time", type="time", nullable=true)
     */
    private $deliveryTime;

    /**
     * @ORM\Column(name="repeated_buyer", type="string", nullable=true)
     */
    private $reapeatedBuyer;

    /**
     * @ORM\Column(name="stop_on_review", type="string", nullable=true)
     */
    private $stopOnReview;

    /**
     * @ORM\Column(name="send_to_order_placed_after", type="datetime", nullable=true)
     */
    private $orderAfterDate;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="emails")
     * @ORM\JoinTable(name="emails_products",
     *      joinColumns={@ORM\JoinColumn(name="email_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     * )
     */
    protected $products;

    /**
     * @ORM\Column(name="body", type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $body;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist"})
     */
    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="emails")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\EmailFolder", inversedBy="emails")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $emailFolder;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailJob", mappedBy="email", cascade={"persist","remove"})
     */
    protected $emailJobs;

    /**
     * @ORM\Column(name="includeInvoice", type="boolean")
     */
    private $includeInvoice = false;

    /**
     * @ORM\Column(name="select_all_products", type="boolean")
     */
    private $selectAllProducts = false;

    /**
     * @ORM\Column(name="auto_resend", type="boolean")
     */
    private $autoResend = false;

    /**
     * @ORM\Column(name="dateStartAutoResend", type="datetime", nullable=true)
     */
    private $dateStartAutoResend;

    /**
     * @ORM\Column(name="autoResendBeforeValue", type="string", nullable=true)
     */
    private $autoResendBeforeValue;

    /**
     * @ORM\Column(name="autoResendBeforeUnit", type="string", nullable=true)
     */
    private $autoResendBeforeUnit;

    /**
     * @ORM\Column(name="autoResendAfterValue", type="string", nullable=true)
     */
    private $autoResendAfterValue;

    /**
     * @ORM\Column(name="autoResendAfterUnit", type="string", nullable=true)
     */
    private $autoResendAfterUnit;

    public function __construct()
    {
        $this->status = self::WORKING;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Email
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Email
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Email
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set marketPlaces
     *
     * @param array $marketPlaces
     *
     * @return Email
     */
    public function setMarketPlaces($marketPlaces)
    {
        $this->marketPlaces = $marketPlaces;

        return $this;
    }

    /**
     * Get marketPlaces
     *
     * @return array
     */
    public function getMarketPlaces()
    {
        return $this->marketPlaces;
    }

    /**
     * Set channel
     *
     * @param string $channel
     *
     * @return Email
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set deliveryValue
     *
     * @param string $deliveryValue
     *
     * @return Email
     */
    public function setDeliveryValue($deliveryValue)
    {
        $this->deliveryValue = $deliveryValue;

        return $this;
    }

    /**
     * Get deliveryValue
     *
     * @return string
     */
    public function getDeliveryValue()
    {
        return $this->deliveryValue;
    }

    /**
     * Set deliveryUnit
     *
     * @param string $deliveryUnit
     *
     * @return Email
     */
    public function setDeliveryUnit($deliveryUnit)
    {
        $this->deliveryUnit = $deliveryUnit;

        return $this;
    }

    /**
     * Get deliveryUnit
     *
     * @return string
     */
    public function getDeliveryUnit()
    {
        return $this->deliveryUnit;
    }

    /**
     * Set deliveryEvent
     *
     * @param string $deliveryEvent
     *
     * @return Email
     */
    public function setDeliveryEvent($deliveryEvent)
    {
        $this->deliveryEvent = $deliveryEvent;

        return $this;
    }

    /**
     * Get deliveryEvent
     *
     * @return string
     */
    public function getDeliveryEvent()
    {
        return $this->deliveryEvent;
    }

    /**
     * Set reapeatedBuyer
     *
     * @param string $reapeatedBuyer
     *
     * @return Email
     */
    public function setReapeatedBuyer($reapeatedBuyer)
    {
        $this->reapeatedBuyer = $reapeatedBuyer;

        return $this;
    }

    /**
     * Get reapeatedBuyer
     *
     * @return string
     */
    public function getReapeatedBuyer()
    {
        return $this->reapeatedBuyer;
    }

    /**
     * Set stopOnReview
     *
     * @param string $stopOnReview
     *
     * @return Email
     */
    public function setStopOnReview($stopOnReview)
    {
        $this->stopOnReview = $stopOnReview;

        return $this;
    }

    /**
     * Get stopOnReview
     *
     * @return string
     */
    public function getStopOnReview()
    {
        return $this->stopOnReview;
    }

    /**
     * Set orderAfterDate
     *
     * @param \DateTime $orderAfterDate
     *
     * @return Email
     */
    public function setOrderAfterDate($orderAfterDate)
    {
        $this->orderAfterDate = $orderAfterDate;

        return $this;
    }

    /**
     * Get orderAfterDate
     *
     * @return \DateTime
     */
    public function getOrderAfterDate()
    {
        return $this->orderAfterDate;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Email
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Add product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return Email
     */
    public function addProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        if(is_array($this->getProducts()) && !in_array($product,$this->getProducts())) {
            $this->products[] = $product;
        }

        if($this->getProducts() instanceof ArrayCollection && !$this->getProducts()->contains($product))
            $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     */
    public function removeProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set file
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return Email
     */
    public function setFile(\AMZcockpitDoctrine\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return Email
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     *
     * @return Email
     */
    public function addEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob)
    {
        $this->emailJobs[] = $emailJob;

        return $this;
    }

    /**
     * Remove emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     */
    public function removeEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob)
    {
        $this->emailJobs->removeElement($emailJob);
    }

    /**
     * Get emailJobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailJobs()
    {
        return $this->emailJobs;
    }


    public function getDeliveryEventValue()
    {
        return self::EVENTS_TRANS[$this->deliveryEvent];
    }

    /**
     * Set predefined
     *
     * @param boolean $predefined
     *
     * @return Email
     */
    public function setPredefined($predefined)
    {
        $this->predefined = $predefined;

        return $this;
    }

    /**
     * Get predefined
     *
     * @return boolean
     */
    public function getPredefined()
    {
        return $this->predefined;
    }

    public function __clone()
    {
        if($this->id) {
            $this->id = null;
        }
        $this->predefined = false;
        $this->emailJobs = new ArrayCollection();
        $this->createdAt = new \DateTime('now');
        if($this->file) {
            $this->file = clone $this->file;
        }

    }

    /**
     * Set includeInvoice
     *
     * @param boolean $includeInvoice
     *
     * @return Email
     */
    public function setIncludeInvoice($includeInvoice)
    {
        $this->includeInvoice = $includeInvoice;

        return $this;
    }

    /**
     * Get includeInvoice
     *
     * @return boolean
     */
    public function getIncludeInvoice()
    {
        return $this->includeInvoice;
    }

    /**
     * Set selectAllProducts
     *
     * @param boolean $selectAllProducts
     *
     * @return Email
     */
    public function setSelectAllProducts($selectAllProducts)
    {
        $this->selectAllProducts = $selectAllProducts;

        return $this;
    }

    /**
     * Get selectAllProducts
     *
     * @return boolean
     */
    public function getSelectAllProducts()
    {
        return $this->selectAllProducts;
    }

    /**
     * Set deliveryTime
     *
     * @param \DateTime $deliveryTime
     *
     * @return Email
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * Get deliveryTime
     *
     * @return \DateTime
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * Set autoResend
     *
     * @param boolean $autoResend
     *
     * @return Email
     */
    public function setAutoResend($autoResend)
    {
        $this->autoResend = $autoResend;

        return $this;
    }

    /**
     * Get autoResend
     *
     * @return boolean
     */
    public function getAutoResend()
    {
        return $this->autoResend;
    }

    /**
     * Set dateStartAutoResend
     *
     * @param \DateTime $dateStartAutoResend
     *
     * @return Email
     */
    public function setDateStartAutoResend($dateStartAutoResend)
    {
        $this->dateStartAutoResend = $dateStartAutoResend;

        return $this;
    }

    /**
     * Get dateStartAutoResend
     *
     * @return \DateTime
     */
    public function getDateStartAutoResend()
    {
        return $this->dateStartAutoResend;
    }

    /**
     * Set autoResendBeforeValue
     *
     * @param string $autoResendBeforeValue
     *
     * @return Email
     */
    public function setAutoResendBeforeValue($autoResendBeforeValue)
    {
        $this->autoResendBeforeValue = $autoResendBeforeValue;

        return $this;
    }

    /**
     * Get autoResendBeforeValue
     *
     * @return string
     */
    public function getAutoResendBeforeValue()
    {
        return $this->autoResendBeforeValue;
    }

    /**
     * Set autoResendBeforeUnit
     *
     * @param string $autoResendBeforeUnit
     *
     * @return Email
     */
    public function setAutoResendBeforeUnit($autoResendBeforeUnit)
    {
        $this->autoResendBeforeUnit = $autoResendBeforeUnit;

        return $this;
    }

    /**
     * Get autoResendBeforeUnit
     *
     * @return string
     */
    public function getAutoResendBeforeUnit()
    {
        return $this->autoResendBeforeUnit;
    }

    /**
     * Set autoResendAfterValue
     *
     * @param string $autoResendAfterValue
     *
     * @return Email
     */
    public function setAutoResendAfterValue($autoResendAfterValue)
    {
        $this->autoResendAfterValue = $autoResendAfterValue;

        return $this;
    }

    /**
     * Get autoResendAfterValue
     *
     * @return string
     */
    public function getAutoResendAfterValue()
    {
        return $this->autoResendAfterValue;
    }

    /**
     * Set autoResendAfterUnit
     *
     * @param string $autoResendAfterUnit
     *
     * @return Email
     */
    public function setAutoResendAfterUnit($autoResendAfterUnit)
    {
        $this->autoResendAfterUnit = $autoResendAfterUnit;

        return $this;
    }

    /**
     * Get autoResendAfterUnit
     *
     * @return string
     */
    public function getAutoResendAfterUnit()
    {
        return $this->autoResendAfterUnit;
    }

    /**
     * Set emailFolder
     *
     * @param \AMZcockpitDoctrine\Entity\EmailFolder $emailFolder
     *
     * @return Email
     */
    public function setEmailFolder(\AMZcockpitDoctrine\Entity\EmailFolder $emailFolder = null)
    {
        $this->emailFolder = $emailFolder;

        return $this;
    }

    /**
     * Get emailFolder
     *
     * @return \AMZcockpitDoctrine\Entity\EmailFolder
     */
    public function getEmailFolder()
    {
        return $this->emailFolder;
    }

    /**
     * Set important.
     *
     * @param bool $important
     *
     * @return Email
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important.
     *
     * @return bool
     */
    public function getImportant()
    {
        return $this->important;
    }
}
