<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EmailFolder
 *
 * @ORM\Table(name="emails_folders")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailFolderRepository")
 */
class EmailFolder
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="emailFolders")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Email", mappedBy="emailFolder", cascade={"persist"})
     */
    protected $emails;

    /**
     * @ORM\Column(name="predefined", type="boolean")
     */
    private $predefined = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->predefined = false;
        $this->emails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EmailFolder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return EmailFolder
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return EmailFolder
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     *
     * @return EmailFolder
     */
    public function addEmail(\AMZcockpitDoctrine\Entity\Email $email)
    {
        $this->emails[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     */
    public function removeEmail(\AMZcockpitDoctrine\Entity\Email $email)
    {
        $this->emails->removeElement($email);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Set predefined
     *
     * @param boolean $predefined
     *
     * @return EmailFolder
     */
    public function setPredefined($predefined)
    {
        $this->predefined = $predefined;

        return $this;
    }

    /**
     * Get predefined
     *
     * @return boolean
     */
    public function getPredefined()
    {
        return $this->predefined;
    }
}
