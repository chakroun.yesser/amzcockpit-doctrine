<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * EmailJob
 *
 * @ORM\Table(name="emails_jobs_schedule")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailJobRepository")
 * @UniqueEntity(
 *     fields={"email", "order"},
 *     errorPath="email",
 *     message="This email job is already programmed."
 * )
 */
class EmailJob
{
    const WAITING  = 'waiting';
    const PENDING  = 'pending';
    const CANCELED  = 'canceled';
    const COMPLETE = 'complete';

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="unsubscribe_id", type="string", nullable=true)
     */
    private $unsubscribeId;

    /**
     * @ORM\Column(name="subject", type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $subject;

    /**
     * @ORM\Column(name="_to", type="string")
     */
    private $to;

    /**
     * @ORM\Column(name="_cc", type="json_array", nullable=true)
     */
    private $cc;

    /**
     * @ORM\Column(name="body", type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $body;

    /**
     * @ORM\Column(name="_date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="_status", type="string")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", inversedBy="emailsJobs", cascade={"persist"})
     */
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\Email", inversedBy="emailJobs", cascade={"persist"})
     */
    protected $email;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\EmailBroadcast", inversedBy="emailJobs", cascade={"persist"})
     */
    protected $emailBroadcast;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\EmailBroadcastAdmin", inversedBy="emailJobs", cascade={"persist"})
     */
    protected $emailBroadcastAdmin;

    /**
     * @ORM\Column(name="is_opened", type="boolean")
     */
    private $isOpened = false;

    /**
     * @ORM\Column(name="auto_resend", type="boolean")
     */
    private $autoResend = false;

    /**
     * @ORM\Column(name="auto_resend_count", type="integer", options={"default" : 0})
     */
    private $autoResendCount;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailJobCountdown", mappedBy="emailJob", cascade={"persist","remove"})
     */
    protected $emailJobsCountsdown;

    /**
     * @ORM\Column(name="swift_mailer_id", type="string", nullable=true)
     */
    private $swiftMailerId;

    /**
     * @ORM\Column(name="resend_swift_mailer_id", type="string", nullable=true)
     */
    private $resendSwiftMailerId;

    public function __construct()
    {
        $this->status = self::WAITING;
        $this->isOpened = false;
        $this->autoResend = false;
        $this->autoResendCount = 0;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EmailJob
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    public function updateDate()
    {
        return $this->date = new \DateTime('now');
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return EmailJob
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return EmailJob
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set to
     *
     * @param string $to
     *
     * @return EmailJob
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }


    /**
     * Set order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return EmailJob
     */
    public function setOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     *
     * @return EmailJob
     */
    public function setEmail(\AMZcockpitDoctrine\Entity\Email $email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \AMZcockpitDoctrine\Entity\Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return EmailJob
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set isOpened
     *
     * @param boolean $isOpened
     *
     * @return EmailJob
     */
    public function setIsOpened($isOpened)
    {
        $this->isOpened = $isOpened;

        return $this;
    }

    /**
     * Get isOpened
     *
     * @return boolean
     */
    public function getIsOpened()
    {
        return $this->isOpened;
    }

    /**
     * Set emailBroadcast
     *
     * @param \AMZcockpitDoctrine\Entity\EmailBroadcast $emailBroadcast
     *
     * @return EmailJob
     */
    public function setEmailBroadcast(\AMZcockpitDoctrine\Entity\EmailBroadcast $emailBroadcast = null)
    {
        $this->emailBroadcast = $emailBroadcast;

        return $this;
    }

    /**
     * Get emailBroadcast
     *
     * @return \AMZcockpitDoctrine\Entity\EmailBroadcast
     */
    public function getEmailBroadcast()
    {
        return $this->emailBroadcast;
    }

    /**
     * Set swiftMailerId
     *
     * @param string $swiftMailerId
     *
     * @return EmailJob
     */
    public function setSwiftMailerId($swiftMailerId)
    {
        $this->swiftMailerId = $swiftMailerId;

        return $this;
    }

    /**
     * Get swiftMailerId
     *
     * @return string
     */
    public function getSwiftMailerId()
    {
        return $this->swiftMailerId;
    }

    /**
     * Set cc.
     *
     * @param array|null $cc
     *
     * @return EmailJob
     */
    public function setCc($cc = null)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * Get cc.
     *
     * @return array|null
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set autoResend
     *
     * @param boolean $autoResend
     *
     * @return EmailJob
     */
    public function setAutoResend($autoResend)
    {
        $this->autoResend = $autoResend;

        return $this;
    }

    /**
     * Get autoResend
     *
     * @return boolean
     */
    public function getAutoResend()
    {
        return $this->autoResend;
    }

    /**
     * Set autoResendCount
     *
     * @param integer $autoResendCount
     *
     * @return EmailJob
     */
    public function setAutoResendCount($autoResendCount)
    {
        $this->autoResendCount = $autoResendCount;

        return $this;
    }

    /**
     * Get autoResendCount
     *
     * @return integer
     */
    public function getAutoResendCount()
    {
        return $this->autoResendCount;
    }

    /**
     * Add emailJobsCountsdown
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJobCountdown $emailJobsCountsdown
     *
     * @return EmailJob
     */
    public function addEmailJobsCountsdown(\AMZcockpitDoctrine\Entity\EmailJobCountdown $emailJobsCountsdown)
    {
        $this->emailJobsCountsdown[] = $emailJobsCountsdown;

        return $this;
    }

    /**
     * Remove emailJobsCountsdown
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJobCountdown $emailJobsCountsdown
     */
    public function removeEmailJobsCountsdown(\AMZcockpitDoctrine\Entity\EmailJobCountdown $emailJobsCountsdown)
    {
        $this->emailJobsCountsdown->removeElement($emailJobsCountsdown);
    }

    /**
     * Get emailJobsCountsdown
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailJobsCountsdown()
    {
        return $this->emailJobsCountsdown;
    }

    /**
     * Set emailBroadcastAdmin
     *
     * @param \AMZcockpitDoctrine\Entity\EmailBroadcastAdmin $emailBroadcastAdmin
     *
     * @return EmailJob
     */
    public function setEmailBroadcastAdmin(\AMZcockpitDoctrine\Entity\EmailBroadcastAdmin $emailBroadcastAdmin = null)
    {
        $this->emailBroadcastAdmin = $emailBroadcastAdmin;

        return $this;
    }

    /**
     * Get emailBroadcastAdmin
     *
     * @return \AMZcockpitDoctrine\Entity\EmailBroadcastAdmin
     */
    public function getEmailBroadcastAdmin()
    {
        return $this->emailBroadcastAdmin;
    }

    /**
     * Set unsubscribeId
     *
     * @param string $unsubscribeId
     *
     * @return EmailJob
     */
    public function setUnsubscribeId($unsubscribeId)
    {
        $this->unsubscribeId = $unsubscribeId;

        return $this;
    }

    /**
     * Get unsubscribeId
     *
     * @return string
     */
    public function getUnsubscribeId()
    {
        return $this->unsubscribeId;
    }

    /**
     * Set resendSwiftMailerId.
     *
     * @param string|null $resendSwiftMailerId
     *
     * @return EmailJob
     */
    public function setResendSwiftMailerId($resendSwiftMailerId = null)
    {
        $this->resendSwiftMailerId = $resendSwiftMailerId;

        return $this;
    }

    /**
     * Get resendSwiftMailerId.
     *
     * @return string|null
     */
    public function getResendSwiftMailerId()
    {
        return $this->resendSwiftMailerId;
    }
}
