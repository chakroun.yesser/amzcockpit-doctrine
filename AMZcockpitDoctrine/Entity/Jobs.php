<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Jobs
 *
 * @ORM\Table(name="jobs_schedule")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\JobsRepository")
 */
class Jobs
{
    const NOT_YET  = 'not_yet';
    const WAITING  = 'waiting';
    const WORKING  = 'working';
    const COMPLETE = 'complete';
    const ERROR    = 'error';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="command", type="string")
     */
    private $command;

    /**
     * @ORM\Column(name="arguments", type="array")
     */
    private $arguments;

    /**
     * @ORM\Column(name="options", type="array")
     */
    private $options;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @ORM\Column(name="object", type="string", nullable=true)
     */
    private $object;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $store;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="jobs")
     */
    protected $user;

    /**
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    public function __construct()
    {
        $this->status = self::WAITING;
        $this->date = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set command
     *
     * @param string $command
     *
     * @return Jobs
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set arguments
     *
     * @param array $arguments
     *
     * @return Jobs
     */
    public function setArguments($arguments)
    {
        $this->arguments = $arguments;

        return $this;
    }

    /**
     * Get arguments
     *
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Set options
     *
     * @param array $options
     *
     * @return Jobs
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Jobs
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    public function updateDate()
    {
        return $this->date = new \DateTime('now');
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Jobs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set object.
     *
     * @param string|null $object
     *
     * @return Jobs
     */
    public function setObject($object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object.
     *
     * @return string|null
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set store.
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store|null $store
     *
     * @return Jobs
     */
    public function setStore(\AMZcockpitDoctrine\Entity\MWS\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Store|null
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set dateEnd.
     *
     * @param \DateTime|null $dateEnd
     *
     * @return Jobs
     */
    public function setDateEnd($dateEnd = null)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd.
     *
     * @return \DateTime|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set user.
     *
     * @param \AMZcockpitDoctrine\Entity\User|null $user
     *
     * @return Jobs
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AMZcockpitDoctrine\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
