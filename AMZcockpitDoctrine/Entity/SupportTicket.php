<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\SupportTicketStatutType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * SupportTicket
 *
 * @ORM\Table(name="support_tickets")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\SupportTicketRepository")
 */
class SupportTicket
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\SupportTicketStatutType")
     * @ORM\Column(name="statut", type="SupportTicketStatutType", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(name="subject", type="string")
     */
    private $subject;

    /**
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="supportTickets")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="assignedSupportTickets")
     * @ORM\JoinColumn(onDelete="SET NULL")
     *
     */
    protected $assignedUser;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\SupportTicketCategory", inversedBy="supportTickets")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\SupportTicketMessage", mappedBy="supportTicket", cascade={"persist", "remove"})
     */
    protected $messages;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist", "remove"})
     */
    protected $file;

    /**
     * @ORM\Column(name="date_cloture", type="datetime", nullable=true)
     */
    private $dateCloture;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->statut = SupportTicketStatutType::WAITING;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statut
     *
     * @param SupportTicketStatutType|string $statut
     *
     * @return SupportTicket
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return SupportTicketStatutType
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return SupportTicket
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return SupportTicket
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set assignedUser
     *
     * @param \AMZcockpitDoctrine\Entity\User $assignedUser
     *
     * @return SupportTicket
     */
    public function setAssignedUser(\AMZcockpitDoctrine\Entity\User $assignedUser = null)
    {
        $this->assignedUser = $assignedUser;

        return $this;
    }

    /**
     * Get assignedUser
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getAssignedUser()
    {
        return $this->assignedUser;
    }

    /**
     * Set category
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketCategory $category
     *
     * @return SupportTicket
     */
    public function setCategory(\AMZcockpitDoctrine\Entity\SupportTicketCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AMZcockpitDoctrine\Entity\SupportTicketCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set dateCloture
     *
     * @param \DateTime $dateCloture
     *
     * @return SupportTicket
     */
    public function setDateCloture($dateCloture)
    {
        $this->dateCloture = $dateCloture;

        return $this;
    }

    /**
     * Get dateCloture
     *
     * @return \DateTime
     */
    public function getDateCloture()
    {
        return $this->dateCloture;
    }

    /**
     * Add message
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketMessage $message
     *
     * @return SupportTicket
     */
    public function addMessage(\AMZcockpitDoctrine\Entity\SupportTicketMessage $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketMessage $message
     */
    public function removeMessage(\AMZcockpitDoctrine\Entity\SupportTicketMessage $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set createdBy
     *
     * @param \AMZcockpitDoctrine\Entity\User $createdBy
     *
     * @return SupportTicket
     */
    public function setCreatedBy(\AMZcockpitDoctrine\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set file
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return SupportTicket
     */
    public function setFile(\AMZcockpitDoctrine\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }
}
