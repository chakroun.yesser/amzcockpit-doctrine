<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\FrequencyType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * ComptabilityPurchase
 *
 * @ORM\Table(name="comptability_purchases")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\ComptabilityPurchaseRepository")
 */
class ComptabilityPurchase
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomIdGenerator")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reference;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateEnd;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\ComptabilitySupplier", inversedBy="achats")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $supplier;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="purchases")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="amount", type="string")
     */
    private $amount;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\FrequencyType")
     * @ORM\Column(name="frequency", type="FrequencyType", length=255, nullable=true)
     */
    private $frequency;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\ComptabilityPurchaseCategoryType")
     * @ORM\Column(name="category", type="ComptabilityPurchaseCategoryType", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist","remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $file;

    public function __construct()
    {
        $this->frequency = FrequencyType::NONE;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ComptabilityPurchase
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ComptabilityPurchase
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ComptabilityPurchase
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set frequency
     *
     * @param FrequencyType $frequency
     *
     * @return ComptabilityPurchase
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return FrequencyType
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ComptabilityPurchase
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set supplier
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilitySupplier $supplier
     *
     * @return ComptabilityPurchase
     */
    public function setSupplier(\AMZcockpitDoctrine\Entity\ComptabilitySupplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \AMZcockpitDoctrine\Entity\ComptabilitySupplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return ComptabilityPurchase
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEnd.
     *
     * @param \DateTime|null $dateEnd
     *
     * @return ComptabilityPurchase
     */
    public function setDateEnd($dateEnd = null)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd.
     *
     * @return \DateTime|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set file
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return ComptabilityPurchase
     */
    public function setFile(\AMZcockpitDoctrine\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set reference.
     *
     * @param string|null $reference
     *
     * @return ComptabilityPurchase
     */
    public function setReference($reference = null)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string|null
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set category.
     *
     * @param ComptabilityPurchaseCategoryType|null $category
     *
     * @return ComptabilityPurchase
     */
    public function setCategory($category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return ComptabilityPurchaseCategoryType|null
     */
    public function getCategory()
    {
        return $this->category;
    }
}
