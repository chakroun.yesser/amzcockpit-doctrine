<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\EmailBroadcastStatutType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Email
 *
 * @ORM\Table(name="emails_broadcast_admin")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailBroadcastAdminRepository")
 */
class EmailBroadcastAdmin
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\EmailBroadcastStatutType")
     * @ORM\Column(name="status", type="EmailBroadcastStatutType", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="title", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $title;

    /**
     * @ORM\Column(name="subject", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $subject;

    /**
     * @ORM\Column(name="custom_emails", type="array", nullable=true)
     */
    private $customEmails;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\User")
     * @ORM\JoinTable(name="emails_broadcast_admin_users")
     */
    protected $users;

    /**
     * @ORM\Column(name="additional_users", type="json_array", nullable=true)
     */
    private $additionalUsers;

    /**
     * @ORM\Column(name="body", type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $body;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist", "remove"})
     */
    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User")
     */
    protected $createdBy;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\EmailBroadcastUsersType")
     * @ORM\Column(name="users_type", type="EmailBroadcastUsersType", length=255, nullable=true)
     */
    private $usersType;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailJob", mappedBy="emailBroadcastAdmin", cascade={"persist","remove"})
     */
    protected $emailJobs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = EmailBroadcastStatutType::DRAFT;
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return EmailBroadcastAdmin
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return EmailBroadcastAdmin
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return EmailBroadcastAdmin
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set customEmails
     *
     * @param array $customEmails
     *
     * @return EmailBroadcastAdmin
     */
    public function setCustomEmails($customEmails)
    {
        $this->customEmails = $customEmails;

        return $this;
    }

    /**
     * Get customEmails
     *
     * @return array
     */
    public function getCustomEmails()
    {
        return $this->customEmails;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return EmailBroadcastAdmin
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Add user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return EmailBroadcastAdmin
     */
    public function addUser(\AMZcockpitDoctrine\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     */
    public function removeUser(\AMZcockpitDoctrine\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set file
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return EmailBroadcastAdmin
     */
    public function setFile(\AMZcockpitDoctrine\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set createdBy
     *
     * @param \AMZcockpitDoctrine\Entity\User $createdBy
     *
     * @return EmailBroadcastAdmin
     */
    public function setCreatedBy(\AMZcockpitDoctrine\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set usersType
     *
     * @param EmailBroadcastUsersType $usersType
     *
     * @return EmailBroadcastAdmin
     */
    public function setUsersType($usersType)
    {
        $this->usersType = $usersType;

        return $this;
    }

    /**
     * Get usersType
     *
     * @return EmailBroadcastUsersType
     */
    public function getUsersType()
    {
        return $this->usersType;
    }

    /**
     * Add emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     *
     * @return EmailBroadcastAdmin
     */
    public function addEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob)
    {
        $this->emailJobs[] = $emailJob;

        return $this;
    }

    /**
     * Remove emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     */
    public function removeEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob)
    {
        $this->emailJobs->removeElement($emailJob);
    }

    /**
     * Get emailJobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailJobs()
    {
        return $this->emailJobs;
    }

    /**
     * Set additionalUsers
     *
     * @param array $additionalUsers
     *
     * @return EmailBroadcastAdmin
     */
    public function setAdditionalUsers($additionalUsers)
    {
        $this->additionalUsers = $additionalUsers;

        return $this;
    }

    /**
     * Get additionalUsers
     *
     * @return array
     */
    public function getAdditionalUsers()
    {
        return $this->additionalUsers;
    }

    public function getTargets() {
        $return = $this->additionalUsers;
        foreach ($this->getUsers() as $user) {
            if(!in_array($user->getEmail(),$return)) {
                $return[] = $user->getEmail();
            }
        }
        return $return;
    }
}
