<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Email
 *
 * @ORM\Table(name="emails_broadcast")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailBroadcastRepository")
 */
class EmailBroadcast
{
    const DRAFT       = 'draft';
    const SENT        = 'sent';
    const TO_SEND     = 'to_send';
    const WORKING_SEND = 'working_send';
    const RESENT      = 'resent';
    const ARCHIVED    = 'archived';

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @ORM\Column(name="title", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $title;

    /**
     * @ORM\Column(name="subject", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $subject;

    /**
     * @ORM\Column(name="marketplaces", type="array", nullable=true)
     */
    private $marketPlaces;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(name="date_from", type="datetime", nullable=true)
     */
    private $dateFrom;

    /**
     * @ORM\Column(name="date_to", type="datetime", nullable=true)
     */
    private $dateTo;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="emails")
     * @ORM\JoinTable(name="emails_broadcast_products",
     *      joinColumns={@ORM\JoinColumn(name="email_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $products;

    /**
     * @ORM\Column(name="body", type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $body;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist"})
     */
    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="emailsBroadcast")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailJob", mappedBy="emailBroadcast", cascade={"persist","remove"})
     */
    protected $emailJobs;

    /**
     * @ORM\Column(name="includeInvoice", type="boolean")
     */
    private $includeInvoice = false;

    /**
     * @ORM\Column(name="targetCount", type="string")
     */
    private $targetCount;

    /**
     * @ORM\Column(name="postalCode", type="array", nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(name="city", type="array", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(name="country", type="array", nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(name="select_all_products", type="boolean")
     */
    private $selectAllProducts = false;

    /**
     * @ORM\Column(name="auto_resend", type="boolean")
     */
    private $autoResend = false;

    /**
     * @ORM\Column(name="dateStartAutoResend", type="datetime", nullable=true)
     */
    private $dateStartAutoResend;

    /**
     * @ORM\Column(name="autoResendBeforeValue", type="string", nullable=true)
     */
    private $autoResendBeforeValue;

    /**
     * @ORM\Column(name="autoResendBeforeUnit", type="string", nullable=true)
     */
    private $autoResendBeforeUnit;

    /**
     * @ORM\Column(name="autoResendAfterValue", type="string", nullable=true)
     */
    private $autoResendAfterValue;

    /**
     * @ORM\Column(name="autoResendAfterUnit", type="string", nullable=true)
     */
    private $autoResendAfterUnit;

    /**
     * @ORM\Column(name="important", type="boolean")
     */
    private $important = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date      = new \DateTime("now");
        $this->status    = self::DRAFT;
        $this->products  = new \Doctrine\Common\Collections\ArrayCollection();
        $this->emailJobs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return EmailBroadcast
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set marketPlaces
     *
     * @param array $marketPlaces
     *
     * @return EmailBroadcast
     */
    public function setMarketPlaces($marketPlaces)
    {
        $this->marketPlaces = $marketPlaces;

        return $this;
    }

    /**
     * Get marketPlaces
     *
     * @return array
     */
    public function getMarketPlaces()
    {
        return $this->marketPlaces;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     *
     * @return EmailBroadcast
     */
    public function setDateFrom($dateFrom)
    {
        if(is_string($dateFrom))
            $dateFrom   =\DateTime::createFromFormat('d/m/Y', $dateFrom);
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return EmailBroadcast
     */
    public function setDateTo($dateTo)
    {
        if(is_string($dateTo))
            $dateTo   =\DateTime::createFromFormat('d/m/Y', $dateTo);
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return EmailBroadcast
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set includeInvoice
     *
     * @param boolean $includeInvoice
     *
     * @return EmailBroadcast
     */
    public function setIncludeInvoice($includeInvoice)
    {
        $this->includeInvoice = $includeInvoice;

        return $this;
    }

    /**
     * Get includeInvoice
     *
     * @return boolean
     */
    public function getIncludeInvoice()
    {
        return $this->includeInvoice;
    }

    /**
     * Add product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return EmailBroadcast
     */
    public function addProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     */
    public function removeProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set file
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return EmailBroadcast
     */
    public function setFile(\AMZcockpitDoctrine\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return EmailBroadcast
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     *
     * @return EmailBroadcast
     */
    public function addEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob)
    {
        $this->emailJobs[] = $emailJob;

        return $this;
    }

    /**
     * Remove emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     */
    public function removeEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob)
    {
        $this->emailJobs->removeElement($emailJob);
    }

    /**
     * Get emailJobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailJobs()
    {
        return $this->emailJobs;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return EmailBroadcast
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return EmailBroadcast
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set targetCount
     *
     * @param string $targetCount
     *
     * @return EmailBroadcast
     */
    public function setTargetCount($targetCount)
    {
        $this->targetCount = $targetCount;

        return $this;
    }

    /**
     * Get targetCount
     *
     * @return string
     */
    public function getTargetCount()
    {
        return $this->targetCount;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return EmailBroadcast
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set postalCode
     *
     * @param array $postalCode
     *
     * @return EmailBroadcast
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return array
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param array $city
     *
     * @return EmailBroadcast
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return array
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param array $country
     *
     * @return EmailBroadcast
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return array
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set selectAllProducts
     *
     * @param boolean $selectAllProducts
     *
     * @return EmailBroadcast
     */
    public function setSelectAllProducts($selectAllProducts)
    {
        $this->selectAllProducts = $selectAllProducts;

        return $this;
    }

    /**
     * Get selectAllProducts
     *
     * @return boolean
     */
    public function getSelectAllProducts()
    {
        return $this->selectAllProducts;
    }

    /**
     * Set autoResend
     *
     * @param boolean $autoResend
     *
     * @return EmailBroadcast
     */
    public function setAutoResend($autoResend)
    {
        $this->autoResend = $autoResend;

        return $this;
    }

    /**
     * Get autoResend
     *
     * @return boolean
     */
    public function getAutoResend()
    {
        return $this->autoResend;
    }

    /**
     * Set dateStartAutoResend
     *
     * @param \DateTime $dateStartAutoResend
     *
     * @return EmailBroadcast
     */
    public function setDateStartAutoResend($dateStartAutoResend)
    {
        $this->dateStartAutoResend = $dateStartAutoResend;

        return $this;
    }

    /**
     * Get dateStartAutoResend
     *
     * @return \DateTime
     */
    public function getDateStartAutoResend()
    {
        return $this->dateStartAutoResend;
    }

    /**
     * Set autoResendBeforeValue
     *
     * @param string $autoResendBeforeValue
     *
     * @return EmailBroadcast
     */
    public function setAutoResendBeforeValue($autoResendBeforeValue)
    {
        $this->autoResendBeforeValue = $autoResendBeforeValue;

        return $this;
    }

    /**
     * Get autoResendBeforeValue
     *
     * @return string
     */
    public function getAutoResendBeforeValue()
    {
        return $this->autoResendBeforeValue;
    }

    /**
     * Set autoResendBeforeUnit
     *
     * @param string $autoResendBeforeUnit
     *
     * @return EmailBroadcast
     */
    public function setAutoResendBeforeUnit($autoResendBeforeUnit)
    {
        $this->autoResendBeforeUnit = $autoResendBeforeUnit;

        return $this;
    }

    /**
     * Get autoResendBeforeUnit
     *
     * @return string
     */
    public function getAutoResendBeforeUnit()
    {
        return $this->autoResendBeforeUnit;
    }

    /**
     * Set autoResendAfterValue
     *
     * @param string $autoResendAfterValue
     *
     * @return EmailBroadcast
     */
    public function setAutoResendAfterValue($autoResendAfterValue)
    {
        $this->autoResendAfterValue = $autoResendAfterValue;

        return $this;
    }

    /**
     * Get autoResendAfterValue
     *
     * @return string
     */
    public function getAutoResendAfterValue()
    {
        return $this->autoResendAfterValue;
    }

    /**
     * Set autoResendAfterUnit
     *
     * @param string $autoResendAfterUnit
     *
     * @return EmailBroadcast
     */
    public function setAutoResendAfterUnit($autoResendAfterUnit)
    {
        $this->autoResendAfterUnit = $autoResendAfterUnit;

        return $this;
    }

    /**
     * Get autoResendAfterUnit
     *
     * @return string
     */
    public function getAutoResendAfterUnit()
    {
        return $this->autoResendAfterUnit;
    }

    /**
     * Set important.
     *
     * @param bool $important
     *
     * @return EmailBroadcast
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important.
     *
     * @return bool
     */
    public function getImportant()
    {
        return $this->important;
    }
}
