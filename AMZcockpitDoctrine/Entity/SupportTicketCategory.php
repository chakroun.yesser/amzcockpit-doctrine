<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SupportTicketCategory
 *
 * @ORM\Table(name="support_tickets_categories")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\SupportTicketCategoryRepository")
 */
class SupportTicketCategory
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\SupportTicket", mappedBy="category")
     */
    protected $supportTickets;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User")
     */
    protected $createdBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->supportTickets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return SupportTicketCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add supportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $supportTicket
     *
     * @return SupportTicketCategory
     */
    public function addSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $supportTicket)
    {
        $this->supportTickets[] = $supportTicket;

        return $this;
    }

    /**
     * Remove supportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $supportTicket
     */
    public function removeSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $supportTicket)
    {
        $this->supportTickets->removeElement($supportTicket);
    }

    /**
     * Get supportTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupportTickets()
    {
        return $this->supportTickets;
    }

    /**
     * Set createdBy
     *
     * @param \AMZcockpitDoctrine\Entity\User $createdBy
     *
     * @return SupportTicketCategory
     */
    public function setCreatedBy(\AMZcockpitDoctrine\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
