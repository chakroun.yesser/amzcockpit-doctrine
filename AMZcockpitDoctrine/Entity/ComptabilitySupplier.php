<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ComptabilitySupplier
 *
 * @ORM\Table(name="comptability_supplier")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\ComptabilitySupplierRepository")
 */
class ComptabilitySupplier
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true, length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=255, nullable=true)
     */
    private $legalForm;

    /**
     * @ORM\Column(type="phone_number", nullable=true, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="phone_number", nullable=true, nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min="2",
     *     minMessage="L'adresse doit contenir {{ limit }} caractères au minimum."
     *     )
     */
    private $adresse;

    /**
     * @ORM\Column(name="code_postal", type="string", length=10, nullable=true)
     * @Assert\Type(
     *     type="numeric",
     *     message="Ce code postal n'est pas valide."
     *     )
     * @Assert\Length(
     *     min="4",
     *     max="6",
     *     minMessage="Le code postal doit contenir {{ limit }} caractères au minimum.",
     *     maxMessage="Le code postal doit contenir {{ limit }} caractères au maximum."
     *     )
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     * @Assert\Length(
     *      min="2",
     *      minMessage="Le ville doit contenir {{ limit }} caractères au minimum."
     *      )
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $country;

    /**
     * @ORM\Column(type="string", nullable=true, length=4)
     */
    private $tva;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="purchases")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\ComptabilityPurchase", mappedBy="supplier")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $purchases;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->country = 'FR';
        $this->purchases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ComptabilitySupplier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set legalForm
     *
     * @param string $legalForm
     *
     * @return ComptabilitySupplier
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    /**
     * Get legalForm
     *
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * Set phone
     *
     * @param phone_number $phone
     *
     * @return ComptabilitySupplier
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return phone_number
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param phone_number $fax
     *
     * @return ComptabilitySupplier
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return phone_number
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ComptabilitySupplier
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return ComptabilitySupplier
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return ComptabilitySupplier
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return ComptabilitySupplier
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return ComptabilitySupplier
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set user.
     *
     * @param \AMZcockpitDoctrine\Entity\User|null $user
     *
     * @return ComptabilitySupplier
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AMZcockpitDoctrine\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add purchase.
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilityPurchase $purchase
     *
     * @return ComptabilitySupplier
     */
    public function addPurchase(\AMZcockpitDoctrine\Entity\ComptabilityPurchase $purchase)
    {
        $this->purchases[] = $purchase;

        return $this;
    }

    /**
     * Remove purchase.
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilityPurchase $purchase
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePurchase(\AMZcockpitDoctrine\Entity\ComptabilityPurchase $purchase)
    {
        return $this->purchases->removeElement($purchase);
    }

    /**
     * Get purchases.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Set tva.
     *
     * @param string|null $tva
     *
     * @return ComptabilitySupplier
     */
    public function setTva($tva = null)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva.
     *
     * @return string|null
     */
    public function getTva()
    {
        return $this->tva;
    }
}
