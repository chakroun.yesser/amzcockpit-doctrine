<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CurrencyExchange
 *
 * @ORM\Table(name="currency_exchange")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\CurrencyExchangeRepository")
 *  * @UniqueEntity(
 *     fields={"fromCurrency", "toCurrency"},
 *     errorPath="value"
 * )
 */
class CurrencyExchange
{
    const CURRENCY_CODES = [
        'GBP',
        'EUR',
        'USD',
        'JPY',
        'INR',
        'MXN',
        'CND',
        'RON',
        'SEK',
        'BGN',
        'DKK',
        'HUF',
        'LTL',
        'PLN',
        'CZK',
        'LVL'
    ];

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="fromCurrency", type="string", length=5)
     * @ORM\Id
     */
    private $fromCurrency;

    /**
     * @var int
     *
     * @ORM\Column(name="toCurrency", type="string", length=5)
     * @ORM\Id
     */
    private $toCurrency;

    /**
     * @ORM\Column(name="value", type="float")
     */
    public $value;

    public function __construct($fromCurrency, $toCurrency)
    {
        $this->fromCurrency = $fromCurrency;
        $this->toCurrency = $toCurrency;
    }

    /**
     * Set fromCurrency.
     *
     * @param string $fromCurrency
     *
     * @return CurrencyExchange
     */
    public function setFromCurrency($fromCurrency)
    {
        $this->fromCurrency = $fromCurrency;

        return $this;
    }

    /**
     * Get fromCurrency.
     *
     * @return string
     */
    public function getFromCurrency()
    {
        return $this->fromCurrency;
    }

    /**
     * Set toCurrency.
     *
     * @param string $toCurrency
     *
     * @return CurrencyExchange
     */
    public function setToCurrency($toCurrency)
    {
        $this->toCurrency = $toCurrency;

        return $this;
    }

    /**
     * Get toCurrency.
     *
     * @return string
     */
    public function getToCurrency()
    {
        return $this->toCurrency;
    }

    /**
     * Set value.
     *
     * @param float $value
     *
     * @return CurrencyExchange
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
}
