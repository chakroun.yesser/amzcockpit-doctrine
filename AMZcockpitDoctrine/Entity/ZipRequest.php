<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ZipRequest
 *
 * @ORM\Table(name="zip_request")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\ZipRequestRepository")
 */
class ZipRequest
{
    const WAITING    = 'waiting';
    const WORKING    = 'working';
    const COMPLETE   = 'complete';
    const CANCELED   = 'canceled';
    const ERROR      = 'error';

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @ORM\Column(name="_from", type="datetime", nullable=true)
     */
    private $from;

    /**
     * @ORM\Column(name="_to", type="datetime", nullable=true)
     */
    private $to;

    /**
     * @ORM\Column(name="ordersCount", type="integer", nullable=true)
     */
    private $ordersCount;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", inversedBy="zipRequests")
     */
    protected $store;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="zipRequests")
     */
    protected $user;

    /**
     * @ORM\Column(name="marketplaces", type="json_array", nullable=true)
     */
    private $marketPlaces;

    /**
     * @ORM\Column(name="zips", type="json_array", nullable=true)
     */
    private $zips;

    public function __construct()
    {
        $this->status = self::WAITING;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ZipRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set from
     *
     * @param \DateTime $from
     *
     * @return ZipRequest
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        if(!$this->from)
            return null;
        return $this->from->format('Y-m-d').' 00:00:00';
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFromDateTime()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     *
     * @return ZipRequest
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getTo()
    {
        if(!$this->to)
            return null;
        return $this->to->format('Y-m-d').' 23:59:59';
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getToDateTime()
    {
        return $this->to;
    }

    /**
     * Set store
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store $store
     *
     * @return ZipRequest
     */
    public function setStore(\AMZcockpitDoctrine\Entity\MWS\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return ZipRequest
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ordersCount
     *
     * @param integer $ordersCount
     *
     * @return ZipRequest
     */
    public function setOrdersCount($ordersCount)
    {
        $this->ordersCount = $ordersCount;

        return $this;
    }

    /**
     * Get ordersCount
     *
     * @return integer
     */
    public function getOrdersCount()
    {
        return $this->ordersCount;
    }

    /**
     * Set marketPlaces
     *
     * @param array $marketPlaces
     *
     * @return ZipRequest
     */
    public function setMarketPlaces($marketPlaces)
    {
        $this->marketPlaces = $marketPlaces;

        return $this;
    }

    /**
     * Get marketPlaces
     *
     * @return array
     */
    public function getMarketPlaces()
    {
        return $this->marketPlaces;
    }

    /**
     * Set zips
     *
     * @param array $zips
     *
     * @return ZipRequest
     */
    public function setZips($zips)
    {
        $this->zips = $zips;

        return $this;
    }

    /**
     * Get zips
     *
     * @return array
     */
    public function getZips()
    {
        return $this->zips;
    }
}
