<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * KeywordTrackingInstance
 *
 * @ORM\Table(name="keywords_tracking_instance")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\KeywordTrackingInstanceRepository")
 */
class KeywordTrackingInstance
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="_date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sponsored;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAmazonChoice;

    /**
     * @ORM\Column(name="ranking", type="integer")
     */
    private $ranking;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\KeywordTrackingItem", inversedBy="instances")
     */
    protected $keywordTrackingItem;

    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return KeywordTrackingInstance
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sponsored
     *
     * @param boolean $sponsored
     *
     * @return KeywordTrackingInstance
     */
    public function setSponsored($sponsored)
    {
        $this->sponsored = $sponsored;

        return $this;
    }

    /**
     * Get sponsored
     *
     * @return boolean
     */
    public function getSponsored()
    {
        return $this->sponsored;
    }

    /**
     * Set ranking
     *
     * @param integer $ranking
     *
     * @return KeywordTrackingInstance
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;

        return $this;
    }

    /**
     * Get ranking
     *
     * @return integer
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * Set isAmazonChoice
     *
     * @param boolean $isAmazonChoice
     *
     * @return KeywordTrackingInstance
     */
    public function setIsAmazonChoice($isAmazonChoice)
    {
        $this->isAmazonChoice = $isAmazonChoice;

        return $this;
    }

    /**
     * Get isAmazonChoice
     *
     * @return boolean
     */
    public function getIsAmazonChoice()
    {
        return $this->isAmazonChoice;
    }

    /**
     * Set keywordTrackingItem
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTrackingItem $keywordTrackingItem
     *
     * @return KeywordTrackingInstance
     */
    public function setKeywordTrackingItem(\AMZcockpitDoctrine\Entity\KeywordTrackingItem $keywordTrackingItem = null)
    {
        $this->keywordTrackingItem = $keywordTrackingItem;

        return $this;
    }

    /**
     * Get keywordTrackingItem
     *
     * @return \AMZcockpitDoctrine\Entity\KeywordTrackingItem
     */
    public function getKeywordTrackingItem()
    {
        return $this->keywordTrackingItem;
    }
}
