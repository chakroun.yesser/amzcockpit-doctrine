<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\AudienceStatutType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Audience
 *
 * @ORM\Table(name="audiences", options={"collate":"utf8_general_ci", "charset":"utf8", "engine"="MyISAM"})
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\AudienceRepository")
 */
class Audience
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomIdGenerator")
     */
    private $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\AudienceStatutType")
     * @ORM\Column(name="statut", type="AudienceStatutType", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(name="select_all_products", type="boolean")
     */
    private $selectAllProducts = false;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="audiences")
     * @ORM\JoinTable(name="audiences_products",
     *      joinColumns={@ORM\JoinColumn(name="audience_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     * )
     */
    protected $products;

    /**
     * @ORM\Column(name="marketplaces", type="array", nullable=true)
     */
    private $marketPlaces;

    /**
     * @ORM\Column(name="date_from", type="datetime", nullable=true)
     */
    private $dateFrom;

    /**
     * @ORM\Column(name="date_to", type="datetime", nullable=true)
     */
    private $dateTo;

    /**
     * @ORM\Column(name="country", type="array", nullable=true)
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Customer", inversedBy="audiences")
     * @ORM\JoinTable(name="audiences_customers",
     *      joinColumns={@ORM\JoinColumn(name="audience_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $customers;

    /**
     * @ORM\Column(name="targetCount", type="string")
     */
    private $targetCount;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="audiences")
     */
    protected $user;

    public function __construct()
    {
        $this->statut = AudienceStatutType::DRAFT;
        $this->customers = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     *
     * @return Audience
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set statut
     *
     * @param AudienceStatutType|string $statut
     *
     * @return Audience
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return AudienceStatutType
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set marketPlaces
     *
     * @param array $marketPlaces
     *
     * @return Audience
     */
    public function setMarketPlaces($marketPlaces)
    {
        $this->marketPlaces = $marketPlaces;

        return $this;
    }

    /**
     * Get marketPlaces
     *
     * @return array
     */
    public function getMarketPlaces()
    {
        return $this->marketPlaces;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     *
     * @return Audience
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return Audience
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set country
     *
     * @param array $country
     *
     * @return Audience
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return array
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set targetCount
     *
     * @param string $targetCount
     *
     * @return Audience
     */
    public function setTargetCount($targetCount)
    {
        $this->targetCount = $targetCount;

        return $this;
    }

    /**
     * Get targetCount
     *
     * @return string
     */
    public function getTargetCount()
    {
        return $this->targetCount;
    }

    /**
     * Add product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return Audience
     */
    public function addProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     */
    public function removeProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add customer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $customer
     *
     * @return Audience
     */
    public function addCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $customer)
    {
        if(!$this->getCustomers()->contains($customer))
            $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $customer
     */
    public function removeCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Set selectAllProducts
     *
     * @param boolean $selectAllProducts
     *
     * @return Audience
     */
    public function setSelectAllProducts($selectAllProducts)
    {
        $this->selectAllProducts = $selectAllProducts;

        return $this;
    }

    /**
     * Get selectAllProducts
     *
     * @return boolean
     */
    public function getSelectAllProducts()
    {
        return $this->selectAllProducts;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return Audience
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Audience
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Audience
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
