<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\PaymentMethodType;
use AMZcockpitDoctrine\DBAL\Types\PaymentStatusType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Payment
 *
 * @ORM\Table(name="payments")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomIdGenerator")
     */
    private $id;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\PaymentMethodType")
     * @ORM\Column(name="method", type="PaymentMethodType", length=255, nullable=false)
     */
    private $method;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="payments")
     */
    protected $user;

    /**
     * @ORM\Column(name="next_fetch_date", type="datetime", nullable=true)
     */
    private $nextFetchDate;

    /**
     * @ORM\Column(name="stripe_charge_id", type="string", nullable=true)
     */
    private $stripeChargeId;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\PaymentStatusType")
     * @ORM\Column(name="status", type="PaymentStatusType", length=255, nullable=false)
     */
    private $status;

    public function __construct()
    {
        $this->method = PaymentMethodType::SEPA;
        $this->status = PaymentStatusType::PENDING;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method.
     *
     * @param PaymentMethodType $method
     *
     * @return Payment
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method.
     *
     * @return PaymentMethodType
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set nextFetchDate.
     *
     * @param \DateTime|null $nextFetchDate
     *
     * @return Payment
     */
    public function setNextFetchDate($nextFetchDate = null)
    {
        $this->nextFetchDate = $nextFetchDate;

        return $this;
    }

    /**
     * Get nextFetchDate.
     *
     * @return \DateTime|null
     */
    public function getNextFetchDate()
    {
        return $this->nextFetchDate;
    }

    /**
     * Set stripeChargeId.
     *
     * @param string|null $stripeChargeId
     *
     * @return Payment
     */
    public function setStripeChargeId($stripeChargeId = null)
    {
        $this->stripeChargeId = $stripeChargeId;

        return $this;
    }

    /**
     * Get stripeChargeId.
     *
     * @return string|null
     */
    public function getStripeChargeId()
    {
        return $this->stripeChargeId;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Payment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user.
     *
     * @param \AMZcockpitDoctrine\Entity\User|null $user
     *
     * @return Payment
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AMZcockpitDoctrine\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
