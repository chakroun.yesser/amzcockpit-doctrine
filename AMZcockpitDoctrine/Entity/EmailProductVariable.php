<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Email
 *
 * @ORM\Table(name="emails_products_variables")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailProductVariableRepository")
 */
class EmailProductVariable
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\EmailVariable", inversedBy="emailsProductsVariables")
     */
    protected $emailVariable;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="emailsProductVariables")
     */
    protected $product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return EmailProductVariable
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set emailVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailVariable $emailVariable
     *
     * @return EmailProductVariable
     */
    public function setEmailVariable(\AMZcockpitDoctrine\Entity\EmailVariable $emailVariable = null)
    {
        $this->emailVariable = $emailVariable;

        return $this;
    }

    /**
     * Get emailVariable
     *
     * @return \AMZcockpitDoctrine\Entity\EmailVariable
     */
    public function getEmailVariable()
    {
        return $this->emailVariable;
    }

    /**
     * Set product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return EmailProductVariable
     */
    public function setProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
