<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\FrequencyType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * ComptabilitySellLine
 *
 * @ORM\Table(name="comptability_sell_lines")
 * @ORM\Entity()
 */
class ComptabilitySellLine
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\ComptabilitySell", inversedBy="lines")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $comptabilitySell;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(name="quantity", type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(name="amount_ht", type="string")
     */
    private $amountHT;

    /**
     * @ORM\Column(name="amount", type="string")
     */
    private $amount;

    /**
     * @ORM\Column(name="discount", type="float", nullable=true)
     */
    private $discount;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return ComptabilitySellLine
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return ComptabilitySellLine
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set reference.
     *
     * @param string|null $reference
     *
     * @return ComptabilitySellLine
     */
    public function setReference($reference = null)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string|null
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set quantity.
     *
     * @param float $quantity
     *
     * @return ComptabilitySellLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set amountHT.
     *
     * @param string $amountHT
     *
     * @return ComptabilitySellLine
     */
    public function setAmountHT($amountHT)
    {
        $this->amountHT = $amountHT;

        return $this;
    }

    /**
     * Get amountHT.
     *
     * @return string
     */
    public function getAmountHT()
    {
        return $this->amountHT;
    }

    /**
     * Set amount.
     *
     * @param string $amount
     *
     * @return ComptabilitySellLine
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set discount.
     *
     * @param float $discount
     *
     * @return ComptabilitySellLine
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set comptabilitySell.
     *
     * @param \AMZcockpitDoctrine\Entity\ComptabilitySell|null $comptabilitySell
     *
     * @return ComptabilitySellLine
     */
    public function setComptabilitySell(\AMZcockpitDoctrine\Entity\ComptabilitySell $comptabilitySell = null)
    {
        $this->comptabilitySell = $comptabilitySell;

        return $this;
    }

    /**
     * Get comptabilitySell.
     *
     * @return \AMZcockpitDoctrine\Entity\ComptabilitySell|null
     */
    public function getComptabilitySell()
    {
        return $this->comptabilitySell;
    }
}
