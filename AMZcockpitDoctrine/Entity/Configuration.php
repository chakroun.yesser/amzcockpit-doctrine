<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Configuration
 *
 * @ORM\Table(name="configurations")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\ConfigurationRepository")
 */
class Configuration
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @ORM\Column(name="process", type="boolean")
     */
    private $process = false;

    /**
     * @ORM\Column(name="process_2", type="boolean")
     */
    private $process2 = false;

    /**
     * @ORM\Column(name="process_reviews", type="boolean")
     */
    private $processReviews = false;

    /**
     * @ORM\Column(name="process_products", type="boolean")
     */
    private $processProducts = false;

    /**
     * @ORM\Column(name="process_payment", type="boolean")
     */
    private $processPayment = false;

    /**
     * @ORM\Column(name="emails", type="boolean")
     */
    private $emails = false;

    /**
     * @ORM\Column(name="emails_resend", type="boolean")
     */
    private $emailsRensend = false;

    /**
     * @ORM\Column(name="emails_broadcast", type="boolean")
     */
    private $emailsBroascast = false;

    /**
     * @ORM\Column(name="payment", type="boolean")
     */
    private $payment = false;

    /**
     * @ORM\Column(name="last_email_broadcast_exec", type="datetime", nullable=true)
     */
    private $lastEmailBroadcastExecutionDate;

    /**
     * @ORM\Column(name="last_email_exec", type="datetime", nullable=true)
     */
    private $lastEmailExecutionDate;

    /**
     * @ORM\Column(name="last_email_resend_exec", type="datetime", nullable=true)
     */
    private $lastEmailResendExecutionDate;

    /**
     * @ORM\Column(name="last_reviews_exec", type="datetime", nullable=true)
     */
    private $lastReviewsExecutionDate;

    /**
     * @ORM\Column(name="last_product_update", type="datetime", nullable=true)
     */
    private $lastProductUpdateDate;

    /**
     * @ORM\Column(name="last_feedbacks_update", type="datetime", nullable=true)
     */
    private $lastFeedbacksUpdateDate;

    /**
     * @ORM\Column(name="last_inventory_update", type="datetime", nullable=true)
     */
    private $lastInventoryUpdateDate;

    /**
     * @ORM\Column(name="last_payment_exec", type="datetime", nullable=true)
     */
    private $lastPaymentExecutionDate;

    /**
     * @ORM\Column(name="quickbook_access_token", type="text", nullable=true)
     */
    private $quickbookAccessToken;

    /**
     * @ORM\Column(name="quickbook_refresh_token", type="text", nullable=true)
     */
    private $quickbookRefreshToken;

    /**
     * @ORM\Column(name="quickbook_access_token_expire_date", type="datetime", nullable=true)
     */
    private $quickbookAccessTokenExpireDate;

    /**
     * @ORM\Column(name="quickbook_refresh_token_expire_date", type="datetime", nullable=true)
     */
    private $quickbookRefreshTokenExpireDate;

    /**
     * @ORM\Column(name="unsubscribe_users", type="json_array", nullable=true)
     */
    private $unsubscribeUsers;

    /**
     * @ORM\Column(name="fillfilement_centers_notified", type="json_array", nullable=true)
     */
    private $fullfilementCentersNotified;

    public function __construct()
    {
        $this->lastUpdate = new \DateTime('now');
        $this->process    = false;
        $this->processReviews    = false;
        $this->emails    = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Configuration
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set process
     *
     * @param boolean $process
     *
     * @return Configuration
     */
    public function setProcess($process)
    {
        $this->process = $process;

        return $this;
    }

    /**
     * Get process
     *
     * @return boolean
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Set process2
     *
     * @param boolean $process2
     *
     * @return Configuration
     */
    public function setProcess2($process2)
    {
        $this->process2 = $process2;

        return $this;
    }

    /**
     * Get process2
     *
     * @return boolean
     */
    public function getProcess2()
    {
        return $this->process2;
    }

    /**
     * Set processReviews
     *
     * @param boolean $processReviews
     *
     * @return Configuration
     */
    public function setProcessReviews($processReviews)
    {
        $this->processReviews = $processReviews;

        return $this;
    }

    /**
     * Get processReviews
     *
     * @return boolean
     */
    public function getProcessReviews()
    {
        return $this->processReviews;
    }

    /**
     * Set emails
     *
     * @param boolean $emails
     *
     * @return Configuration
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;

        return $this;
    }

    /**
     * Get emails
     *
     * @return boolean
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Set lastEmailExecutionDate
     *
     * @param \DateTime $lastEmailExecutionDate
     *
     * @return Configuration
     */
    public function setLastEmailExecutionDate($lastEmailExecutionDate)
    {
        $this->lastEmailExecutionDate = $lastEmailExecutionDate;

        return $this;
    }

    /**
     * Get lastEmailExecutionDate
     *
     * @return \DateTime
     */
    public function getLastEmailExecutionDate()
    {
        return $this->lastEmailExecutionDate;
    }

    /**
     * Set emailsBroascast
     *
     * @param boolean $emailsBroascast
     *
     * @return Configuration
     */
    public function setEmailsBroascast($emailsBroascast)
    {
        $this->emailsBroascast = $emailsBroascast;

        return $this;
    }

    /**
     * Get emailsBroascast
     *
     * @return boolean
     */
    public function getEmailsBroascast()
    {
        return $this->emailsBroascast;
    }

    /**
     * Set lastEmailBroadcastExecutionDate
     *
     * @param \DateTime $lastEmailBroadcastExecutionDate
     *
     * @return Configuration
     */
    public function setLastEmailBroadcastExecutionDate($lastEmailBroadcastExecutionDate)
    {
        $this->lastEmailBroadcastExecutionDate = $lastEmailBroadcastExecutionDate;

        return $this;
    }

    /**
     * Get lastEmailBroadcastExecutionDate
     *
     * @return \DateTime
     */
    public function getLastEmailBroadcastExecutionDate()
    {
        return $this->lastEmailBroadcastExecutionDate;
    }

    /**
     * Set lastReviewsExecutionDate
     *
     * @param \DateTime $lastReviewsExecutionDate
     *
     * @return Configuration
     */
    public function setLastReviewsExecutionDate($lastReviewsExecutionDate)
    {
        $this->lastReviewsExecutionDate = $lastReviewsExecutionDate;

        return $this;
    }

    /**
     * Get lastReviewsExecutionDate
     *
     * @return \DateTime
     */
    public function getLastReviewsExecutionDate()
    {
        return $this->lastReviewsExecutionDate;
    }

    /**
     * Set payment
     *
     * @param boolean $payment
     *
     * @return Configuration
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return boolean
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set lastProductUpdateDate
     *
     * @param \DateTime $lastProductUpdateDate
     *
     * @return Configuration
     */
    public function setLastProductUpdateDate($lastProductUpdateDate)
    {
        $this->lastProductUpdateDate = $lastProductUpdateDate;

        return $this;
    }

    /**
     * Get lastProductUpdateDate
     *
     * @return \DateTime
     */
    public function getLastProductUpdateDate()
    {
        return $this->lastProductUpdateDate;
    }

    /**
     * Set processProducts
     *
     * @param boolean $processProducts
     *
     * @return Configuration
     */
    public function setProcessProducts($processProducts)
    {
        $this->processProducts = $processProducts;

        return $this;
    }

    /**
     * Get processProducts
     *
     * @return boolean
     */
    public function getProcessProducts()
    {
        return $this->processProducts;
    }

    /**
     * Set lastInventoryUpdateDate.
     *
     * @param \DateTime|null $lastInventoryUpdateDate
     *
     * @return Configuration
     */
    public function setLastInventoryUpdateDate($lastInventoryUpdateDate = null)
    {
        $this->lastInventoryUpdateDate = $lastInventoryUpdateDate;

        return $this;
    }

    /**
     * Get lastInventoryUpdateDate.
     *
     * @return \DateTime|null
     */
    public function getLastInventoryUpdateDate()
    {
        return $this->lastInventoryUpdateDate;
    }

    /**
     * Set lastFeedbacksUpdateDate.
     *
     * @param \DateTime|null $lastFeedbacksUpdateDate
     *
     * @return Configuration
     */
    public function setLastFeedbacksUpdateDate($lastFeedbacksUpdateDate = null)
    {
        $this->lastFeedbacksUpdateDate = $lastFeedbacksUpdateDate;

        return $this;
    }

    /**
     * Get lastFeedbacksUpdateDate.
     *
     * @return \DateTime|null
     */
    public function getLastFeedbacksUpdateDate()
    {
        return $this->lastFeedbacksUpdateDate;
    }

    /**
     * Set quickbookAccessToken
     *
     * @param string $quickbookAccessToken
     *
     * @return Configuration
     */
    public function setQuickbookAccessToken($quickbookAccessToken)
    {
        $this->quickbookAccessToken = $quickbookAccessToken;

        return $this;
    }

    /**
     * Get quickbookAccessToken
     *
     * @return string
     */
    public function getQuickbookAccessToken()
    {
        return $this->quickbookAccessToken;
    }

    /**
     * Set quickbookRefreshToken
     *
     * @param string $quickbookRefreshToken
     *
     * @return Configuration
     */
    public function setQuickbookRefreshToken($quickbookRefreshToken)
    {
        $this->quickbookRefreshToken = $quickbookRefreshToken;

        return $this;
    }

    /**
     * Get quickbookRefreshToken
     *
     * @return string
     */
    public function getQuickbookRefreshToken()
    {
        return $this->quickbookRefreshToken;
    }

    /**
     * Set quickbookAccessTokenExpireDate
     *
     * @param \DateTime $quickbookAccessTokenExpireDate
     *
     * @return Configuration
     */
    public function setQuickbookAccessTokenExpireDate($quickbookAccessTokenExpireDate)
    {
        $this->quickbookAccessTokenExpireDate = $quickbookAccessTokenExpireDate;

        return $this;
    }

    /**
     * Get quickbookAccessTokenExpireDate
     *
     * @return \DateTime
     */
    public function getQuickbookAccessTokenExpireDate()
    {
        return $this->quickbookAccessTokenExpireDate;
    }

    /**
     * Set quickbookRefreshTokenExpireDate
     *
     * @param \DateTime $quickbookRefreshTokenExpireDate
     *
     * @return Configuration
     */
    public function setQuickbookRefreshTokenExpireDate($quickbookRefreshTokenExpireDate)
    {
        $this->quickbookRefreshTokenExpireDate = $quickbookRefreshTokenExpireDate;

        return $this;
    }

    /**
     * Get quickbookRefreshTokenExpireDate
     *
     * @return \DateTime
     */
    public function getQuickbookRefreshTokenExpireDate()
    {
        return $this->quickbookRefreshTokenExpireDate;
    }

    /**
     * Set unsubscribeUsers
     *
     * @param array $unsubscribeUsers
     *
     * @return Configuration
     */
    public function addUnsubscribeUsers($unsubscribeUser)
    {
        if(!is_array($this->unsubscribeUsers))
            $this->unsubscribeUsers = [];

        if(!in_array($unsubscribeUser,$this->unsubscribeUsers))
            $this->unsubscribeUsers[] = $unsubscribeUser;

        return $this;
    }

    /**
     * Set unsubscribeUsers
     *
     * @param array $unsubscribeUsers
     *
     * @return Configuration
     */
    public function setUnsubscribeUsers($unsubscribeUsers)
    {
        $this->unsubscribeUsers = $unsubscribeUsers;

        return $this;
    }

    /**
     * Get unsubscribeUsers
     *
     * @return array
     */
    public function getUnsubscribeUsers()
    {
        return $this->unsubscribeUsers;
    }

    /**
     * Set processPayment.
     *
     * @param bool $processPayment
     *
     * @return Configuration
     */
    public function setProcessPayment($processPayment)
    {
        $this->processPayment = $processPayment;

        return $this;
    }

    /**
     * Get processPayment.
     *
     * @return bool
     */
    public function getProcessPayment()
    {
        return $this->processPayment;
    }

    /**
     * Set lastPaymentExecutionDate.
     *
     * @param \DateTime|null $lastPaymentExecutionDate
     *
     * @return Configuration
     */
    public function setLastPaymentExecutionDate($lastPaymentExecutionDate = null)
    {
        $this->lastPaymentExecutionDate = $lastPaymentExecutionDate;

        return $this;
    }

    /**
     * Get lastPaymentExecutionDate.
     *
     * @return \DateTime|null
     */
    public function getLastPaymentExecutionDate()
    {
        return $this->lastPaymentExecutionDate;
    }

    /**
     * Set emailsRensend.
     *
     * @param bool $emailsRensend
     *
     * @return Configuration
     */
    public function setEmailsRensend($emailsRensend)
    {
        $this->emailsRensend = $emailsRensend;

        return $this;
    }

    /**
     * Get emailsRensend.
     *
     * @return bool
     */
    public function getEmailsRensend()
    {
        return $this->emailsRensend;
    }
    /**
     * Set lastEmailResendExecutionDate.
     *
     * @param \DateTime|null $lastEmailResendExecutionDate
     *
     * @return Configuration
     */
    public function setLastEmailResendExecutionDate($lastEmailResendExecutionDate = null)
    {
        $this->lastEmailResendExecutionDate = $lastEmailResendExecutionDate;

        return $this;
    }

    /**
     * Get lastEmailResendExecutionDate.
     *
     * @return \DateTime|null
     */
    public function getLastEmailResendExecutionDate()
    {
        return $this->lastEmailResendExecutionDate;
    }

    /**
     * Set fullfilementCentersNotified.
     *
     * @param array|null $fullfilementCentersNotified
     *
     * @return Configuration
     */
    public function setFullfilementCentersNotified($fullfilementCentersNotified = null)
    {
        $this->fullfilementCentersNotified = $fullfilementCentersNotified;

        return $this;
    }

    public function addFullfilementCentersNotified($fullfilementCentersNotified = null)
    {
        if(!is_array($this->fullfilementCentersNotified))
            $this->fullfilementCentersNotified = [];

        if(!in_array($fullfilementCentersNotified, $this->fullfilementCentersNotified))
            $this->fullfilementCentersNotified[] = $fullfilementCentersNotified;

        return $this;
    }

    /**
     * Get fullfilementCentersNotified.
     *
     * @return array|null
     */
    public function getFullfilementCentersNotified()
    {
        return $this->fullfilementCentersNotified;
    }
}
