<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Connection user.
 *
 * @ORM\Table(name="user_connection")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\UserConnectionRepository")
 */
class UserConnection
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="connections")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(name="loginDate", type="datetime")
     */
    private $loginDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="logoutDate", type="datetime", nullable=true)
     */
    private $logoutDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="IP", type="string", nullable=true)
     */
    private $ip;

    /**
     * @var \string
     * @ORM\Column(name="countryCode", type="string", nullable=true)
     */
    private $countryCode;

    public function __construct()
    {
        $this->loginDate = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set User.
     *
     * @param User $user
     *
     * @return UserConnection
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set loginDate.
     *
     * @param Datetime $loginDate
     *
     * @return UserConnection
     */
    public function setLoginDate($loginDate)
    {
        $this->loginDate = $loginDate;

        return $this;
    }

    /**
     * Get loginDate.
     *
     * @return Datetime
     */
    public function getLoginDate()
    {
        return $this->loginDate;
    }

    /**
     * Set logoutDate.
     *
     * @param Datetime $logoutDate
     *
     * @return UserConnection
     */
    public function setLogoutDate($logoutDate)
    {
        $this->logoutDate = $logoutDate;

        return $this;
    }

    /**
     * Get logoutDate.
     *
     * @return Datetime
     */
    public function getLogoutDate()
    {
        return $this->logoutDate;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return UserConnection
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set countryCode.
     *
     * @param \DateTime|null $countryCode
     *
     * @return UserConnection
     */
    public function setCountryCode($countryCode = null)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode.
     *
     * @return \DateTime|null
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
}
