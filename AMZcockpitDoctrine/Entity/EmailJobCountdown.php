<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * EmailJob
 *
 * @ORM\Table(name="emails_jobs_countdown")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailJobCountdownRepository")
 */
class EmailJobCountdown
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="_code", type="string")
     */
    private $code;

    /**
     * @ORM\Column(name="_time", type="datetime")
     */
    private $time;

    /**
     * @ORM\Column(name="_color", type="string", length=10, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\EmailJob", inversedBy="emailJobsCountsdown", cascade={"persist"})
     */
    protected $emailJob;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return EmailJobCountdown
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set emailJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailJob
     *
     * @return EmailJobCountdown
     */
    public function setEmailJob(\AMZcockpitDoctrine\Entity\EmailJob $emailJob = null)
    {
        $this->emailJob = $emailJob;

        return $this;
    }

    /**
     * Get emailJob
     *
     * @return \AMZcockpitDoctrine\Entity\EmailJob
     */
    public function getEmailJob()
    {
        return $this->emailJob;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return EmailJobCountdown
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return EmailJobCountdown
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
