<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Invoice
 *
 * @ORM\Table(name="invoices")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\InvoiceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Invoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="amzcockpit_index", type="string", nullable=true)
     */
    private $index;

    /**
     * @ORM\Column(name="amzcockpit_invoice_id", type="string", nullable=true)
     */
    private $amzcockpitInvoiceId;

    /**
     * @ORM\Column(name="stripe_invoice_id", type="string", nullable=true)
     */
    private $invoiceId;

    /**
     * @ORM\Column(name="quickbook_tva", type="string", nullable=true)
     */
    private $quickbookTVA;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $currentPeriodeStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $currentPeriodeEnd;

    /**
     * @ORM\Column(name="billing", type="string", nullable=true)
     */
    private $billing;

    /**
     * @ORM\Column(name="discount", type="string", nullable=true)
     */
    private $discount;

    /**
     * @ORM\Column(name="subtotal", type="string")
     */
    private $subTotal;

    /**
     * @ORM\Column(name="total", type="string")
     */
    private $total;

    /**
     * @ORM\Column(name="amount", type="string")
     */
    private $amount;

    /**
     * @ORM\Column(name="currency", type="string")
     */
    private $currency;

    /**
     * @ORM\Column(name="plan_id", type="string", nullable=true)
     */
    private $planId;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="invoices")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}, nullable=true)
     */
    private $paymentYearly = false;

    /**
     * @ORM\Column(type="float", options={"default" : 20})
     */
    private $TVA = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quickbookInvoiceId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return Invoice
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set invoiceId
     *
     * @param string $invoiceId
     *
     * @return Invoice
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return string
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set currentPeriodeStart
     *
     * @param \DateTime $currentPeriodeStart
     *
     * @return Invoice
     */
    public function setCurrentPeriodeStart($currentPeriodeStart)
    {
        $this->currentPeriodeStart = $currentPeriodeStart;

        return $this;
    }

    /**
     * Get currentPeriodeStart
     *
     * @return \DateTime
     */
    public function getCurrentPeriodeStart()
    {
        return $this->currentPeriodeStart;
    }

    /**
     * Set currentPeriodeEnd
     *
     * @param \DateTime $currentPeriodeEnd
     *
     * @return Invoice
     */
    public function setCurrentPeriodeEnd($currentPeriodeEnd)
    {
        $this->currentPeriodeEnd = $currentPeriodeEnd;

        return $this;
    }

    /**
     * Get currentPeriodeEnd
     *
     * @return \DateTime
     */
    public function getCurrentPeriodeEnd()
    {
        return $this->currentPeriodeEnd;
    }

    /**
     * Set billing
     *
     * @param string $billing
     *
     * @return Invoice
     */
    public function setBilling($billing)
    {
        $this->billing = $billing;

        return $this;
    }

    /**
     * Get billing
     *
     * @return string
     */
    public function getBilling()
    {
        return $this->billing;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return Invoice
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set subTotal
     *
     * @param string $subTotal
     *
     * @return Invoice
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    /**
     * Get subTotal
     *
     * @return string
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return Invoice
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Invoice
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Invoice
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency == "eur" ? '€' : $this->currency;
    }

    /**
     * Set planId
     *
     * @param string $planId
     *
     * @return Invoice
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * Get planId
     *
     * @return string
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Invoice
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set paymentYearly
     *
     * @param boolean $paymentYearly
     *
     * @return Invoice
     */
    public function setPaymentYearly($paymentYearly)
    {
        $this->paymentYearly = $paymentYearly;

        return $this;
    }

    /**
     * Get paymentYearly
     *
     * @return boolean
     */
    public function getPaymentYearly()
    {
        return $this->paymentYearly;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate
     */
    public function updateInvoiceId()
    {
        $invoiceId = $this->getCreatedAt()->format('dmY').'-'.sprintf("%09s",($this->getId()));
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * Set tVA
     *
     * @param float $tVA
     *
     * @return Invoice
     */
    public function setTVA($tVA)
    {
        $this->TVA = $tVA;

        return $this;
    }

    /**
     * Get tVA
     *
     * @return float
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * Set quickbookInvoiceId
     *
     * @param string $quickbookInvoiceId
     *
     * @return Invoice
     */
    public function setQuickbookInvoiceId($quickbookInvoiceId)
    {
        $this->quickbookInvoiceId = $quickbookInvoiceId;

        return $this;
    }

    /**
     * Get quickbookInvoiceId
     *
     * @return string
     */
    public function getQuickbookInvoiceId()
    {
        return $this->quickbookInvoiceId;
    }

    /**
     * Set amzcockpitInvoiceId
     *
     * @param string $amzcockpitInvoiceId
     *
     * @return Invoice
     */
    public function setAmzcockpitInvoiceId($amzcockpitInvoiceId)
    {
        $this->amzcockpitInvoiceId = $amzcockpitInvoiceId;

        return $this;
    }

    /**
     * Get amzcockpitInvoiceId
     *
     * @return string
     */
    public function getAmzcockpitInvoiceId()
    {
        return $this->amzcockpitInvoiceId;
    }

    /**
     * Set index
     *
     * @param string $index
     *
     * @return Invoice
     */
    public function setIndex($index)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set quickbookTVA
     *
     * @param string $quickbookTVA
     *
     * @return Invoice
     */
    public function setQuickbookTVA($quickbookTVA)
    {
        $this->quickbookTVA = $quickbookTVA;

        return $this;
    }

    /**
     * Get quickbookTVA
     *
     * @return string
     */
    public function getQuickbookTVA()
    {
        return $this->quickbookTVA;
    }
}
