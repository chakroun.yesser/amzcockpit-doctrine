<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * KeywordTracking
 *
 * @ORM\Table(name="keywords_tracking")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\KeywordTrackingRepository")
 */
class KeywordTracking
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomIdGenerator")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="keywordsTracking")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $ASIN;

    /**
     * @ORM\Column(name="product_image", type="string", nullable=true)
     */
    private $productImage;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\KeywordTrackingItem", mappedBy="keywordTracking" , cascade={"persist","remove"})
     */
    protected $items;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $nextSchedule;

    /**
     * @ORM\Column(name="marketplace_id", type="string")
     */
    private $marketplaceId;

    /**
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nextSchedule = (new \DateTime('now'))->add(new \DateInterval('PT5M'));
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplaceId
     *
     * @param string $marketplaceId
     *
     * @return KeywordTracking
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return string
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return KeywordTracking
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set aSIN
     *
     * @param string $aSIN
     *
     * @return KeywordTracking
     */
    public function setASIN($aSIN)
    {
        $this->ASIN = $aSIN;

        return $this;
    }

    /**
     * Get aSIN
     *
     * @return string
     */
    public function getASIN()
    {
        return $this->ASIN;
    }

    /**
     * Set productImage
     *
     * @param string $productImage
     *
     * @return KeywordTracking
     */
    public function setProductImage($productImage)
    {
        $this->productImage = $productImage;

        return $this;
    }

    /**
     * Get productImage
     *
     * @return string
     */
    public function getProductImage()
    {
        return $this->productImage;
    }

    /**
     * Set nextSchedule
     *
     * @param \DateTime $nextSchedule
     *
     * @return KeywordTracking
     */
    public function setNextSchedule($nextSchedule)
    {
        $this->nextSchedule = $nextSchedule;

        return $this;
    }

    /**
     * Get nextSchedule
     *
     * @return \DateTime
     */
    public function getNextSchedule()
    {
        return $this->nextSchedule;
    }

    /**
     * Add item
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTrackingItem $item
     *
     * @return KeywordTracking
     */
    public function addItem(\AMZcockpitDoctrine\Entity\KeywordTrackingItem $item)
    {
        $item->setKeywordTracking($this);
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTrackingItem $item
     */
    public function removeItem(\AMZcockpitDoctrine\Entity\KeywordTrackingItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return KeywordTracking
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}
