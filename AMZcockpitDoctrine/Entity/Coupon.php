<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\FrequencyType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Coupon
 *
 * @ORM\Table(name="coupons")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\CouponRepository")
 */
class Coupon
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomCouponGenerator")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="coupons")
     */
    protected $users;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add user.
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return Coupon
     */
    public function addUser(\AMZcockpitDoctrine\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUser(\AMZcockpitDoctrine\Entity\User $user)
    {
        return $this->users->removeElement($user);
    }

    /**
     * Get user.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
