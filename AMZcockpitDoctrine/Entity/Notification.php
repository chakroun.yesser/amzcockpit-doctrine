<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\AudienceStatutType;
use AMZcockpitDoctrine\DBAL\Types\EmailBroadcastStatutType;
use AMZcockpitDoctrine\DBAL\Types\EmailBroadcastUsersType;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Notification
 *
 * @ORM\Table(name="notifications")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\NotificationRepository")
 */
class Notification
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $content;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\AudienceStatutType")
     * @ORM\Column(name="statut", type="AudienceStatutType", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User")
     */
    protected $createdBy;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\EmailBroadcastUsersType")
     * @ORM\Column(name="users_type", type="EmailBroadcastUsersType", length=255, nullable=true)
     */
    private $usersType;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     **/
    protected $users;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\User")
     * @ORM\JoinTable(name="notification_seen_users")
     **/
    protected $usersSeen;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->statut = AudienceStatutType::DRAFT;
        $this->usersType = EmailBroadcastUsersType::ALL;
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usersSeen = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return Notification
     */
    public function addUser(\AMZcockpitDoctrine\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     */
    public function removeUser(\AMZcockpitDoctrine\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add usersSeen
     *
     * @param \AMZcockpitDoctrine\Entity\User $usersSeen
     *
     * @return Notification
     */
    public function addUsersSeen(\AMZcockpitDoctrine\Entity\User $usersSeen)
    {
        $this->usersSeen[] = $usersSeen;

        return $this;
    }

    /**
     * Remove usersSeen
     *
     * @param \AMZcockpitDoctrine\Entity\User $usersSeen
     */
    public function removeUsersSeen(\AMZcockpitDoctrine\Entity\User $usersSeen)
    {
        $this->usersSeen->removeElement($usersSeen);
    }

    /**
     * Get usersSeen
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsersSeen()
    {
        return $this->usersSeen;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Notification
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set createdBy
     *
     * @param \AMZcockpitDoctrine\Entity\User $createdBy
     *
     * @return Notification
     */
    public function setCreatedBy(\AMZcockpitDoctrine\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set usersType
     *
     * @param EmailBroadcastUsersType $usersType
     *
     * @return Notification
     */
    public function setUsersType($usersType)
    {
        $this->usersType = $usersType;

        return $this;
    }

    /**
     * Get usersType
     *
     * @return EmailBroadcastUsersType
     */
    public function getUsersType()
    {
        return $this->usersType;
    }
}
