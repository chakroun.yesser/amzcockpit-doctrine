<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\SupportTicketStatutType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * SupportTicketMessage
 *
 * @ORM\Table(name="support_ticket_messages")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\SupportTicketMessageRepository")
 */
class SupportTicketMessage
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\SupportTicket", inversedBy="messages")
     */
    protected $supportTicket;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\SupportTicketStatutType")
     * @ORM\Column(name="statut", type="SupportTicketStatutType", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="supportTicketsMessagesCreated")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="supportTicketsMessages")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $assignedUser;

    /**
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist"})
     */
    protected $file;

    /**
     * @ORM\Column(name="is_private", type="boolean")
     */
    private $isPrivate;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\User")
     * @ORM\JoinTable(name="support_ticket_messages_seen_users")
     */
    protected $seenUsers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seenUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isPrivate = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statut
     *
     * @param SupportTicketStatutType $statut
     *
     * @return SupportTicketMessage
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return SupportTicketStatutType
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return SupportTicketMessage
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set assignedUser
     *
     * @param \AMZcockpitDoctrine\Entity\User $assignedUser
     *
     * @return SupportTicketMessage
     */
    public function setAssignedUser(\AMZcockpitDoctrine\Entity\User $assignedUser = null)
    {
        $this->assignedUser = $assignedUser;

        return $this;
    }

    /**
     * Get assignedUser
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getAssignedUser()
    {
        return $this->assignedUser;
    }

    /**
     * Set file
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return SupportTicketMessage
     */
    public function setFile(\AMZcockpitDoctrine\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set supportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $supportTicket
     *
     * @return SupportTicketMessage
     */
    public function setSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $supportTicket = null)
    {
        $this->supportTicket = $supportTicket;

        return $this;
    }

    /**
     * Get supportTicket
     *
     * @return \AMZcockpitDoctrine\Entity\SupportTicket
     */
    public function getSupportTicket()
    {
        return $this->supportTicket;
    }
    /**
     * Add seenUser
     *
     * @param \AMZcockpitDoctrine\Entity\User $seenUser
     *
     * @return SupportTicketMessage
     */
    public function addSeenUser(\AMZcockpitDoctrine\Entity\User $seenUser)
    {
        $this->seenUsers[] = $seenUser;

        return $this;
    }

    /**
     * Remove seenUser
     *
     * @param \AMZcockpitDoctrine\Entity\User $seenUser
     */
    public function removeSeenUser(\AMZcockpitDoctrine\Entity\User $seenUser)
    {
        $this->seenUsers->removeElement($seenUser);
    }

    /**
     * Get seenUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeenUsers()
    {
        return $this->seenUsers;
    }

    /**
     * Set createdBy
     *
     * @param \AMZcockpitDoctrine\Entity\User $createdBy
     *
     * @return SupportTicketMessage
     */
    public function setCreatedBy(\AMZcockpitDoctrine\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set isPrivate
     *
     * @param boolean $isPrivate
     *
     * @return SupportTicketMessage
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return boolean
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }
}
