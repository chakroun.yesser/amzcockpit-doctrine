<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\PaymentStatutType;
use AMZcockpitDoctrine\Entity\MWS\Customer;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @UniqueEntity("email")
 */
class User extends BaseUser
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_VIP = 'ROLE_VIP';

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", mappedBy="user", cascade={"persist","remove"})
     */
    protected $stores;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\ZipRequest", mappedBy="user", cascade={"persist","remove"})
     */
    protected $zipRequests;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailFolder", mappedBy="user", cascade={"persist","remove"})
     */
    protected $emailFolders;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Email", mappedBy="user", cascade={"persist","remove"})
     */
    protected $emails;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailBroadcast", mappedBy="user", cascade={"persist","remove"})
     */
    protected $emailsBroadcast;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailVariable", mappedBy="user", cascade={"persist","remove"})
     */
    protected $emailsVariables;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Invoice", mappedBy="user", cascade={"persist"})
     */
    protected $invoices;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Log", mappedBy="user", cascade={"persist","remove"})
     */
    protected $logs;

    /**
     * @ORM\Column(type="string", nullable=true, length=70, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", nullable=true, length=70, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File",cascade={"persist","remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $picture;

    /**
     * @ORM\Column(type="phone_number", nullable=true, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min="2",
     *     minMessage="L'adresse doit contenir {{ limit }} caractères au minimum."
     *     )
     */
    private $adresse;

    /**
     * @ORM\Column(name="adresse_complement", type="string", length=255, nullable=true)
     * @Assert\Length(
     *     min="2",
     *     minMessage="L'adresse doit contenir {{ limit }} caractères au minimum."
     *     )
     */
    private $adresseComplement;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     * @Assert\Length(
     *      min="2",
     *      minMessage="Le ville doit contenir {{ limit }} caractères au minimum."
     *      )
     */
    private $ville;

    /**
     * @ORM\Column(name="code_postal", type="string", length=10, nullable=true)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $nomEntreprise;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $paysEntreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresseEntreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresseComplementEntreprise;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $villeEntreprise;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $codePostalEntreprise;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $pays;

    /**
     * @ORM\Column(name="telephone", type="string", length=100, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(name="raison_sociale", type="string", length=80, nullable=true)
     * @Assert\Length(
     *     min="2",
     *     minMessage="La raison social doit contenir {{ limit }} caractères au minimum."
     *     )
     */
    private $raisonSociale;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $statut;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tva;

    /**
     * @ORM\Column(type="string", nullable=true, length=10)
     */
    private $paysStockageFBA;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tvaFBM;

    /**
     * @ORM\Column(type="string", nullable=true, length=10)
     */
    private $paysStockageFBM;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $capitalSocial;

    /**
     * @ORM\Column(type="string", options={"default" : "EUR"}, nullable=true)
     */
    private $capitalSocialCurrency;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $checkTva;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mentionTva;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tvaIntracom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SIREN;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SIRET;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $RCS;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $planId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookAccountId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookAccessToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $getResponseContactId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subscriptionId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $blocked = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $currentPeriodeStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $currentPeriodeEnd;

    /**
     * @ORM\Column(name="emails_quota", type="integer", nullable=true)
     */
    private $emailsQuota = null;

    /**
     * @ORM\Column(name="factures_quota", type="integer", nullable=true)
     */
    private $facturesQuota = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasCard = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasIBAN = false;

    /**
     * @ORM\Column(type="integer", options={"default" : "0"}, nullable=true)
     */
    private $failedSEPAChargingCount = 0;

    /**
     * @ORM\Column(type="integer", options={"default" : "0"}, nullable=true)
     */
    private $failedCardChargingCount = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasStores = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasTemplates = false;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $buyedOffers = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $notifEmptyStore = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $notifTrialElapsePoke = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $notifEmailsQuota = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $notifInvoicesQuota = false;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    private $notifEmailsQuotaCount = 0;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    private $notifInvoicesQuotaCount = 0;

    /**
     * @ORM\Column(name="process_reviews", type="boolean")
     */
    private $processReviews = false;

    /**
     * @ORM\Column(name="process_feedbacks", type="boolean")
     */
    private $processFeedbacks = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastFeedbacksUpdate;

    /**
     * @ORM\Column(name="process_inventory", type="boolean")
     */
    private $processInventory = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastInventoryUpdate;

    /**
     * @ORM\Column(name="last_process_reviews_exec", type="datetime", nullable=true)
     */
    private $lastProcessReviewsExecutionDate;

    /**
     * @ORM\Column(name="process_reviews_problem_notified", type="boolean")
     */
    private $processReviewsProblemNotified = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastStoresUpdate;

    /**
     * @ORM\Column(name="process_stores_update", type="boolean")
     */
    private $processStoresUpdate = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastProductsUpdateDate;

    /**
     * @ORM\Column(name="process_products", type="boolean")
     */
    private $processProducts = false;

    /**
     * @ORM\Column(name="process_fees", type="boolean")
     */
    private $processFees = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastFeesDate;

    /**
     * @ORM\Column(name="is_called", type="boolean")
     */
    private $isCalled = false;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Customer")
     * @ORM\JoinTable(name="user_blacklist_customers")
     */
    private $blacklistCustomers;

    /**
     * @ORM\Column(name="block_returned_customers", type="boolean")
     */
    private $blockReturnedCustomers = false;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\Notification", mappedBy="users")
     */
    private $notifications;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastSubscriptionPlanChange;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}, nullable=true)
     */
    private $isDeleted = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $authCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateAuthCode;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $trusted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $redirectAuthCode = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="last_sms_sent", nullable=true)
     */
    protected $lastSmsSentDate;

    /**
     * @ORM\OneToMany(targetEntity="UserConnection", mappedBy="user", cascade={"persist","remove"})
     */
    private $connections;

    /**
     * @ORM\Column(name="loginCount", type="integer", nullable=false)
     */
    private $loginCount;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateExpireAdminAccessAuthorization;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $expireAdminAccessAuthorizationToken;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\SupportTicket", mappedBy="assignedUser")
     */
    protected $assignedSupportTickets;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\SupportTicket", mappedBy="createdBy")
     */
    protected $supportTickets;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\SupportTicketMessage", mappedBy="assignedUser")
     */
    protected $supportTicketsMessages;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\SupportTicketMessage", mappedBy="createdBy")
     */
    protected $supportTicketsMessagesCreated;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paymentWrongTimes = null;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\PaymentStatutType")
     * @ORM\Column(type="PaymentStatutType", length=255, nullable=true)
     */
    private $paymentStatut;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $paymentNextDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $paymentLastAttempt;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}, nullable=true)
     */
    private $paymentYearly = false;

    /**
     * @ORM\Column(name="payment_amount", type="float", nullable=true)
     */
    private $paymentAmount;

    /**
     * @ORM\Column(name="payment_ht_amount", type="float", nullable=true)
     */
    private $paymentHtAmount;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $unsubscribeDate;

    /**
     * @ORM\Column(name="unsubscribe_notified", options={"default" : 0}, type="boolean")
     */
    private $unsubscribeNotified = false;

    /**
     * @ORM\Column(name="tva_notified", type="boolean")
     */
    private $tvaNotified = false;

    /**
     * @ORM\Column(name="card_problem_notified", type="boolean")
     */
    private $cardProblemNotif = false;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\KeywordTracking", mappedBy="user", cascade={"persist","remove"})
     */
    protected $keywordsTracking;

    /**
     * @ORM\Column(name="on_boarding_step", type="integer")
     */
    private $onBoardingStep = 1;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $onBoarding;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\OnBoardingWhenStartAmazon")
     * @ORM\Column(type="OnBoardingWhenStartAmazon", length=255, nullable=true)
     */
    private $onBoardingWhenStartAmazon;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $onBoardingWhatDoYouDo;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\OnBoardingHowManyProduct")
     * @ORM\Column(type="OnBoardingHowManyProduct", length=255, nullable=true)
     */
    private $onBoardingHowManyProduct;

    /**
     * @ORM\Column(type="boolean", length=255, nullable=true)
     */
    private $onBoardingMasteryPPC;

    /**
     * @ORM\Column(type="boolean", length=255, nullable=true)
     */
    private $onBoardingLiveFromAmazon;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1}, length=255, nullable=true)
     */
    private $onBoardingEnded;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}, length=255, nullable=true)
     */
    private $onBoardingVideoSeen;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1}, length=255, nullable=true)
     */
    private $trialEnded;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1}, length=255, nullable=true)
     */
    private $paymentModalOpened;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}, length=255, nullable=true)
     */
    private $cgu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quickbookCustomerId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $comptabilityACCRE;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $comptabilityDischarge;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $comptabilityDOM;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $comptabilityFranchiseTVA;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0}, nullable=true)
     */
    private $comptabilityMonthly = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $comptabilityDateStart;

    /**
     * @ORM\Column(type="float",options={"default" : 20}, nullable=true)
     */
    private $comptabilityProjectionTVA;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\File", mappedBy="user", cascade={"persist","remove"})
     */
    protected $files;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Payment", mappedBy="user", cascade={"persist","remove"})
     */
    protected $payments;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $salesJournals;

    /**
     * @ORM\Column(type="integer", length=4, options={"default" : 1}, nullable=true)
     */
    private $currentSalesJournal;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $salesJournalsNC;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0}, length=255, nullable=true)
     */
    private $imAmazonSeller;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\Coupon", mappedBy="users")
     * @ORM\JoinTable(name="user_coupons")
     */
    protected $coupons;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $vocalMessageId;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Jobs", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $jobs;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $affilaeHasPartnership;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $affilaePartnershipId;

    public $recaptcha;

    public function __construct()
    {
        parent::__construct();
        $this->pays = "FR";
        $this->buyedOffers = [];
        $this->trialEnded = false;
        $this->isDeleted = false;
        $this->paymentYearly = false;
        $this->onBoardingEnded = false;
        $this->paymentModalOpened = false;
        $this->loginCount = 0;
        $this->paymentWrongTimes = 0;
        $this->onBoardingStep = 0;
        $this->notifEmailsQuotaCount = 0;
    }

    public function hasPaymentMethod()
    {
        return $this->getHasIBAN() or $this->getHasCard();
    }

    /**
     * Increment loginCount.
     */
    public function loginCountIncrement()
    {
        $this->loginCount += 1;
    }

    /**
     * Add store
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store $store
     *
     * @return User
     */
    public function addStore(\AMZcockpitDoctrine\Entity\MWS\Store $store)
    {
        $this->stores[]  = $store;

        return $this;
    }

    /**
     * Remove store
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store $store
     */
    public function removeStore(\AMZcockpitDoctrine\Entity\MWS\Store $store)
    {
        $this->stores->removeElement($store);
    }

    /**
     * Get stores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStores()
    {
        return $this->stores;
    }

    public function setEmail($email){
        parent::setEmail($email);
        $this->setUsername($email);
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresseComplement
     *
     * @param string $adresseComplement
     *
     * @return User
     */
    public function setAdresseComplement($adresseComplement)
    {
        $this->adresseComplement = $adresseComplement;

        return $this;
    }

    /**
     * Get adresseComplement
     *
     * @return string
     */
    public function getAdresseComplement()
    {
        return $this->adresseComplement;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return User
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return User
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set nomEntreprise
     *
     * @param string $nomEntreprise
     *
     * @return User
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * Get nomEntreprise
     *
     * @return string
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->prenom.' '.$this->nom;
    }

    /**
     * Set planId
     *
     * @param string $planId
     *
     * @return User
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * Get planId
     *
     * @return string
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * Set customerId
     *
     * @param string $customerId
     *
     * @return User
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set subscriptionId
     *
     * @param string $subscriptionId
     *
     * @return User
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;

        return $this;
    }

    /**
     * Get subscriptionId
     *
     * @return string
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * Set blocked
     *
     * @param boolean $blocked
     *
     * @return User
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * Get blocked
     *
     * @return boolean
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * Set currentPeriodeEnd
     *
     * @param \DateTime $currentPeriodeEnd
     *
     * @return User
     */
    public function setCurrentPeriodeEnd($currentPeriodeEnd)
    {
        $this->currentPeriodeEnd = $currentPeriodeEnd;

        return $this;
    }

    /**
     * Get currentPeriodeEnd
     *
     * @return \DateTime
     */
    public function getCurrentPeriodeEnd()
    {
        return $this->currentPeriodeEnd;
    }

    /**
     * Set emailsQuota
     *
     * @param integer $emailsQuota
     *
     * @return User
     */
    public function setEmailsQuota($emailsQuota)
    {
        $this->emailsQuota = $emailsQuota;

        return $this;
    }

    /**
     * Get emailsQuota
     *
     * @return integer
     */
    public function getEmailsQuota()
    {
        return $this->emailsQuota;
    }

    /**
     * Set facturesQuota
     *
     * @param integer $facturesQuota
     *
     * @return User
     */
    public function setFacturesQuota($facturesQuota)
    {
        $this->facturesQuota = $facturesQuota;

        return $this;
    }

    /**
     * Get facturesQuota
     *
     * @return integer
     */
    public function getFacturesQuota()
    {
        return $this->facturesQuota;
    }


    /**
     * Add email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     *
     * @return User
     */
    public function addEmail(\AMZcockpitDoctrine\Entity\Email $email)
    {
        $this->emails[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     */
    public function removeEmail(\AMZcockpitDoctrine\Entity\Email $email)
    {
        $this->emails->removeElement($email);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add emailsVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailVariable $emailsVariable
     *
     * @return User
     */
    public function addEmailsVariable(\AMZcockpitDoctrine\Entity\EmailVariable $emailsVariable)
    {
        $this->emailsVariables[] = $emailsVariable;

        return $this;
    }

    /**
     * Remove emailsVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailVariable $emailsVariable
     */
    public function removeEmailsVariable(\AMZcockpitDoctrine\Entity\EmailVariable $emailsVariable)
    {
        $this->emailsVariables->removeElement($emailsVariable);
    }

    /**
     * Get emailsVariables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailsVariables()
    {
        return $this->emailsVariables;
    }

    /**
     * Set currentPeriodeStart
     *
     * @param \DateTime $currentPeriodeStart
     *
     * @return User
     */
    public function setCurrentPeriodeStart($currentPeriodeStart)
    {
        $this->currentPeriodeStart = $currentPeriodeStart;

        return $this;
    }

    /**
     * Get currentPeriodeStart
     *
     * @return \DateTime
     */
    public function getCurrentPeriodeStart()
    {
        return $this->currentPeriodeStart;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set hasCard
     *
     * @param boolean $hasCard
     *
     * @return User
     */
    public function setHasCard($hasCard)
    {
        $this->hasCard = $hasCard;

        return $this;
    }

    /**
     * Get hasCard
     *
     * @return boolean
     */
    public function getHasCard()
    {
        return $this->hasCard;
    }

    /**
     * Set hasStores
     *
     * @param boolean $hasStores
     *
     * @return User
     */
    public function setHasStores($hasStores)
    {
        $this->hasStores = $hasStores;

        return $this;
    }

    /**
     * Get hasStores
     *
     * @return boolean
     */
    public function getHasStores()
    {
        return $this->hasStores;
    }

    /**
     * Set hasTemplates
     *
     * @param boolean $hasTemplates
     *
     * @return User
     */
    public function setHasTemplates($hasTemplates)
    {
        $this->hasTemplates = $hasTemplates;

        return $this;
    }

    /**
     * Get hasTemplates
     *
     * @return boolean
     */
    public function getHasTemplates()
    {
        return $this->hasTemplates;
    }

    /**
     * Add buyedOffer
     * @param array $buyedOffer
     * @return User
     */
    public function addBuyedOffer($buyedOffer = [])
    {
        if(is_string($buyedOffer)) $buyedOffer = ['key' => $buyedOffer];
        if(!is_array($this->buyedOffers)) $this->buyedOffers = [];
        foreach ($this->buyedOffers as $cbuyedOffers) {
            if($cbuyedOffers['key'] == $buyedOffer['key'])
                return $this;
        }
        $this->buyedOffers[] = $buyedOffer;

        return $this;
    }

    /**
     * Contain buyedOffer
     * @return boolean
     */
    public function containBuyedOffer($buyedOfferKey)
    {
        if(is_array($this->buyedOffers) and count($this->buyedOffers)) {
            foreach ($this->buyedOffers as $cbuyedOffers) {
                if(isset($cbuyedOffers['key']) and $cbuyedOffers['key'] == $buyedOfferKey)
                    return true;
            }
        }

        return false;
    }

    /**
     * Set buyedOffers
     *
     * @param array $buyedOffers
     *
     * @return User
     */
    public function setBuyedOffers($buyedOffers)
    {
        $this->buyedOffers = $buyedOffers;

        return $this;
    }

    /**
     * Get buyedOffers
     *
     * @return array
     */
    public function getBuyedOffers()
    {
        return $this->buyedOffers;
    }

    /**
     * Add emailsBroadcast
     *
     * @param \AMZcockpitDoctrine\Entity\EmailBroadcast $emailsBroadcast
     *
     * @return User
     */
    public function addEmailsBroadcast(\AMZcockpitDoctrine\Entity\EmailBroadcast $emailsBroadcast)
    {
        $this->emailsBroadcast[] = $emailsBroadcast;

        return $this;
    }

    /**
     * Remove emailsBroadcast
     *
     * @param \AMZcockpitDoctrine\Entity\EmailBroadcast $emailsBroadcast
     */
    public function removeEmailsBroadcast(\AMZcockpitDoctrine\Entity\EmailBroadcast $emailsBroadcast)
    {
        $this->emailsBroadcast->removeElement($emailsBroadcast);
    }

    /**
     * Get emailsBroadcast
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailsBroadcast()
    {
        return $this->emailsBroadcast;
    }

    /**
     * Add invoice
     *
     * @param \AMZcockpitDoctrine\Entity\Invoice $invoice
     *
     * @return User
     */
    public function addInvoice(\AMZcockpitDoctrine\Entity\Invoice $invoice)
    {
        $this->invoices[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice
     *
     * @param \AMZcockpitDoctrine\Entity\Invoice $invoice
     */
    public function removeInvoice(\AMZcockpitDoctrine\Entity\Invoice $invoice)
    {
        $this->invoices->removeElement($invoice);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Add zipRequest
     *
     * @param \AMZcockpitDoctrine\Entity\ZipRequest $zipRequest
     *
     * @return User
     */
    public function addZipRequest(\AMZcockpitDoctrine\Entity\ZipRequest $zipRequest)
    {
        $this->zipRequests[] = $zipRequest;

        return $this;
    }

    /**
     * Remove zipRequest
     *
     * @param \AMZcockpitDoctrine\Entity\ZipRequest $zipRequest
     */
    public function removeZipRequest(\AMZcockpitDoctrine\Entity\ZipRequest $zipRequest)
    {
        $this->zipRequests->removeElement($zipRequest);
    }

    /**
     * Get zipRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZipRequests()
    {
        return $this->zipRequests;
    }

    /**
     * Add log
     *
     * @param \AMZcockpitDoctrine\Entity\Log $log
     *
     * @return User
     */
    public function addLog(\AMZcockpitDoctrine\Entity\Log $log)
    {
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AMZcockpitDoctrine\Entity\Log $log
     */
    public function removeLog(\AMZcockpitDoctrine\Entity\Log $log)
    {
        $this->logs->removeElement($log);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Set notifEmailsQuota
     *
     * @param boolean $notifEmailsQuota
     *
     * @return User
     */
    public function setNotifEmailsQuota($notifEmailsQuota)
    {
        $this->notifEmailsQuota = $notifEmailsQuota;

        return $this;
    }

    /**
     * Get notifEmailsQuota
     *
     * @return boolean
     */
    public function getNotifEmailsQuota()
    {
        return $this->notifEmailsQuota;
    }

    /**
     * Set notifInvoicesQuota
     *
     * @param boolean $notifInvoicesQuota
     *
     * @return User
     */
    public function setNotifInvoicesQuota($notifInvoicesQuota)
    {
        $this->notifInvoicesQuota = $notifInvoicesQuota;

        return $this;
    }

    /**
     * Get notifInvoicesQuota
     *
     * @return boolean
     */
    public function getNotifInvoicesQuota()
    {
        return $this->notifInvoicesQuota;
    }

    /**
     * Set processReviews
     *
     * @param boolean $processReviews
     *
     * @return User
     */
    public function setProcessReviews($processReviews)
    {
        $this->processReviews = $processReviews;

        return $this;
    }

    /**
     * Get processReviews
     *
     * @return boolean
     */
    public function getProcessReviews()
    {
        return $this->processReviews;
    }

    /**
     * Set lastProcessReviewsExecutionDate
     *
     * @param \DateTime $lastProcessReviewsExecutionDate
     *
     * @return User
     */
    public function setLastProcessReviewsExecutionDate($lastProcessReviewsExecutionDate)
    {
        $this->lastProcessReviewsExecutionDate = $lastProcessReviewsExecutionDate;

        return $this;
    }

    /**
     * Get lastProcessReviewsExecutionDate
     *
     * @return \DateTime
     */
    public function getLastProcessReviewsExecutionDate()
    {
        return $this->lastProcessReviewsExecutionDate;
    }

    /**
     * Set processReviewsProblemNotified
     *
     * @param boolean $processReviewsProblemNotified
     *
     * @return User
     */
    public function setProcessReviewsProblemNotified($processReviewsProblemNotified)
    {
        $this->processReviewsProblemNotified = $processReviewsProblemNotified;

        return $this;
    }

    /**
     * Get processReviewsProblemNotified
     *
     * @return boolean
     */
    public function getProcessReviewsProblemNotified()
    {
        return $this->processReviewsProblemNotified;
    }

    /**
     * Set lastStoresUpdate
     *
     * @param \DateTime $lastStoresUpdate
     *
     * @return User
     */
    public function setLastStoresUpdate($lastStoresUpdate)
    {
        $this->lastStoresUpdate = $lastStoresUpdate;

        return $this;
    }

    /**
     * Get lastStoresUpdate
     *
     * @return \DateTime
     */
    public function getLastStoresUpdate()
    {
        return $this->lastStoresUpdate;
    }

    /**
     * Set processStoresUpdate
     *
     * @param boolean $processStoresUpdate
     *
     * @return User
     */
    public function setProcessStoresUpdate($processStoresUpdate)
    {
        $this->processStoresUpdate = $processStoresUpdate;

        return $this;
    }

    /**
     * Get processStoresUpdate
     *
     * @return boolean
     */
    public function getProcessStoresUpdate()
    {
        return $this->processStoresUpdate;
    }

    /**
     * Set lastProductsUpdateDate
     *
     * @param \DateTime $lastProductsUpdateDate
     *
     * @return User
     */
    public function setLastProductsUpdateDate($lastProductsUpdateDate)
    {
        $this->lastProductsUpdateDate = $lastProductsUpdateDate;

        return $this;
    }

    /**
     * Get lastProductsUpdateDate
     *
     * @return \DateTime
     */
    public function getLastProductsUpdateDate()
    {
        return $this->lastProductsUpdateDate;
    }

    /**
     * Set processProducts
     *
     * @param boolean $processProducts
     *
     * @return User
     */
    public function setProcessProducts($processProducts)
    {
        $this->processProducts = $processProducts;

        return $this;
    }

    /**
     * Get processProducts
     *
     * @return boolean
     */
    public function getProcessProducts()
    {
        return $this->processProducts;
    }

    /**
     * Set isCalled
     *
     * @param boolean $isCalled
     *
     * @return User
     */
    public function setIsCalled($isCalled)
    {
        $this->isCalled = $isCalled;

        return $this;
    }

    /**
     * Get isCalled
     *
     * @return boolean
     */
    public function getIsCalled()
    {
        return $this->isCalled;
    }
    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return User
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    public function getProductsCount()
    {
        $count = 0;
        if(count($this->getStores())) {
            /** @var \AMZcockpitDoctrine\Entity\MWS\Store $store */
            foreach ($this->getStores() as $store) {
                $count += count($store->getProducts());
            }
        }
        return $count;
    }

    /**
     * Add blacklistCustomer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $blacklistCustomer
     *
     * @return User
     */
    public function addBlacklistCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $blacklistCustomer)
    {
        $this->blacklistCustomers[] = $blacklistCustomer;

        return $this;
    }

    /**
     * Remove blacklistCustomer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $blacklistCustomer
     */
    public function removeBlacklistCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $blacklistCustomer)
    {
        $this->blacklistCustomers->removeElement($blacklistCustomer);
    }

    /**
     * Get blacklistCustomers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlacklistCustomers()
    {
        return $this->blacklistCustomers;
    }

    public function isBlacklistedCustomer(Customer $customer)
    {
        return $this->getBlacklistCustomers()->contains($customer);
    }

    /**
     * Set notifEmptyStore.
     *
     * @param bool|null $notifEmptyStore
     *
     * @return User
     */
    public function setNotifEmptyStore($notifEmptyStore = null)
    {
        $this->notifEmptyStore = $notifEmptyStore;

        return $this;
    }

    /**
     * Get notifEmptyStore.
     *
     * @return bool|null
     */
    public function getNotifEmptyStore()
    {
        return $this->notifEmptyStore;
    }

    /**
     * Set notifTrialElapsePoke.
     *
     * @param bool|null $notifTrialElapsePoke
     *
     * @return User
     */
    public function setNotifTrialElapsePoke($notifTrialElapsePoke = null)
    {
        $this->notifTrialElapsePoke = $notifTrialElapsePoke;

        return $this;
    }

    /**
     * Get notifTrialElapsePoke.
     *
     * @return bool|null
     */
    public function getNotifTrialElapsePoke()
    {
        return $this->notifTrialElapsePoke;
    }

    /**
     * Set processFeedbacks.
     *
     * @param bool $processFeedbacks
     *
     * @return User
     */
    public function setProcessFeedbacks($processFeedbacks)
    {
        $this->processFeedbacks = $processFeedbacks;

        return $this;
    }

    /**
     * Get processFeedbacks.
     *
     * @return bool
     */
    public function getProcessFeedbacks()
    {
        return $this->processFeedbacks;
    }

    /**
     * Set lastFeedbacksUpdate.
     *
     * @param \DateTime|null $lastFeedbacksUpdate
     *
     * @return User
     */
    public function setLastFeedbacksUpdate($lastFeedbacksUpdate = null)
    {
        $this->lastFeedbacksUpdate = $lastFeedbacksUpdate;

        return $this;
    }

    /**
     * Get lastFeedbacksUpdate.
     *
     * @return \DateTime|null
     */
    public function getLastFeedbacksUpdate()
    {
        return $this->lastFeedbacksUpdate;
    }

    /**
     * Set processInventory.
     *
     * @param bool $processInventory
     *
     * @return User
     */
    public function setProcessInventory($processInventory)
    {
        $this->processInventory = $processInventory;

        return $this;
    }

    /**
     * Get processInventory.
     *
     * @return bool
     */
    public function getProcessInventory()
    {
        return $this->processInventory;
    }

    /**
     * Set lastInventoryUpdate.
     *
     * @param \DateTime|null $lastInventoryUpdate
     *
     * @return User
     */
    public function setLastInventoryUpdate($lastInventoryUpdate = null)
    {
        $this->lastInventoryUpdate = $lastInventoryUpdate;

        return $this;
    }

    /**
     * Get lastInventoryUpdate.
     *
     * @return \DateTime|null
     */
    public function getLastInventoryUpdate()
    {
        return $this->lastInventoryUpdate;
    }

    /**
     * Set lastSubscriptionPlanChange.
     *
     * @param \DateTime|null $lastSubscriptionPlanChange
     *
     * @return User
     */
    public function setLastSubscriptionPlanChange($lastSubscriptionPlanChange = null)
    {
        $this->lastSubscriptionPlanChange = $lastSubscriptionPlanChange;

        return $this;
    }

    /**
     * Get lastSubscriptionPlanChange.
     *
     * @return \DateTime|null
     */
    public function getLastSubscriptionPlanChange()
    {
        return $this->lastSubscriptionPlanChange;
    }

    /**
     * Set isDeleted.
     *
     * @param bool|null $isDeleted
     *
     * @return User
     */
    public function setIsDeleted($isDeleted = null)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted.
     *
     * @return bool|null
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set authCode
     *
     * @param integer $authCode
     *
     * @return User
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;

        return $this;
    }

    /**
     * Get authCode
     *
     * @return integer
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set dateAuthCode
     *
     * @param \DateTime $dateAuthCode
     *
     * @return User
     */
    public function setDateAuthCode($dateAuthCode)
    {
        $this->dateAuthCode = $dateAuthCode;

        return $this;
    }

    /**
     * Get dateAuthCode
     *
     * @return \DateTime
     */
    public function getDateAuthCode()
    {
        return $this->dateAuthCode;
    }

    /**
     * Set trusted
     *
     * @param array $trusted
     *
     * @return User
     */
    public function setTrusted($trusted)
    {
        $this->trusted = $trusted;

        return $this;
    }

    /**
     * Get trusted
     *
     * @return array
     */
    public function getTrusted()
    {
        return $this->trusted;
    }

    /**
     * Set redirectAuthCode
     *
     * @param boolean $redirectAuthCode
     *
     * @return User
     */
    public function setRedirectAuthCode($redirectAuthCode)
    {
        $this->redirectAuthCode = $redirectAuthCode;

        return $this;
    }

    /**
     * Get redirectAuthCode
     *
     * @return boolean
     */
    public function getRedirectAuthCode()
    {
        return $this->redirectAuthCode;
    }

    /**
     * Set lastSmsSentDate
     *
     * @param \DateTime $lastSmsSentDate
     *
     * @return User
     */
    public function setLastSmsSentDate($lastSmsSentDate)
    {
        $this->lastSmsSentDate = $lastSmsSentDate;

        return $this;
    }

    /**
     * Get lastSmsSentDate
     *
     * @return \DateTime
     */
    public function getLastSmsSentDate()
    {
        return $this->lastSmsSentDate;
    }

    public function getPhoneEncrypter()
    {
        $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumber = $this->getPhone() ? $phoneNumberUtil->format($this->getPhone(), \libphonenumber\PhoneNumberFormat::INTERNATIONAL) : null;
        $phoneNumber = preg_replace('/\s+/', '', $phoneNumber);
        return substr($phoneNumber,0,3).'******'.substr($phoneNumber,-4,strlen($phoneNumber));
    }


    /**
     * Set loginCount
     *
     * @param integer $loginCount
     *
     * @return User
     */
    public function setLoginCount($loginCount)
    {
        $this->loginCount = $loginCount;

        return $this;
    }

    /**
     * Get loginCount
     *
     * @return integer
     */
    public function getLoginCount()
    {
        return $this->loginCount;
    }

    /**
     * Add connection
     *
     * @param \AMZcockpitDoctrine\Entity\UserConnection $connection
     *
     * @return User
     */
    public function addConnection(\AMZcockpitDoctrine\Entity\UserConnection $connection)
    {
        $this->connections[] = $connection;

        return $this;
    }

    /**
     * Remove connection
     *
     * @param \AMZcockpitDoctrine\Entity\UserConnection $connection
     */
    public function removeConnection(\AMZcockpitDoctrine\Entity\UserConnection $connection)
    {
        $this->connections->removeElement($connection);
    }

    /**
     * Get connections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConnections()
    {
        return $this->connections;
    }

    /**
     * Set dateExpireAdminAccessAuthorization
     *
     * @param \DateTime $dateExpireAdminAccessAuthorization
     *
     * @return User
     */
    public function setDateExpireAdminAccessAuthorization($dateExpireAdminAccessAuthorization)
    {
        $this->dateExpireAdminAccessAuthorization = $dateExpireAdminAccessAuthorization;

        return $this;
    }

    /**
     * Get dateExpireAdminAccessAuthorization
     *
     * @return \DateTime
     */
    public function getDateExpireAdminAccessAuthorization()
    {
        return $this->dateExpireAdminAccessAuthorization;
    }

    /**
     * Set expireAdminAccessAuthorizationToken
     *
     * @param string $expireAdminAccessAuthorizationToken
     *
     * @return User
     */
    public function setExpireAdminAccessAuthorizationToken($expireAdminAccessAuthorizationToken)
    {
        $this->expireAdminAccessAuthorizationToken = $expireAdminAccessAuthorizationToken;

        return $this;
    }

    /**
     * Get expireAdminAccessAuthorizationToken
     *
     * @return string
     */
    public function getExpireAdminAccessAuthorizationToken()
    {
        return $this->expireAdminAccessAuthorizationToken;
    }


    public function addTrustedComputer($token, \DateTime $validUntil)
    {
        $this->trusted[$token] = $validUntil->format("r");
    }

   public function isTrustedComputer($token)
   {
        if (isset($this->trusted[$token])) {
            $now = new \DateTime();
            $validUntil = new \DateTime($this->trusted[$token]);
            return $now < $validUntil;
        }
        return false;
   }

   public function isGrantedInvoice() {
        return is_null($this->getFacturesQuota()) or $this->hasRole(self::ROLE_SUPER_ADMIN) or $this->hasRole(self::ROLE_ADMIN) or $this->hasRole(self::ROLE_VIP) or $this->getFacturesQuota() > 0;
   }


    /**
     * Set paysEntreprise
     *
     * @param string $paysEntreprise
     *
     * @return User
     */
    public function setPaysEntreprise($paysEntreprise)
    {
        $this->paysEntreprise = $paysEntreprise;

        return $this;
    }

    /**
     * Get paysEntreprise
     *
     * @return string
     */
    public function getPaysEntreprise()
    {
        return $this->paysEntreprise;
    }

    /**
     * Set adresseEntreprise
     *
     * @param string $adresseEntreprise
     *
     * @return User
     */
    public function setAdresseEntreprise($adresseEntreprise)
    {
        $this->adresseEntreprise = $adresseEntreprise;

        return $this;
    }

    /**
     * Get adresseEntreprise
     *
     * @return string
     */
    public function getAdresseEntreprise()
    {
        return $this->adresseEntreprise;
    }

    /**
     * Set adresseComplementEntreprise
     *
     * @param string $adresseComplementEntreprise
     *
     * @return User
     */
    public function setAdresseComplementEntreprise($adresseComplementEntreprise)
    {
        $this->adresseComplementEntreprise = $adresseComplementEntreprise;

        return $this;
    }

    /**
     * Get adresseComplementEntreprise
     *
     * @return string
     */
    public function getAdresseComplementEntreprise()
    {
        return $this->adresseComplementEntreprise;
    }

    /**
     * Set villeEntreprise
     *
     * @param string $villeEntreprise
     *
     * @return User
     */
    public function setVilleEntreprise($villeEntreprise)
    {
        $this->villeEntreprise = $villeEntreprise;

        return $this;
    }

    /**
     * Get villeEntreprise
     *
     * @return string
     */
    public function getVilleEntreprise()
    {
        return $this->villeEntreprise;
    }

    /**
     * Set codePostalEntreprise
     *
     * @param string $codePostalEntreprise
     *
     * @return User
     */
    public function setCodePostalEntreprise($codePostalEntreprise)
    {
        $this->codePostalEntreprise = $codePostalEntreprise;

        return $this;
    }

    /**
     * Get codePostalEntreprise
     *
     * @return string
     */
    public function getCodePostalEntreprise()
    {
        return $this->codePostalEntreprise;
    }

    /**
     * Add assignedSupportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $assignedSupportTicket
     *
     * @return User
     */
    public function addAssignedSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $assignedSupportTicket)
    {
        $this->assignedSupportTickets[] = $assignedSupportTicket;

        return $this;
    }

    /**
     * Remove assignedSupportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $assignedSupportTicket
     */
    public function removeAssignedSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $assignedSupportTicket)
    {
        $this->assignedSupportTickets->removeElement($assignedSupportTicket);
    }

    /**
     * Get assignedSupportTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignedSupportTickets()
    {
        return $this->assignedSupportTickets;
    }

    /**
     * Add supportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $supportTicket
     *
     * @return User
     */
    public function addSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $supportTicket)
    {
        $this->supportTickets[] = $supportTicket;

        return $this;
    }

    /**
     * Remove supportTicket
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicket $supportTicket
     */
    public function removeSupportTicket(\AMZcockpitDoctrine\Entity\SupportTicket $supportTicket)
    {
        $this->supportTickets->removeElement($supportTicket);
    }

    /**
     * Get supportTickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupportTickets()
    {
        return $this->supportTickets;
    }

    /**
     * Add supportTicketsMessage
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessage
     *
     * @return User
     */
    public function addSupportTicketsMessage(\AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessage)
    {
        $this->supportTicketsMessages[] = $supportTicketsMessage;

        return $this;
    }

    /**
     * Remove supportTicketsMessage
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessage
     */
    public function removeSupportTicketsMessage(\AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessage)
    {
        $this->supportTicketsMessages->removeElement($supportTicketsMessage);
    }

    /**
     * Get supportTicketsMessages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupportTicketsMessages()
    {
        return $this->supportTicketsMessages;
    }

    /**
     * Add supportTicketsMessagesCreated
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessagesCreated
     *
     * @return User
     */
    public function addSupportTicketsMessagesCreated(\AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessagesCreated)
    {
        $this->supportTicketsMessagesCreated[] = $supportTicketsMessagesCreated;

        return $this;
    }

    /**
     * Remove supportTicketsMessagesCreated
     *
     * @param \AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessagesCreated
     */
    public function removeSupportTicketsMessagesCreated(\AMZcockpitDoctrine\Entity\SupportTicketMessage $supportTicketsMessagesCreated)
    {
        $this->supportTicketsMessagesCreated->removeElement($supportTicketsMessagesCreated);
    }

    /**
     * Get supportTicketsMessagesCreated
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupportTicketsMessagesCreated()
    {
        return $this->supportTicketsMessagesCreated;
    }

    /**
     * Set getResponseContactId
     *
     * @param string $getResponseContactId
     *
     * @return User
     */
    public function setGetResponseContactId($getResponseContactId)
    {
        $this->getResponseContactId = $getResponseContactId;

        return $this;
    }

    /**
     * Get getResponseContactId
     *
     * @return string
     */
    public function getGetResponseContactId()
    {
        return $this->getResponseContactId;
    }

    public function isAdmin()
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN) or $this->hasRole(static::ROLE_ADMIN);
    }

    /**
     * Set paymentWrongTimes
     *
     * @param integer $paymentWrongTimes
     *
     * @return User
     */
    public function setPaymentWrongTimes($paymentWrongTimes)
    {
        $this->paymentWrongTimes = $paymentWrongTimes;

        return $this;
    }

    /**
     * Get paymentWrongTimes
     *
     * @return integer
     */
    public function getPaymentWrongTimes()
    {
        return $this->paymentWrongTimes;
    }

    /**
     * Set paymentStatut
     *
     * @param PaymentStatutType|string $paymentStatut
     *
     * @return User
     */
    public function setPaymentStatut($paymentStatut)
    {
        $this->paymentStatut = $paymentStatut;

        return $this;
    }

    /**
     * Get paymentStatut
     *
     * @return PaymentStatutType
     */
    public function getPaymentStatut()
    {
        return $this->paymentStatut;
    }

    /**
     * Set paymentNextDate
     *
     * @param \DateTime $paymentNextDate
     *
     * @return User
     */
    public function setPaymentNextDate($paymentNextDate)
    {
        $this->paymentNextDate = $paymentNextDate;

        return $this;
    }

    /**
     * Get paymentNextDate
     *
     * @return \DateTime
     */
    public function getPaymentNextDate()
    {
        return $this->paymentNextDate;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return User
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set raisonSociale
     *
     * @param string $raisonSociale
     *
     * @return User
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    /**
     * Get raisonSociale
     *
     * @return string
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * Set tva
     *
     * @param float $tva
     *
     * @return User
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return float
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set paysStockageFBA
     *
     * @param string $paysStockageFBA
     *
     * @return User
     */
    public function setPaysStockageFBA($paysStockageFBA)
    {
        $this->paysStockageFBA = $paysStockageFBA;

        return $this;
    }

    /**
     * Get paysStockageFBA
     *
     * @return string
     */
    public function getPaysStockageFBA()
    {
        return $this->paysStockageFBA;
    }

    /**
     * Set tvaFBM
     *
     * @param float $tvaFBM
     *
     * @return User
     */
    public function setTvaFBM($tvaFBM)
    {
        $this->tvaFBM = $tvaFBM;

        return $this;
    }

    /**
     * Get tvaFBM
     *
     * @return float
     */
    public function getTvaFBM()
    {
        return $this->tvaFBM;
    }

    /**
     * Set paysStockageFBM
     *
     * @param string $paysStockageFBM
     *
     * @return User
     */
    public function setPaysStockageFBM($paysStockageFBM)
    {
        $this->paysStockageFBM = $paysStockageFBM;

        return $this;
    }

    /**
     * Get paysStockageFBM
     *
     * @return string
     */
    public function getPaysStockageFBM()
    {
        return $this->paysStockageFBM;
    }

    /**
     * Set capitalSocial
     *
     * @param float $capitalSocial
     *
     * @return User
     */
    public function setCapitalSocial($capitalSocial)
    {
        $this->capitalSocial = $capitalSocial;

        return $this;
    }

    /**
     * Get capitalSocial
     *
     * @return float
     */
    public function getCapitalSocial()
    {
        return $this->capitalSocial;
    }

    /**
     * Set checkTva
     *
     * @param boolean $checkTva
     *
     * @return User
     */
    public function setCheckTva($checkTva)
    {
        $this->checkTva = $checkTva;

        return $this;
    }

    /**
     * Get checkTva
     *
     * @return boolean
     */
    public function getCheckTva()
    {
        return $this->checkTva;
    }

    /**
     * Set mentionTva
     *
     * @param boolean $mentionTva
     *
     * @return User
     */
    public function setMentionTva($mentionTva)
    {
        $this->mentionTva = $mentionTva;

        return $this;
    }

    /**
     * Get mentionTva
     *
     * @return boolean
     */
    public function getMentionTva()
    {
        return $this->mentionTva;
    }

    /**
     * Set tvaIntracom
     *
     * @param string $tvaIntracom
     *
     * @return User
     */
    public function setTvaIntracom($tvaIntracom)
    {
        $this->tvaIntracom = $tvaIntracom;

        return $this;
    }

    /**
     * Get tvaIntracom
     *
     * @return string
     */
    public function getTvaIntracom()
    {
        return $this->tvaIntracom;
    }

    /**
     * Set sIREN
     *
     * @param string $sIREN
     *
     * @return User
     */
    public function setSIREN($sIREN)
    {
        $this->SIREN = $sIREN;

        return $this;
    }

    /**
     * Get sIREN
     *
     * @return string
     */
    public function getSIREN()
    {
        return $this->SIREN;
    }

    /**
     * Set sIRET
     *
     * @param string $sIRET
     *
     * @return User
     */
    public function setSIRET($sIRET)
    {
        $this->SIRET = $sIRET;

        return $this;
    }

    /**
     * Get sIRET
     *
     * @return string
     */
    public function getSIRET()
    {
        return $this->SIRET;
    }

    /**
     * Set rCS
     *
     * @param string $rCS
     *
     * @return User
     */
    public function setRCS($rCS)
    {
        $this->RCS = $rCS;

        return $this;
    }

    /**
     * Get rCS
     *
     * @return string
     */
    public function getRCS()
    {
        return $this->RCS;
    }

    /**
     * Set paymentYearly
     *
     * @param boolean $paymentYearly
     *
     * @return User
     */
    public function setPaymentYearly($paymentYearly)
    {
        $this->paymentYearly = $paymentYearly;

        return $this;
    }

    /**
     * Get paymentYearly
     *
     * @return boolean
     */
    public function getPaymentYearly()
    {
        return $this->paymentYearly;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }


    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Set facebookAccountId
     *
     * @param string $facebookAccountId
     *
     * @return User
     */
    public function setFacebookAccountId($facebookAccountId)
    {
        $this->facebookAccountId = $facebookAccountId;

        return $this;
    }

    /**
     * Get facebookAccountId
     *
     * @return string
     */
    public function getFacebookAccountId()
    {
        return $this->facebookAccountId;
    }

    /**
     * Set unsubscribeDate
     *
     * @param \DateTime $unsubscribeDate
     *
     * @return User
     */
    public function setUnsubscribeDate($unsubscribeDate)
    {
        $this->unsubscribeDate = $unsubscribeDate;

        return $this;
    }

    /**
     * Get unsubscribeDate
     *
     * @return \DateTime
     */
    public function getUnsubscribeDate()
    {
        return $this->unsubscribeDate;
    }

    /**
     * Set processFees
     *
     * @param boolean $processFees
     *
     * @return User
     */
    public function setProcessFees($processFees)
    {
        $this->processFees = $processFees;

        return $this;
    }

    /**
     * Get processFees
     *
     * @return boolean
     */
    public function getProcessFees()
    {
        return $this->processFees;
    }

    /**
     * Set lastFeesDate
     *
     * @param \DateTime $lastFeesDate
     *
     * @return User
     */
    public function setLastFeesDate($lastFeesDate)
    {
        $this->lastFeesDate = $lastFeesDate;

        return $this;
    }

    /**
     * Get lastFeesDate
     *
     * @return \DateTime
     */
    public function getLastFeesDate()
    {
        return $this->lastFeesDate;
    }

    /**
     * Set tvaNotified
     *
     * @param boolean $tvaNotified
     *
     * @return User
     */
    public function setTvaNotified($tvaNotified)
    {
        $this->tvaNotified = $tvaNotified;

        return $this;
    }

    /**
     * Get tvaNotified
     *
     * @return boolean
     */
    public function getTvaNotified()
    {
        return $this->tvaNotified;
    }

    /**
     * Set picture
     *
     * @param \AMZcockpitDoctrine\Entity\File $picture
     *
     * @return User
     */
    public function setPicture(\AMZcockpitDoctrine\Entity\File $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Add keywordsTracking
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTracking $keywordsTracking
     *
     * @return User
     */
    public function addKeywordsTracking(\AMZcockpitDoctrine\Entity\KeywordTracking $keywordsTracking)
    {
        $this->keywordsTracking[] = $keywordsTracking;

        return $this;
    }

    /**
     * Remove keywordsTracking
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTracking $keywordsTracking
     */
    public function removeKeywordsTracking(\AMZcockpitDoctrine\Entity\KeywordTracking $keywordsTracking)
    {
        $this->keywordsTracking->removeElement($keywordsTracking);
    }

    /**
     * Get keywordsTracking
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeywordsTracking()
    {
        return $this->keywordsTracking;
    }

    /**
     * Set cardProblemNotif
     *
     * @param boolean $cardProblemNotif
     *
     * @return User
     */
    public function setCardProblemNotif($cardProblemNotif)
    {
        $this->cardProblemNotif = $cardProblemNotif;

        return $this;
    }

    /**
     * Get cardProblemNotif
     *
     * @return boolean
     */
    public function getCardProblemNotif()
    {
        return $this->cardProblemNotif;
    }

    /**
     * Set paymentLastAttempt
     *
     * @param \DateTime $paymentLastAttempt
     *
     * @return User
     */
    public function setPaymentLastAttempt($paymentLastAttempt)
    {
        $this->paymentLastAttempt = $paymentLastAttempt;

        return $this;
    }

    /**
     * Get paymentLastAttempt
     *
     * @return \DateTime
     */
    public function getPaymentLastAttempt()
    {
        return $this->paymentLastAttempt;
    }

    /**
     * Set onBoardingStep
     *
     * @param integer $onBoardingStep
     *
     * @return User
     */
    public function setOnBoardingStep($onBoardingStep)
    {
        $this->onBoardingStep = $onBoardingStep;

        return $this;
    }

    /**
     * Get onBoardingStep
     *
     * @return integer
     */
    public function getOnBoardingStep()
    {
        return $this->onBoardingStep;
    }

    /**
     * Set onBoarding
     *
     * @param array $onBoarding
     *
     * @return User
     */
    public function setOnBoarding($onBoarding)
    {
        $this->onBoarding = $onBoarding;

        return $this;
    }

    /**
     * Get onBoarding
     *
     * @return array
     */
    public function getOnBoarding()
    {
        return $this->onBoarding;
    }

    /**
     * Set onBoardingWhenStartAmazon
     *
     * @param OnBoardingWhenStartAmazon $onBoardingWhenStartAmazon
     *
     * @return User
     */
    public function setOnBoardingWhenStartAmazon($onBoardingWhenStartAmazon)
    {
        $this->onBoardingWhenStartAmazon = $onBoardingWhenStartAmazon;

        return $this;
    }

    /**
     * Get onBoardingWhenStartAmazon
     *
     * @return OnBoardingWhenStartAmazon
     */
    public function getOnBoardingWhenStartAmazon()
    {
        return $this->onBoardingWhenStartAmazon;
    }

    /**
     * Set onBoardingWhatDoYouDo
     *
     * @param OnBoardingWhatDoYouDo $onBoardingWhatDoYouDo
     *
     * @return User
     */
    public function setOnBoardingWhatDoYouDo($onBoardingWhatDoYouDo)
    {
        $this->onBoardingWhatDoYouDo = $onBoardingWhatDoYouDo;

        return $this;
    }

    /**
     * Get onBoardingWhatDoYouDo
     *
     * @return OnBoardingWhatDoYouDo
     */
    public function getOnBoardingWhatDoYouDo()
    {
        return $this->onBoardingWhatDoYouDo;
    }

    /**
     * Set onBoardingHowManyProduct
     *
     * @param OnBoardingHowManyProduct $onBoardingHowManyProduct
     *
     * @return User
     */
    public function setOnBoardingHowManyProduct($onBoardingHowManyProduct)
    {
        $this->onBoardingHowManyProduct = $onBoardingHowManyProduct;

        return $this;
    }

    /**
     * Get onBoardingHowManyProduct
     *
     * @return OnBoardingHowManyProduct
     */
    public function getOnBoardingHowManyProduct()
    {
        return $this->onBoardingHowManyProduct;
    }

    /**
     * Set onBoardingMasteryPPC
     *
     * @param boolean $onBoardingMasteryPPC
     *
     * @return User
     */
    public function setOnBoardingMasteryPPC($onBoardingMasteryPPC)
    {
        $this->onBoardingMasteryPPC = $onBoardingMasteryPPC;

        return $this;
    }

    /**
     * Get onBoardingMasteryPPC
     *
     * @return boolean
     */
    public function getOnBoardingMasteryPPC()
    {
        return $this->onBoardingMasteryPPC;
    }

    /**
     * Set onBoardingLiveFromAmazon
     *
     * @param boolean $onBoardingLiveFromAmazon
     *
     * @return User
     */
    public function setOnBoardingLiveFromAmazon($onBoardingLiveFromAmazon)
    {
        $this->onBoardingLiveFromAmazon = $onBoardingLiveFromAmazon;

        return $this;
    }

    /**
     * Get onBoardingLiveFromAmazon
     *
     * @return boolean
     */
    public function getOnBoardingLiveFromAmazon()
    {
        return $this->onBoardingLiveFromAmazon;
    }

    /**
     * Set onBoardingEnded
     *
     * @param boolean $onBoardingEnded
     *
     * @return User
     */
    public function setOnBoardingEnded($onBoardingEnded)
    {
        $this->onBoardingEnded = $onBoardingEnded;

        return $this;
    }

    /**
     * Get onBoardingEnded
     *
     * @return boolean
     */
    public function getOnBoardingEnded()
    {
        return $this->onBoardingEnded;
    }

    /**
     * Set cgu
     *
     * @param boolean $cgu
     *
     * @return User
     */
    public function setCgu($cgu)
    {
        $this->cgu = $cgu;

        return $this;
    }

    /**
     * Get cgu
     *
     * @return boolean
     */
    public function getCgu()
    {
        return $this->cgu;
    }

    public function hasTrialQuestionsReply() {
        if(
            $this->getOnBoardingWhenStartAmazon() and
            !is_null($this->getOnBoardingWhatDoYouDo()) and
            !empty($this->getOnBoardingWhatDoYouDo()) and
            !is_null($this->getOnBoardingMasteryPPC())
        ) {
            return true;
        }
        return false;
    }

    public function getTrialDayCount() {
        $return = 2;
        if(
            $this->getOnBoardingWhenStartAmazon() and
            !is_null($this->getOnBoardingWhatDoYouDo()) and
            !empty($this->getOnBoardingWhatDoYouDo()) and
            !is_null($this->getOnBoardingMasteryPPC())
        ) {
            $return+=5;
        }

        if(count($this->getStores())) {
            $return +=3;
        }

//        if($this->getOnBoardingVideoSeen()) {
//            $return +=2;
//        }

        return $return;
    }

    /**
     * Set onBoardingVideoSeen
     *
     * @param boolean $onBoardingVideoSeen
     *
     * @return User
     */
    public function setOnBoardingVideoSeen($onBoardingVideoSeen)
    {
        $this->onBoardingVideoSeen = $onBoardingVideoSeen;

        return $this;
    }

    /**
     * Get onBoardingVideoSeen
     *
     * @return boolean
     */
    public function getOnBoardingVideoSeen()
    {
        return $this->onBoardingVideoSeen;
    }


    /**
     * Add notification
     *
     * @param \AMZcockpitDoctrine\Entity\Notification $notification
     *
     * @return User
     */
    public function addNotification(\AMZcockpitDoctrine\Entity\Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \AMZcockpitDoctrine\Entity\Notification $notification
     */
    public function removeNotification(\AMZcockpitDoctrine\Entity\Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Add emailFolder
     *
     * @param \AMZcockpitDoctrine\Entity\EmailFolder $emailFolder
     *
     * @return User
     */
    public function addEmailFolder(\AMZcockpitDoctrine\Entity\EmailFolder $emailFolder)
    {
        $this->emailFolders[] = $emailFolder;

        return $this;
    }

    /**
     * Remove emailFolder
     *
     * @param \AMZcockpitDoctrine\Entity\EmailFolder $emailFolder
     */
    public function removeEmailFolder(\AMZcockpitDoctrine\Entity\EmailFolder $emailFolder)
    {
        $this->emailFolders->removeElement($emailFolder);
    }

    /**
     * Get emailFolders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailFolders()
    {
        return $this->emailFolders;
    }

    /**
     * Set quickbookCustomerId
     *
     * @param string $quickbookCustomerId
     *
     * @return User
     */
    public function setQuickbookCustomerId($quickbookCustomerId)
    {
        $this->quickbookCustomerId = $quickbookCustomerId;

        return $this;
    }

    /**
     * Get quickbookCustomerId
     *
     * @return string
     */
    public function getQuickbookCustomerId()
    {
        return $this->quickbookCustomerId;
    }

    /**
     * Set paymentAmount
     *
     * @param float $paymentAmount
     *
     * @return User
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get paymentAmount
     *
     * @return float
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * Set paymentHtAmount
     *
     * @param float $paymentHtAmount
     *
     * @return User
     */
    public function setPaymentHtAmount($paymentHtAmount)
    {
        $this->paymentHtAmount = $paymentHtAmount;

        return $this;
    }

    /**
     * Get paymentHtAmount
     *
     * @return float
     */
    public function getPaymentHtAmount()
    {
        return $this->paymentHtAmount;
    }

    /**
     * Set paymentModalOpened
     *
     * @param boolean $paymentModalOpened
     *
     * @return User
     */
    public function setPaymentModalOpened($paymentModalOpened)
    {
        $this->paymentModalOpened = $paymentModalOpened;

        return $this;
    }

    /**
     * Get paymentModalOpened
     *
     * @return boolean
     */
    public function getPaymentModalOpened()
    {
        return $this->paymentModalOpened;
    }

    public function hasTrialEnded()
    {
        $date = clone $this->createdAt;
        $date->add(new \DateInterval('P10D'));
        return $date < new \DateTime('now');
    }

    /**
     * Set capitalSocialCurrency
     *
     * @param string $capitalSocialCurrency
     *
     * @return User
     */
    public function setCapitalSocialCurrency($capitalSocialCurrency)
    {
        $this->capitalSocialCurrency = $capitalSocialCurrency;

        return $this;
    }

    /**
     * Get capitalSocialCurrency
     *
     * @return string
     */
    public function getCapitalSocialCurrency()
    {
        return $this->capitalSocialCurrency;
    }

    /**
     * Set trialEnded
     *
     * @param boolean $trialEnded
     *
     * @return User
     */
    public function setTrialEnded($trialEnded)
    {
        $this->trialEnded = $trialEnded;

        return $this;
    }

    /**
     * Get trialEnded
     *
     * @return boolean
     */
    public function getTrialEnded()
    {
        return $this->trialEnded;
    }

    /**
     * Set comptabilityACCRE
     *
     * @param boolean $comptabilityACCRE
     *
     * @return User
     */
    public function setComptabilityACCRE($comptabilityACCRE)
    {
        $this->comptabilityACCRE = $comptabilityACCRE;

        return $this;
    }

    /**
     * Get comptabilityACCRE
     *
     * @return boolean
     */
    public function getComptabilityACCRE()
    {
        return $this->comptabilityACCRE;
    }

    /**
     * Set comptabilityDischarge
     *
     * @param boolean $comptabilityDischarge
     *
     * @return User
     */
    public function setComptabilityDischarge($comptabilityDischarge)
    {
        $this->comptabilityDischarge = $comptabilityDischarge;

        return $this;
    }

    /**
     * Get comptabilityDischarge
     *
     * @return boolean
     */
    public function getComptabilityDischarge()
    {
        return $this->comptabilityDischarge;
    }

    /**
     * Set comptabilityDOM
     *
     * @param boolean $comptabilityDOM
     *
     * @return User
     */
    public function setComptabilityDOM($comptabilityDOM)
    {
        $this->comptabilityDOM = $comptabilityDOM;

        return $this;
    }

    /**
     * Get comptabilityDOM
     *
     * @return boolean
     */
    public function getComptabilityDOM()
    {
        return $this->comptabilityDOM;
    }

    /**
     * Set comptabilityFranchiseTVA
     *
     * @param boolean $comptabilityFranchiseTVA
     *
     * @return User
     */
    public function setComptabilityFranchiseTVA($comptabilityFranchiseTVA)
    {
        $this->comptabilityFranchiseTVA = $comptabilityFranchiseTVA;

        return $this;
    }

    /**
     * Get comptabilityFranchiseTVA
     *
     * @return boolean
     */
    public function getComptabilityFranchiseTVA()
    {
        return $this->comptabilityFranchiseTVA;
    }

    /**
     * Set comptabilityMonthly
     *
     * @param boolean $comptabilityMonthly
     *
     * @return User
     */
    public function setComptabilityMonthly($comptabilityMonthly)
    {
        $this->comptabilityMonthly = $comptabilityMonthly;

        return $this;
    }

    /**
     * Get comptabilityMonthly
     *
     * @return boolean
     */
    public function getComptabilityMonthly()
    {
        return $this->comptabilityMonthly;
    }

    /**
     * Set comptabilityDateStart
     *
     * @param \DateTime $comptabilityDateStart
     *
     * @return User
     */
    public function setComptabilityDateStart($comptabilityDateStart)
    {
        $this->comptabilityDateStart = $comptabilityDateStart;

        return $this;
    }

    /**
     * Get comptabilityDateStart
     *
     * @return \DateTime
     */
    public function getComptabilityDateStart()
    {
        return $this->comptabilityDateStart;
    }

    /**
     * Set comptabilityProjectionTVA.
     *
     * @param float|null $comptabilityProjectionTVA
     *
     * @return User
     */
    public function setComptabilityProjectionTVA($comptabilityProjectionTVA = null)
    {
        $this->comptabilityProjectionTVA = $comptabilityProjectionTVA;

        return $this;
    }

    /**
     * Get comptabilityProjectionTVA.
     *
     * @return float|null
     */
    public function getComptabilityProjectionTVA()
    {
        return $this->comptabilityProjectionTVA;
    }

    /**
     * Set unsubscribeNotified.
     *
     * @param bool $unsubscribeNotified
     *
     * @return User
     */
    public function setUnsubscribeNotified($unsubscribeNotified)
    {
        $this->unsubscribeNotified = $unsubscribeNotified;

        return $this;
    }

    /**
     * Get unsubscribeNotified.
     *
     * @return bool
     */
    public function getUnsubscribeNotified()
    {
        return $this->unsubscribeNotified;
    }

    /**
     * Add file.
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return User
     */
    public function addFile(\AMZcockpitDoctrine\Entity\File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file.
     *
     * @param \AMZcockpitDoctrine\Entity\File $file
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFile(\AMZcockpitDoctrine\Entity\File $file)
    {
        return $this->files->removeElement($file);
    }

    /**
     * Get files.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set notifEmailsQuotaCount.
     *
     * @param int|null $notifEmailsQuotaCount
     *
     * @return User
     */
    public function setNotifEmailsQuotaCount($notifEmailsQuotaCount = null)
    {
        $this->notifEmailsQuotaCount = $notifEmailsQuotaCount;

        return $this;
    }

    /**
     * Get notifEmailsQuotaCount.
     *
     * @return int|null
     */
    public function getNotifEmailsQuotaCount()
    {
        return $this->notifEmailsQuotaCount;
    }

    /**
     * Set notifInvoicesQuotaCount.
     *
     * @param int|null $notifInvoicesQuotaCount
     *
     * @return User
     */
    public function setNotifInvoicesQuotaCount($notifInvoicesQuotaCount = null)
    {
        $this->notifInvoicesQuotaCount = $notifInvoicesQuotaCount;

        return $this;
    }

    /**
     * Get notifInvoicesQuotaCount.
     *
     * @return int|null
     */
    public function getNotifInvoicesQuotaCount()
    {
        return $this->notifInvoicesQuotaCount;
    }

    /**
     * Set hasIBAN.
     *
     * @param bool|null $hasIBAN
     *
     * @return User
     */
    public function setHasIBAN($hasIBAN = null)
    {
        $this->hasIBAN = $hasIBAN;

        return $this;
    }

    /**
     * Get hasIBAN.
     *
     * @return bool|null
     */
    public function getHasIBAN()
    {
        return $this->hasIBAN;
    }


    /**
     * Set failedSEPAChargingCount.
     *
     * @param int|null $failedSEPAChargingCount
     *
     * @return User
     */
    public function setFailedSEPAChargingCount($failedSEPAChargingCount = null)
    {
        $this->failedSEPAChargingCount = $failedSEPAChargingCount;

        return $this;
    }

    /**
     * Get failedSEPAChargingCount.
     *
     * @return int|null
     */
    public function getFailedSEPAChargingCount()
    {
        return $this->failedSEPAChargingCount;
    }

    /**
     * Set failedCardChargingCount.
     *
     * @param int|null $failedCardChargingCount
     *
     * @return User
     */
    public function setFailedCardChargingCount($failedCardChargingCount = null)
    {
        $this->failedCardChargingCount = $failedCardChargingCount;

        return $this;
    }

    /**
     * Get failedCardChargingCount.
     *
     * @return int|null
     */
    public function getFailedCardChargingCount()
    {
        return $this->failedCardChargingCount;
    }

    /**
     * Add payment.
     *
     * @param \AMZcockpitDoctrine\Entity\Payment $payment
     *
     * @return User
     */
    public function addPayment(\AMZcockpitDoctrine\Entity\Payment $payment)
    {
        $this->payments[] = $payment;

        return $this;
    }

    /**
     * Remove payment.
     *
     * @param \AMZcockpitDoctrine\Entity\Payment $payment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePayment(\AMZcockpitDoctrine\Entity\Payment $payment)
    {
        return $this->payments->removeElement($payment);
    }

    /**
     * Get payments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayments()
    {
        return $this->payments;
    }


    /**
     * Set blockReturnedCustomers.
     *
     * @param bool $blockReturnedCustomers
     *
     * @return User
     */
    public function setBlockReturnedCustomers($blockReturnedCustomers)
    {
        $this->blockReturnedCustomers = $blockReturnedCustomers;

        return $this;
    }

    /**
     * Get blockReturnedCustomers.
     *
     * @return bool
     */
    public function getBlockReturnedCustomers()
    {
        return $this->blockReturnedCustomers;
    }

    /**
     * Set salesJournals.
     *
     * @param array|null $salesJournals
     *
     * @return User
     */
    public function setSalesJournals($salesJournals = null)
    {
        $this->salesJournals = $salesJournals;

        return $this;
    }

    /**
     * Get salesJournals.
     *
     * @return array|null
     */
    public function getSalesJournals()
    {
        return $this->salesJournals;
    }

    /**
     * Set currentSalesJournal.
     *
     * @param integer|null $currentSalesJournal
     *
     * @return User
     */
    public function setCurrentSalesJournal($currentSalesJournal = null)
    {
        $this->currentSalesJournal = $currentSalesJournal;

        return $this;
    }

    /**
     * Get currentSalesJournal.
     *
     * @return integer|null
     */
    public function getCurrentSalesJournal()
    {
        return $this->currentSalesJournal;
    }

    /**
     * Set imAmazonSeller.
     *
     * @param bool|null $imAmazonSeller
     *
     * @return User
     */
    public function setImAmazonSeller($imAmazonSeller = null)
    {
        $this->imAmazonSeller = $imAmazonSeller;

        return $this;
    }

    /**
     * Get imAmazonSeller.
     *
     * @return bool|null
     */
    public function getImAmazonSeller()
    {
        return $this->imAmazonSeller;
    }

    /**
     * Set salesJournalsNC.
     *
     * @param array|null $salesJournalsNC
     *
     * @return User
     */
    public function setSalesJournalsNC($salesJournalsNC = null)
    {
        $this->salesJournalsNC = $salesJournalsNC;

        return $this;
    }

    /**
     * Get salesJournalsNC.
     *
     * @return array|null
     */
    public function getSalesJournalsNC()
    {
        return $this->salesJournalsNC;
    }

    /**
     * Add coupon.
     *
     * @param \AMZcockpitDoctrine\Entity\Coupon $coupon
     *
     * @return User
     */
    public function addCoupon(\AMZcockpitDoctrine\Entity\Coupon $coupon)
    {
        $this->coupons[] = $coupon;

        return $this;
    }

    /**
     * Remove coupon.
     *
     * @param \AMZcockpitDoctrine\Entity\Coupon $coupon
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCoupon(\AMZcockpitDoctrine\Entity\Coupon $coupon)
    {
        return $this->coupons->removeElement($coupon);
    }

    /**
     * Get coupons.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * Set vocalMessageId.
     *
     * @param int|null $vocalMessageId
     *
     * @return User
     */
    public function setVocalMessageId($vocalMessageId = null)
    {
        $this->vocalMessageId = $vocalMessageId;

        return $this;
    }

    /**
     * Get vocalMessageId.
     *
     * @return int|null
     */
    public function getVocalMessageId()
    {
        return $this->vocalMessageId;
    }

    /**
     * Add job.
     *
     * @param \AMZcockpitDoctrine\Entity\Jobs $job
     *
     * @return User
     */
    public function addJob(\AMZcockpitDoctrine\Entity\Jobs $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job.
     *
     * @param \AMZcockpitDoctrine\Entity\Jobs $job
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeJob(\AMZcockpitDoctrine\Entity\Jobs $job)
    {
        return $this->jobs->removeElement($job);
    }

    /**
     * Get jobs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    public function getJobByObject($object)
    {
        if(count($this->getJobs())) {
            /** @var Jobs $job */
            foreach ($this->getJobs() as $job) {
                if($job->getObject() === $object) {
                    return $job;
                }
            }
        }
        return null;
    }

    public function getActiveJobByObject($object)
    {
        if(count($this->getJobs())) {
            /** @var Jobs $job */
            foreach ($this->getJobs() as $job) {
                if($job->getObject() === $object and !in_array($job->getStatus(), [Jobs::COMPLETE, Jobs::ERROR])) {
                    return $job;
                }
            }
        }
        return null;
    }

    /**
     * Set affilaeHasPartnership.
     *
     * @param bool|null $affilaeHasPartnership
     *
     * @return User
     */
    public function setAffilaeHasPartnership($affilaeHasPartnership = null)
    {
        $this->affilaeHasPartnership = $affilaeHasPartnership;

        return $this;
    }

    /**
     * Get affilaeHasPartnership.
     *
     * @return bool|null
     */
    public function getAffilaeHasPartnership()
    {
        return $this->affilaeHasPartnership;
    }

    /**
     * Set affilaePartnershipId.
     *
     * @param int|null $affilaePartnershipId
     *
     * @return User
     */
    public function setAffilaePartnershipId($affilaePartnershipId = null)
    {
        $this->affilaePartnershipId = $affilaePartnershipId;

        return $this;
    }

    /**
     * Get affilaePartnershipId.
     *
     * @return int|null
     */
    public function getAffilaePartnershipId()
    {
        return $this->affilaePartnershipId;
    }
}
