<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * KeywordTracking
 *
 * @ORM\Table(name="keywords_tracking_items")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\KeywordTrackingItemRepository")
 */
class KeywordTrackingItem
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="AMZcockpitDoctrine\Doctrine\RandomIdGenerator")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\KeywordTrackingInstance", mappedBy="keywordTrackingItem", cascade={"persist","remove"})
     */
    protected $instances;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $nextSchedule;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $keyword;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\KeywordTracking", inversedBy="items")
     */
    protected $keywordTracking;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAmazonChoice;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isProcessing;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nextSchedule = (new \DateTime('now'))->add(new \DateInterval('PT2M'));
        $this->instances = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isProcessing = false;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     *
     * @return KeywordTrackingItem
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Add instance
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTrackingInstance $instance
     *
     * @return KeywordTrackingItem
     */
    public function addInstance(\AMZcockpitDoctrine\Entity\KeywordTrackingInstance $instance)
    {
        $this->instances[] = $instance;

        return $this;
    }

    /**
     * Remove instance
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTrackingInstance $instance
     */
    public function removeInstance(\AMZcockpitDoctrine\Entity\KeywordTrackingInstance $instance)
    {
        $this->instances->removeElement($instance);
    }

    /**
     * Get instances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstances()
    {
        return $this->instances;
    }

    /**
     * Set keywordTracking
     *
     * @param \AMZcockpitDoctrine\Entity\KeywordTracking $keywordTracking
     *
     * @return KeywordTrackingItem
     */
    public function setKeywordTracking(\AMZcockpitDoctrine\Entity\KeywordTracking $keywordTracking = null)
    {
        $this->keywordTracking = $keywordTracking;

        return $this;
    }

    /**
     * Get keywordTracking
     *
     * @return \AMZcockpitDoctrine\Entity\KeywordTracking
     */
    public function getKeywordTracking()
    {
        return $this->keywordTracking;
    }

    public function getTrend()
    {
        $count = count($this->getInstances());
        if($count > 1) {
            /** @var KeywordTrackingInstance $last */
            $last = $this->getInstances()->last();
            /** @var KeywordTrackingInstance $beforeLast */
            $beforeLast = $this->getInstances()->get($count-2);
            if($last->getRanking() > ($beforeLast->getRanking() + 3)) {
                return 'down';
            } elseif($last->getRanking() < ($beforeLast->getRanking() - 3)) {
                return 'up';
            } else {
                return 'stable';
            }
        }
        return null;
    }

    /**
     * Set isAmazonChoice
     *
     * @param boolean $isAmazonChoice
     *
     * @return KeywordTrackingItem
     */
    public function setIsAmazonChoice($isAmazonChoice)
    {
        $this->isAmazonChoice = $isAmazonChoice;

        return $this;
    }

    /**
     * Get isAmazonChoice
     *
     * @return boolean
     */
    public function getIsAmazonChoice()
    {
        return $this->isAmazonChoice;
    }

    /**
     * @return null|int
     */
    public function getLastSponsoredRanking()
    {
        /** @var KeywordTrackingInstance $instance */
        $instance = $this->getInstances()->last();
        if($instance) {
            $ranking = $instance->getRanking();
            if($instance->getSponsored())
                return $ranking;
            else {
                /** @var KeywordTrackingInstance $item */
                foreach ($this->getInstances() as $item) {
                    $instance->getDate()->setTime(0, 0, 0);
                    $item->getDate()->setTime(0, 0, 0);
                    if($item->getDate() == $instance->getDate() and $item->getSponsored()) {
                        return $item->getRanking();
                    }

                }
            }
        }
        return null;
    }

    /**
     * @return null|int
     */
    public function getLastNaturalRanking()
    {
        /** @var KeywordTrackingInstance $instance */
        $instance = $this->getInstances()->last();
        if($instance) {
            $ranking = $instance->getRanking();
            if(!$instance->getSponsored())
                return $ranking;
            else {
                /** @var KeywordTrackingInstance $item */
                foreach ($this->getInstances() as $item) {
                    $instance->getDate()->setTime(0, 0, 0);
                    $item->getDate()->setTime(0, 0, 0);
                    if($item->getDate() == $instance->getDate() and !$item->getSponsored()) {
                        return $item->getRanking();
                    }

                }
            }
        }
        return null;
    }

    /**
     * Set nextSchedule.
     *
     * @param \DateTime $nextSchedule
     *
     * @return KeywordTrackingItem
     */
    public function setNextSchedule($nextSchedule)
    {
        $this->nextSchedule = $nextSchedule;

        return $this;
    }

    /**
     * Get nextSchedule.
     *
     * @return \DateTime
     */
    public function getNextSchedule()
    {
        return $this->nextSchedule;
    }

    /**
     * Set isProcessing
     *
     * @param boolean $isProcessing
     *
     * @return KeywordTrackingItem
     */
    public function setIsProcessing($isProcessing)
    {
        $this->isProcessing = $isProcessing;

        return $this;
    }

    /**
     * Get isProcessing
     *
     * @return boolean
     */
    public function getIsProcessing()
    {
        return $this->isProcessing;
    }
}
