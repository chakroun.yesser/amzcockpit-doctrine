<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Product
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\ProductRepository")
 * @UniqueEntity("FNSKU")
 */
class Product
{

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="token", type="uuid", options={"unsigned"=true}, unique=true)
     */
    private $token;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="ASIN", type="string")
     */
    private $ASIN;

    /**
     * @ORM\Column(name="FNSKU", type="string", nullable=true)
     */
    private $FNSKU;

    /**
     * @ORM\Column(name="SKU", type="string", nullable=true)
     */
    private $SKU;

    /**
     * @ORM\Column(name="brand", type="string", nullable=true)
     */
    private $brand;

    /**
     * @ORM\Column(name="item_dimensions", type="array", nullable=true)
     */
    private $itemDimensions;

    /**
     * @ORM\Column(name="package_dimensions", type="array", nullable=true)
     */
    private $packageDimensions;

    /**
     * @ORM\Column(name="part_number", type="string", nullable=true)
     */
    private $partNumber;

    /**
     * @ORM\Column(name="product_group", type="string", nullable=true)
     */
    private $productGroup;

    /**
     * @ORM\Column(name="product_type_name", type="string", nullable=true)
     */
    private $productTypeName;

    /**
     * @ORM\Column(name="publisher", type="text", nullable=true)
     */
    private $publisher;

    /**
     * @ORM\Column(name="isAdultProduct", type="boolean", nullable=true)
     */
    private $isAdultProduct;

    /**
     * @ORM\Column(name="currency", type="string", length=6, nullable=true)
     */
    private $currencyCode;

    /**
     * @ORM\Column(name="item_price_amount", type="float", length=50, nullable=true)
     */
    private $priceAmount;

    /**
     * @ORM\Column(name="small_image", type="string", nullable=true)
     */
    private $smallImage;

    /**
     * @ORM\Column(name="large_image", type="string", nullable=true)
     */
    private $largeImage;

    /**
     * @ORM\Column(name="origin_image", type="string", nullable=true)
     */
    private $originImage;

    /**
     * @ORM\Column(name="studio", type="text", nullable=true)
     */
    private $studio;

    /**
     * @ORM\Column(name="average_rating", type="float", length=50, nullable=true)
     */
    private $averageRating;

    /**
     * @ORM\Column(name="totalSupplyQuantity", type="integer", length=6, nullable=true)
     */
    private $totalSupplyQuantity;

    /**
     * @ORM\Column(name="inStockSupplyQuantity", type="integer", length=6, nullable=true)
     */
    private $inStockSupplyQuantity;

    /**
     * @ORM\Column(name="earliestAvailability", type="string", length=50, nullable=true)
     */
    private $earliestAvailability;

    /**
     * @ORM\Column(name="features", type="array", nullable=true)
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductInfo", mappedBy="product", cascade={"persist","remove"})
     */
    protected $productInfos;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Review", mappedBy="product", cascade={"persist","remove"})
     */
    protected $reviews;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\OrderItem", mappedBy="product", cascade={"persist"})
     */
    protected $ordersItem;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", inversedBy="products")
     * @ORM\JoinColumn(onDelete="cascade")
     */
    protected $store;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", mappedBy="parentProduct", cascade={"persist"})
     */
    protected $childsProducts;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="childsProducts", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $parentProduct;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\Email", mappedBy="products")
     */
    protected $emails;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailProductVariable", mappedBy="product", cascade={"remove"})
     */
    protected $emailsProductVariables;

    /**
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\Column(name="_orders_count", type="integer", nullable=true)
     */
    private $ordersCount;

    /**
     * @ORM\Column(name="alert_reviews", type="boolean", options={"default" : 1}, nullable=true)
     */
    private $alertReviews;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mentionTvaMarge;

    /**
     * @ORM\Column(name="is_archived", type="boolean", nullable=true, options={"default" : 0})
     */
    private $archived;

    /**
     * @ORM\Column(name="process_reviews", type="boolean", options={"default" : 0})
     */
    private $processReviews = false;

    /**
     * @ORM\Column(name="last_process_reviews_exec", type="datetime", nullable=true)
     */
    private $lastProcessReviewsExecutionDate;

    public function __construct($data = null)
    {
        $this->productInfos = new ArrayCollection();
        if(!is_null($data)) {
            $this->token                = Uuid::uuid4();
            $this->ASIN                 = $data['Identifiers']['Request']['Id'];
            $this->brand                = isset($data['AttributeSets'][0]['Brand']) ? $data['AttributeSets'][0]['Brand'] : null;
            $this->itemDimensions       = isset($data['AttributeSets'][0]['ItemDimensions']) ? $data['AttributeSets'][0]['ItemDimensions'] : null;
            $this->packageDimensions    = isset($data['AttributeSets'][0]['PackageDimensions']) ? $data['AttributeSets'][0]['PackageDimensions'] : null;
            $this->isAdultProduct       = isset($data['AttributeSets'][0]['IsAdultProduct']) && $data['AttributeSets'][0]['IsAdultProduct'] == "false" ? false : true;
            if(isset($data['AttributeSets'][0]['ListPrice'])) {
                $this->priceAmount          = isset($data['AttributeSets'][0]['ListPrice']['Amount']) ? $data['AttributeSets'][0]['ListPrice']['Amount'] : null;
                $this->currencyCode         = isset($data['AttributeSets'][0]['ListPrice']['CurrencyCode']) ? $data['AttributeSets'][0]['ListPrice']['CurrencyCode'] : null;
            }
            $this->partNumber           = isset($data['AttributeSets'][0]['PartNumber']) ? $data['AttributeSets'][0]['PartNumber'] : null;
            $this->productGroup         = isset($data['AttributeSets'][0]['ProductGroup']) ? $data['AttributeSets'][0]['ProductGroup'] : null;
            $this->productTypeName      = isset($data['AttributeSets'][0]['ProductTypeName']) ? $data['AttributeSets'][0]['ProductTypeName'] : null;
            $this->publisher            = isset($data['AttributeSets'][0]['Publisher']) ? $data['AttributeSets'][0]['Publisher'] : null;
            $this->smallImage           = isset($data['AttributeSets'][0]['SmallImage']['URL']) ? $data['AttributeSets'][0]['SmallImage']['URL'] : null;
            $this->largeImage           = isset($data['AttributeSets'][0]['SmallImage']['URL']) ? str_replace('_SL75_.','', $data['AttributeSets'][0]['SmallImage']['URL']) : null;
            $this->studio               = isset($data['AttributeSets'][0]['Studio']) ? $data['AttributeSets'][0]['Studio'] : null;
            $this->ordersCount = 0;
            $this->addProductInfo(new ProductInfo($data));
            $this->alertReviews = true;
            $this->archived = false;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set aSIN
     *
     * @param string $aSIN
     *
     * @return Product
     */
    public function setASIN($aSIN)
    {
        $this->ASIN = $aSIN;

        return $this;
    }

    /**
     * Get aSIN
     *
     * @return string
     */
    public function getASIN()
    {
        return $this->ASIN;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Product
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set itemDimensions
     *
     * @param array $itemDimensions
     *
     * @return Product
     */
    public function setItemDimensions($itemDimensions)
    {
        $this->itemDimensions = $itemDimensions;

        return $this;
    }

    /**
     * Get itemDimensions
     *
     * @return array
     */
    public function getItemDimensions()
    {
        return $this->itemDimensions;
    }

    /**
     * Set packageDimensions
     *
     * @param array $packageDimensions
     *
     * @return Product
     */
    public function setPackageDimensions($packageDimensions)
    {
        $this->packageDimensions = $packageDimensions;

        return $this;
    }

    /**
     * Get packageDimensions
     *
     * @return array
     */
    public function getPackageDimensions()
    {
        return $this->packageDimensions;
    }

    /**
     * Set partNumber
     *
     * @param string $partNumber
     *
     * @return Product
     */
    public function setPartNumber($partNumber)
    {
        $this->partNumber = $partNumber;

        return $this;
    }

    /**
     * Get partNumber
     *
     * @return string
     */
    public function getPartNumber()
    {
        return $this->partNumber;
    }

    /**
     * Set productGroup
     *
     * @param string $productGroup
     *
     * @return Product
     */
    public function setProductGroup($productGroup)
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    /**
     * Get productGroup
     *
     * @return string
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }

    /**
     * Set productTypeName
     *
     * @param string $productTypeName
     *
     * @return Product
     */
    public function setProductTypeName($productTypeName)
    {
        $this->productTypeName = $productTypeName;

        return $this;
    }

    /**
     * Get productTypeName
     *
     * @return string
     */
    public function getProductTypeName()
    {
        return $this->productTypeName;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     *
     * @return Product
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set smallImage
     *
     * @param string $smallImage
     *
     * @return Product
     */
    public function setSmallImage($smallImage)
    {
        $this->smallImage = $smallImage;

        return $this;
    }

    /**
     * Get smallImage
     *
     * @return string
     */
    public function getSmallImage()
    {
        $this->smallImage =  str_replace('http://g-ecx.images-amazon.com','https://images-na.ssl-images-amazon.com',$this->smallImage);
        $this->smallImage = str_replace('http://ecx.images-amazon.com','https://images-na.ssl-images-amazon.com',$this->smallImage);
        return $this->smallImage;
    }

    /**
     * Set studio
     *
     * @param string $studio
     *
     * @return Product
     */
    public function setStudio($studio)
    {
        $this->studio = $studio;

        return $this;
    }

    /**
     * Get studio
     *
     * @return string
     */
    public function getStudio()
    {
        return $this->studio;
    }

    /**
     * Set isAdultProduct
     *
     * @param boolean $isAdultProduct
     *
     * @return Product
     */
    public function setIsAdultProduct($isAdultProduct)
    {
        $this->isAdultProduct = $isAdultProduct;

        return $this;
    }

    /**
     * Get isAdultProduct
     *
     * @return boolean
     */
    public function getIsAdultProduct()
    {
        return $this->isAdultProduct;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Product
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set priceAmount
     *
     * @param float $priceAmount
     *
     * @return Product
     */
    public function setPriceAmount($priceAmount)
    {
        $this->priceAmount = $priceAmount;

        return $this;
    }

    /**
     * Get priceAmount
     *
     * @return float
     */
    public function getPriceAmount()
    {
        return $this->priceAmount;
    }

    /**
     * Add productInfo
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductInfo $productInfo
     *
     * @return Product
     */
    public function addProductInfo(\AMZcockpitDoctrine\Entity\MWS\ProductInfo $productInfo)
    {
        $existProductInfo = $this->getProductInfosBymarketPlace($productInfo->getMarketplaceId());
        if(!$existProductInfo) {
            $productInfo->setProduct($this);
            $this->productInfos[] = $productInfo;
        }
        return $this;
    }

    /**
     * Remove productInfo
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductInfo $productInfo
     */
    public function removeProductInfo(\AMZcockpitDoctrine\Entity\MWS\ProductInfo $productInfo)
    {
        $this->productInfos->removeElement($productInfo);
    }

    /**
     * Get productInfos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductInfos()
    {
        return $this->productInfos;
    }


    public function getProductInfosBymarketPlace($marketPlaceId)
    {
        /** @var ProductInfo $productInfo */
        if(count($this->getProductInfos()) > 0) {
            foreach ($this->getProductInfos() as $productInfo) {
                if($productInfo->getMarketplaceId() == $marketPlaceId) {
                    return $productInfo;
                }
            }
        }
        return null;
    }

    /**
     * Set sKU
     *
     * @param string $sKU
     *
     * @return Product
     */
    public function setSKU($sKU)
    {
        $this->SKU = $sKU;

        return $this;
    }

    /**
     * Get sKU
     *
     * @return string
     */
    public function getSKU()
    {
        return $this->SKU;
    }

    /**
     * Set averageRating
     *
     * @param float $averageRating
     *
     * @return Product
     */
    public function setAverageRating($averageRating)
    {
        $this->averageRating = $averageRating;

        return $this;
    }

    /**
     * Get averageRating
     *
     * @return float
     */
    public function getAverageRating()
    {
        return $this->averageRating;
    }

    /**
     * Set totalSupplyQuantity
     *
     * @param integer $totalSupplyQuantity
     *
     * @return Product
     */
    public function setTotalSupplyQuantity($totalSupplyQuantity)
    {
        $this->totalSupplyQuantity = $totalSupplyQuantity;

        return $this;
    }

    /**
     * Get totalSupplyQuantity
     *
     * @return integer
     */
    public function getTotalSupplyQuantity()
    {
        return $this->totalSupplyQuantity;
    }

    /**
     * Set inStockSupplyQuantity
     *
     * @param integer $inStockSupplyQuantity
     *
     * @return Product
     */
    public function setInStockSupplyQuantity($inStockSupplyQuantity)
    {
        $this->inStockSupplyQuantity = (int)$inStockSupplyQuantity;

        return $this;
    }

    /**
     * Get inStockSupplyQuantity
     *
     * @return integer
     */
    public function getInStockSupplyQuantity()
    {
        return $this->inStockSupplyQuantity;
    }

    /**
     * Set earliestAvailability
     *
     * @param string $earliestAvailability
     *
     * @return Product
     */
    public function setEarliestAvailability($earliestAvailability)
    {
        $this->earliestAvailability = $earliestAvailability;

        return $this;
    }

    /**
     * Get earliestAvailability
     *
     * @return string
     */
    public function getEarliestAvailability()
    {
        return $this->earliestAvailability;
    }

    /**
     * Set store
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store $store
     *
     * @return Product
     */
    public function setStore(\AMZcockpitDoctrine\Entity\MWS\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Store
     */
    public function getStore()
    {
        return $this->store;
    }


    public function __toString()
    {
        if(!$this or ($this and !$this->getId())) { return ""; }
        return (string)($this->getDefaultProductInfo() ? $this->getDefaultProductInfo()->getTitle() : '');
    }

    public function getDefaultProductInfo($default = null) {

        if(!$default) {
            $marketPlaceId = null;
            if($this->getStore())
                $marketPlaceId = $this->getStore()->getDefaultMarketPlace();
            $default = $marketPlaceId ? $marketPlaceId : Store::DEFAULT_MARKETPLACE;
        }
        $currentProductInfo = null;
        if(count($this->getProductInfos())) {
            /** @var ProductInfo $productInfo */
            foreach($this->productInfos as $productInfo) {
                if($productInfo->getMarketplaceId() == $default) {
                    return $productInfo;
                }
            }
            return $this->productInfos[0];
        }

        return $currentProductInfo;
    }

    /**
     * Set largeImage
     *
     * @param string $largeImage
     *
     * @return Product
     */
    public function setLargeImage($largeImage)
    {
        $this->largeImage = $largeImage;

        return $this;
    }

    /**
     * Get largeImage
     *
     * @return string
     */
    public function getLargeImage()
    {
        return $this->largeImage;
    }

    /**
     * Add childsProduct
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $childsProduct
     *
     * @return Product
     */
    public function addChildsProduct(\AMZcockpitDoctrine\Entity\MWS\Product $childsProduct)
    {
        $childsProduct->setParentProduct($this);
        $this->childsProducts[] = $childsProduct;

        return $this;
    }

    /**
     * Remove childsProduct
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $childsProduct
     */
    public function removeChildsProduct(\AMZcockpitDoctrine\Entity\MWS\Product $childsProduct)
    {
        $this->childsProducts->removeElement($childsProduct);
    }

    /**
     * Get childsProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildsProducts()
    {
        return $this->childsProducts;
    }

    /**
     * Set parentProduct
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $parentProduct
     *
     * @return Product
     */
    public function setParentProduct(\AMZcockpitDoctrine\Entity\MWS\Product $parentProduct = null)
    {
        $this->parentProduct = $parentProduct;

        return $this;
    }

    /**
     * Get parentProduct
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Product
     */
    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    /**
     * Set features
     *
     * @param array $features
     *
     * @return Product
     */
    public function setFeatures($features)
    {
        $this->features = $features;

        return $this;
    }

    /**
     * Get features
     *
     * @return array
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Add ordersItem
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\OrderItem $ordersItem
     *
     * @return Product
     */
    public function addOrdersItem(\AMZcockpitDoctrine\Entity\MWS\OrderItem $ordersItem)
    {
        $ordersItem->setProduct($this);
        $this->ordersItem[] = $ordersItem;

        return $this;
    }

    /**
     * Remove ordersItem
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\OrderItem $ordersItem
     */
    public function removeOrdersItem(\AMZcockpitDoctrine\Entity\MWS\OrderItem $ordersItem)
    {
        $this->ordersItem->removeElement($ordersItem);
    }

    /**
     * Get ordersItem
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdersItem()
    {
        return $this->ordersItem;
    }

    /**
     * Add review
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Review $review
     *
     * @return Product
     */
    public function addReview(\AMZcockpitDoctrine\Entity\MWS\Review $review)
    {
        $review->setProduct($this);
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Review $review
     */
    public function removeReview(\AMZcockpitDoctrine\Entity\MWS\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Add email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     *
     * @return Product
     */
    public function addEmail(\AMZcockpitDoctrine\Entity\Email $email)
    {
        $this->emails[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \AMZcockpitDoctrine\Entity\Email $email
     */
    public function removeEmail(\AMZcockpitDoctrine\Entity\Email $email)
    {
        $this->emails->removeElement($email);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add emailsProductVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductVariable
     *
     * @return Product
     */
    public function addEmailsProductVariable(\AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductVariable)
    {
        $this->emailsProductVariables[] = $emailsProductVariable;

        return $this;
    }

    /**
     * Remove emailsProductVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductVariable
     */
    public function removeEmailsProductVariable(\AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductVariable)
    {
        $this->emailsProductVariables->removeElement($emailsProductVariable);
    }

    /**
     * Get emailsProductVariables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailsProductVariables()
    {
        return $this->emailsProductVariables;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set originImage
     *
     * @param string $originImage
     *
     * @return Product
     */
    public function setOriginImage($originImage)
    {
        $this->originImage = $originImage;

        return $this;
    }

    /**
     * Get originImage
     *
     * @return string
     */
    public function getOriginImage()
    {
        return $this->originImage;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set fNSKU.
     *
     * @param string|null $fNSKU
     *
     * @return Product
     */
    public function setFNSKU($fNSKU = null)
    {
        $this->FNSKU = $fNSKU;

        return $this;
    }

    /**
     * Get fNSKU.
     *
     * @return string|null
     */
    public function getFNSKU()
    {
        return $this->FNSKU;
    }

    /**
     * Set deleted.
     *
     * @param bool|null $deleted
     *
     * @return Product
     */
    public function setDeleted($deleted = null)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted.
     *
     * @return bool|null
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set ordersCount.
     *
     * @param int|null $ordersCount
     *
     * @return Product
     */
    public function setOrdersCount($ordersCount = null)
    {
        $this->ordersCount = $ordersCount;

        return $this;
    }

    /**
     * Get ordersCount.
     *
     * @return int|null
     */
    public function getOrdersCount()
    {
        if(is_null($this->ordersCount))
            $this->ordersCount = 0;
        return $this->ordersCount;
    }

    /**
     * Set alertReviews.
     *
     * @param bool|null $alertReviews
     *
     * @return Product
     */
    public function setAlertReviews($alertReviews = null)
    {
        $this->alertReviews = $alertReviews;

        return $this;
    }

    /**
     * Get alertReviews.
     *
     * @return bool|null
     */
    public function getAlertReviews()
    {
        return $this->alertReviews;
    }

    /**
     * Set mentionTvaMarge
     *
     * @param boolean $mentionTvaMarge
     *
     * @return Product
     */
    public function setMentionTvaMarge($mentionTvaMarge)
    {
        $this->mentionTvaMarge = $mentionTvaMarge;

        return $this;
    }

    /**
     * Get mentionTvaMarge
     *
     * @return boolean
     */
    public function getMentionTvaMarge()
    {
        return $this->mentionTvaMarge;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return Product
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Set processReviews.
     *
     * @param bool $processReviews
     *
     * @return Product
     */
    public function setProcessReviews($processReviews)
    {
        $this->processReviews = $processReviews;

        return $this;
    }

    /**
     * Get processReviews.
     *
     * @return bool
     */
    public function getProcessReviews()
    {
        return $this->processReviews;
    }

    /**
     * Set lastProcessReviewsExecutionDate.
     *
     * @param \DateTime|null $lastProcessReviewsExecutionDate
     *
     * @return Product
     */
    public function setLastProcessReviewsExecutionDate($lastProcessReviewsExecutionDate = null)
    {
        $this->lastProcessReviewsExecutionDate = $lastProcessReviewsExecutionDate;

        return $this;
    }

    /**
     * Get lastProcessReviewsExecutionDate.
     *
     * @return \DateTime|null
     */
    public function getLastProcessReviewsExecutionDate()
    {
        return $this->lastProcessReviewsExecutionDate;
    }

    /**
     * Set token.
     *
     * @param uuid $token
     *
     * @return Product
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return uuid
     */
    public function getToken()
    {
        return $this->token;
    }
}
