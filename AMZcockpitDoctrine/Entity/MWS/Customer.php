<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Customer
 *
 * @ORM\Table(name="customers")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\CustomerRepository")
 * @UniqueEntity("email")
 */

class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128)
     */
    private $slug;

    /**
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(name="shipping_address_name", type="string", nullable=true)
     */
    private $shippingAddressName;

    /**
     * @ORM\Column(name="shipping_address_adresse_1", type="string", nullable=true)
     */
    private $shippingAddressAdresse1;

    /**
     * @ORM\Column(name="shipping_address_adresse_2", type="string", nullable=true)
     */
    private $shippingAddressAdresse2;

    /**
     * @ORM\Column(name="shipping_address_adresse_3", type="string", nullable=true)
     */
    private $shippingAddressAdresse3;

    /**
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(name="county", type="string", nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(name="district", type="string", nullable=true)
     */
    private $district;

    /**
     * @ORM\Column(name="state_or_region", type="string", nullable=true)
     */
    private $stateOrRegion;

    /**
     * @ORM\Column(name="postal_code", type="string", nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(name="country_code", type="string", nullable=true)
     */
    private $countryCode;

    /**
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", mappedBy="customer", cascade={"persist"})
     */
    protected $orders;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Review", mappedBy="customer")
     */
    protected $reviews;

    /**
     * @ORM\Column(name="market_places", type="array", nullable=true)
     */
    protected $marketPlaces;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Feedback", mappedBy="customer", cascade={"persist"})
     */
    protected $feedback;

    public function __construct($data = null)
    {
        if(!is_null($data)) {
            $this->name                     = isset($data['BuyerName']) ? $data['BuyerName'] : (isset($data['BuyerEmail']) ? $data['BuyerEmail'] : null);
            $this->email                    = isset($data['BuyerEmail']) ? $data['BuyerEmail'] : null;
            if(isset($data['ShippingAddress'])) {
                $this->shippingAddressName      = $data['ShippingAddress']['Name'];
                $this->shippingAddressAdresse1  = $data['ShippingAddress']['AddressLine1'];
                $this->shippingAddressAdresse2  = $data['ShippingAddress']['AddressLine2'];
                $this->shippingAddressAdresse3  = $data['ShippingAddress']['AddressLine3'];
                $this->city                     = $data['ShippingAddress']['City'];
                $this->county                   = $data['ShippingAddress']['County'];
                $this->district                 = $data['ShippingAddress']['District'];
                $this->stateOrRegion            = $data['ShippingAddress']['StateOrRegion'];
                $this->postalCode               = $data['ShippingAddress']['PostalCode'];
                $this->countryCode              = $data['ShippingAddress']['CountryCode'];
                $this->phone                    = $data['ShippingAddress']['Phone'];
                $this->marketPlaces             = [$data['MarketplaceId']];
            }
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set shippingAddressName
     *
     * @param string $shippingAddressName
     *
     * @return Customer
     */
    public function setShippingAddressName($shippingAddressName)
    {
        $this->shippingAddressName = $shippingAddressName;

        return $this;
    }

    /**
     * Get shippingAddressName
     *
     * @return string
     */
    public function getShippingAddressName()
    {
        return $this->shippingAddressName;
    }

    /**
     * Set shippingAddressAdresse1
     *
     * @param string $shippingAddressAdresse1
     *
     * @return Customer
     */
    public function setShippingAddressAdresse1($shippingAddressAdresse1)
    {
        $this->shippingAddressAdresse1 = $shippingAddressAdresse1;

        return $this;
    }

    /**
     * Get shippingAddressAdresse1
     *
     * @return string
     */
    public function getShippingAddressAdresse1()
    {
        return $this->shippingAddressAdresse1;
    }

    /**
     * Set shippingAddressAdresse2
     *
     * @param string $shippingAddressAdresse2
     *
     * @return Customer
     */
    public function setShippingAddressAdresse2($shippingAddressAdresse2)
    {
        $this->shippingAddressAdresse2 = $shippingAddressAdresse2;

        return $this;
    }

    /**
     * Get shippingAddressAdresse2
     *
     * @return string
     */
    public function getShippingAddressAdresse2()
    {
        return $this->shippingAddressAdresse2;
    }

    /**
     * Set shippingAddressAdresse3
     *
     * @param string $shippingAddressAdresse3
     *
     * @return Customer
     */
    public function setShippingAddressAdresse3($shippingAddressAdresse3)
    {
        $this->shippingAddressAdresse3 = $shippingAddressAdresse3;

        return $this;
    }

    /**
     * Get shippingAddressAdresse3
     *
     * @return string
     */
    public function getShippingAddressAdresse3()
    {
        return $this->shippingAddressAdresse3;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set county
     *
     * @param string $county
     *
     * @return Customer
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set district
     *
     * @param string $district
     *
     * @return Customer
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set stateOrRegion
     *
     * @param string $stateOrRegion
     *
     * @return Customer
     */
    public function setStateOrRegion($stateOrRegion)
    {
        $this->stateOrRegion = $stateOrRegion;

        return $this;
    }

    /**
     * Get stateOrRegion
     *
     * @return string
     */
    public function getStateOrRegion()
    {
        return $this->stateOrRegion;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Customer
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return Customer
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return Customer
     */
    public function addOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     */
    public function removeOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set marketPlaces
     *
     * @param array $marketPlaces
     *
     * @return Customer
     */
    public function setMarketPlaces($marketPlaces)
    {
        $this->marketPlaces = $marketPlaces;

        return $this;
    }

    public function addMarketPlace($marketPlace)
    {
        if(!in_array($marketPlace, $this->marketPlaces))
            $this->marketPlaces[] = $marketPlace;

        return $this;
    }

    /**
     * Get marketPlaces
     *
     * @return array
     */
    public function getMarketPlaces()
    {
        return $this->marketPlaces;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Customer
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add feedback
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Feedback $feedback
     *
     * @return Customer
     */
    public function addFeedback(\AMZcockpitDoctrine\Entity\MWS\Feedback $feedback)
    {
        $this->feedback[] = $feedback;

        return $this;
    }

    /**
     * Remove feedback
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Feedback $feedback
     */
    public function removeFeedback(\AMZcockpitDoctrine\Entity\MWS\Feedback $feedback)
    {
        $this->feedback->removeElement($feedback);
    }

    /**
     * Get feedback
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Add review
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Review $review
     *
     * @return Customer
     */
    public function addReview(\AMZcockpitDoctrine\Entity\MWS\Review $review)
    {
        $review->setCustomer($this);
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Review $review
     */
    public function removeReview(\AMZcockpitDoctrine\Entity\MWS\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }
}
