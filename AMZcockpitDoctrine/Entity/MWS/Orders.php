<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Order
 *
 * @ORM\Table(name="orders",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="amazonOrderId",
 *            columns={"amazon_order_id", "store_id"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\OrdersRepository")
 * @UniqueEntity(fields={"amazonOrderId", "store"})
 *
 */
class Orders
{
    const CANCELED  = 'Canceled';
    const DELIVERED = 'Delivered';
    const PENDING   = 'Pending';
    const CONFIRMED = 'Confirmed';
    const RETURNED  = 'Returned';

    const REFUNDED  = 'Refunded';
    const SHIPPED   = 'Shipped';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="amazon_order_id", type="string")
     */
    private $amazonOrderId;

    /**
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(name="purchase_date", type="datetime", nullable=true)
     */
    private $purchaseDate;

    /**
     * @ORM\Column(name="last_update_date", type="datetime", nullable=true)
     */
    private $lastUpdateDate;

    /**
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $orderStatus;

    /**
     * @ORM\Column(name="channel", type="string", length=20, nullable=true)
     */
    private $salesChannel;

    /**
     * @ORM\Column(name="currency", type="string", length=6, nullable=true)
     */
    private $currencyCode;

    /**
     * @ORM\Column(name="order_total", type="float", nullable=true)
     */
    private $orderTotal;

    /**
     * @ORM\Column(name="order_final_total", type="float", nullable=true)
     */
    private $orderFinalTotal;

    /**
     * @ORM\Column(name="tva_amount", type="float", nullable=true)
     */
    private $tvaAmount;

    /**
     * @ORM\Column(name="income", type="float", nullable=true)
     */
    private $income;

    /**
     * @ORM\Column(name="income_gain", type="float", nullable=true)
     */
    private $incomeGain;

    /**
     * @ORM\Column(name="order_type", type="string", length=30)
     */
    private $orderType;

    /**
     * @ORM\Column(name="items_unshipped", type="integer", nullable=true)
     */
    private $numberOfItemsUnshipped;

    /**
     * @ORM\Column(name="items_shipped", type="integer", nullable=true)
     */
    private $numberOfItemsShipped;

    /**
     * @ORM\Column(name="payment_method", type="string", length=25, nullable=true)
     */
    private $paymentMethod;

    /**
     * @ORM\Column(name="buyer_name", type="string", nullable=true)
     */
    private $buyerName;

    /**
     * @ORM\Column(name="buyer_email", type="string", nullable=true)
     */
    private $BuyerEmail;

    /**
     * @ORM\Column(name="shipping_address", type="array", nullable=true)
     */
    private $ShippingAddress;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ShippingPostalCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ShippingCity;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ShippingCountryCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ShippingAddress1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ShippingAddress2;

    /**
     * @ORM\Column(name="shipment_service_level_category", type="string", nullable=true)
     */
    private $shipmentServiceLevelCategory;

    /**
     * @ORM\Column(name="marketplace_Id", type="string")
     */
    private $marketplaceId;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", inversedBy="orders")
     */
    protected $store;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\OrderItem", mappedBy="order", cascade={"persist","remove"}, fetch="EAGER")
     */
    protected $items;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Fee", mappedBy="order", cascade={"persist","remove"})
     */
    protected $fees;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Charge", mappedBy="order", cascade={"persist","remove"})
     */
    protected $charges;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\OrderStatus", mappedBy="order", cascade={"persist","remove"}, fetch="EAGER")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Customer", inversedBy="orders", cascade={"persist"})
     */
    protected $customer;

    /**
     * @ORM\Column(name="carrier", type="string", nullable=true)
     */
    private $carrier;

    /**
     * @ORM\Column(name="tracking_number", type="string", nullable=true)
     */
    private $trackingNumber;

    ## INVOICE INFORMATIONS

    /**
     * @ORM\Column(name="shipping_price", type="string", nullable=true)
     */
    private $shippingPrice;

    /**
     * @ORM\Column(name="shipping_tax", type="string", nullable=true)
     */
    private $shippingTax;

    /**
     * @ORM\Column(name="shipping_discount", type="string", nullable=true)
     */
    private $shippingDiscount;

    /**
     * @ORM\Column(name="gift_wrap_price", type="string", nullable=true)
     */
    private $giftWrapPrice;

    /**
     * @ORM\Column(name="gift_wrap_tax", type="string", nullable=true)
     */
    private $giftWrapTax;

    /**
     * @ORM\Column(name="promotion_discount", type="string", nullable=true)
     */
    private $promotionDiscount;

    /**
     * @ORM\Column(name="billAddress1", type="string", nullable=true)
     */
    private $billAddress1;

    /**
     * @ORM\Column(name="billAddress2", type="string", nullable=true)
     */
    private $billAddress2;

    /**
     * @ORM\Column(name="billAddress3", type="string", nullable=true)
     */
    private $billAddress3;

    /**
     * @ORM\Column(name="billCity", type="string", nullable=true)
     */
    private $billCity;

    /**
     * @ORM\Column(name="BillState", type="string", nullable=true)
     */
    private $billState;

    /**
     * @ORM\Column(name="BillPostalCode", type="string", nullable=true)
     */
    private $billPostalCode;

    /**
     * @ORM\Column(name="BillCountry", type="string", nullable=true)
     */
    private $billCountry;

    /**
     * @ORM\Column(name="manual_bill", type="boolean", nullable=true)
     */
    private $manualBill = false;

    /**
     * @ORM\Column(name="manual_bill_company", type="string", nullable=true)
     */
    private $manualBillCompany;

    /**
     * @ORM\Column(name="manual_bill_buyer_name", type="string", nullable=true)
     */
    private $manualBillBuyerName;

    /**
     * @ORM\Column(name="manual_bill_address", type="string", nullable=true)
     */
    private $manualBillAddress;

    /**
     * @ORM\Column(name="manual_bill_postalcode", type="string", nullable=true)
     */
    private $manualBillPostalCode;

    /**
     * @ORM\Column(name="manual_bill_city", type="string", nullable=true)
     */
    private $manualBillCity;

    /**
     * @ORM\Column(name="manual_bill_country", type="string", nullable=true)
     */
    private $manualBillCountry;


    /**
     * @ORM\Column(name="manual_bill_tva_intracom", type="string", nullable=true)
     */
    private $manualBillTvaIntracom;


    ## FULFILLEMENT

    /**
     * @ORM\Column(name="SellerFulfillmentOrderId", type="string", nullable=true)
     */
    private $sellerFulfillmentOrderId;

    /**
     * @ORM\Column(name="FulfillmentOrderStatus", type="string", nullable=true)
     */
    private $fulfillmentOrderStatus;

    /**
     * @ORM\Column(name="FulfillmentAction", type="string", nullable=true)
     */
    private $fulfillmentAction;

    /**
     * @ORM\Column(name="FulfillmentPolicy", type="string", nullable=true)
     */
    private $fulfillmentPolicy;

    /**
     * @ORM\Column(name="FulfillmentMethod", type="string", nullable=true)
     */
    private $fulfillmentMethod;

    /**
     * @ORM\Column(name="FulfillmentChannel", type="string", nullable=true)
     */
    private $FulfillmentChannel;

    /**
     * @ORM\Column(name="ReceivedDateTime", type="datetime", nullable=true)
     */
    private $receivedDateTime;

    /**
     * @ORM\Column(name="StatusUpdatedDateTime", type="datetime", nullable=true)
     */
    private $statusUpdatedDateTime;

    # Shipment
    /**
     * @ORM\Column(name="shipmentId", type="string", nullable=true)
     */
    private $shipmentId;

    /**
     * @ORM\Column(name="shipmentDate", type="datetime", nullable=true)
     */
    private $shipmentDate;

    /**
     * @ORM\Column(name="estimateArrivalDate", type="datetime", nullable=true)
     */
    private $estimateArrivalDate;

    /**
     * @ORM\Column(name="fulfillmentCenterId", type="string", nullable=true)
     */
    private $fulfillmentCenterId;

    /**
     * @ORM\Column(name="fulfillmentCenterCountryCode", type="string", nullable=true)
     */
    private $fulfillmentCenterCountryCode;

    /**
     * @ORM\Column(name="return_date", type="datetime", nullable=true)
     */
    private $returnDate;

    /**
     * @ORM\Column(name="return_reason", type="string", nullable=true)
     */
    private $returnReason;

    /**
     * @ORM\Column(name="return_status", type="string", nullable=true)
     */
    private $returnStatus;

    /**
     * @ORM\Column(name="refund_id", type="string", nullable=true)
     */
    private $refundId;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Review", mappedBy="order", cascade={"persist","remove"})
     */
    protected $reviews;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailJob", mappedBy="order", cascade={"persist","remove"})
     */
    protected $emailsJobs;

    /**
     * @ORM\Column(name="has_facture", type="boolean")
     */
    private $facture = false;

    /**
     * @ORM\Column(name="sent_facture", type="boolean")
     */
    private $factureSent = false;

    /**
     * @ORM\Column(name="has_pending_facture", type="boolean")
     */
    private $facturePending = false;

    /**
     * @ORM\Column(name="updated", type="boolean")
     */
    private $updated = false;

    /**
     * @ORM\Column(name="isManuelFulfillment", type="boolean")
     */
    private $isManuelFulfillment = false;

    /**
     * @ORM\Column(name="facture_date", type="datetime", nullable=true)
     */
    private $factureDate;

    /**
     * @ORM\Column(name="isCreditNote", type="boolean")
     */
    private $isCreditNote = false;

    /**
     * @ORM\Column(name="isMultisite", type="boolean")
     */
    private $isMultisite = false;

    /**
     * @ORM\Column(name="credit_note_date", type="datetime", nullable=true)
     */
    private $dateCreditNote;

    /**
     * @ORM\Column(name="is_prime", type="boolean", nullable=true)
     */
    private $IsPrime = false;

    /**
     * @ORM\Column(name="is_premium_order", type="boolean", nullable=true)
     */
    private $IsPremiumOrder = false;

    /**
     * @ORM\Column(name="is_business_order", type="boolean", nullable=true)
     */
    private $IsBusinessOrder = false;

    /**
     * @ORM\Column(name="is_replacement_order", type="boolean", nullable=true)
     */
    private $IsReplacementOrder = false;

    /**
     * @ORM\Column(name="replacement_order_id", type="string", nullable=true)
     */
    private $replacedOrderId = false;
    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $originalOrder;

    /**
     * @ORM\Column(name="exchange_rate", type="float", nullable=true)
     */
    private $exchangeRate;

    public function __construct($data = null)
    {
        if(!is_null($data)) {
            $this->amazonOrderId        = $data['AmazonOrderId'];
            $this->purchaseDate         = new \DateTime($data['PurchaseDate']);
            $this->lastUpdateDate       = new \DateTime($data['LastUpdateDate']);
            $this->orderStatus          = $data['OrderStatus'] == "Shipped" ? self::SHIPPED : ($data['OrderStatus'] == "Unshipped" ? self::CONFIRMED : self::PENDING);
            $this->salesChannel         = $data['SalesChannel'];
            $this->ShippingAddress      = isset($data['ShippingAddress']) ? $data['ShippingAddress'] : null;
            $this->ShippingCountryCode  = isset($data['ShippingAddress']) ? $data['ShippingAddress']["CountryCode"] : null;
            $this->ShippingCity         = isset($data['ShippingAddress']) ? $data['ShippingAddress']["City"] : null;
            $this->ShippingPostalCode   = isset($data['ShippingAddress']) ? $data['ShippingAddress']["PostalCode"] : null;
            $this->ShippingAddress1     = isset($data['ShippingAddress']) ? $data['ShippingAddress']["AddressLine1"] : null;
            $this->ShippingAddress2     = isset($data['ShippingAddress']) ? $data['ShippingAddress']["AddressLine2"] : null;
            if(isset($data['OrderTotal'])) {
                $this->currencyCode         = is_null($data['OrderTotal']['CurrencyCode']) ? 'EUR' : $data['OrderTotal']['CurrencyCode'];
                $this->orderTotal           = $data['OrderTotal']['Amount'];
            }
            $this->numberOfItemsShipped = isset($data['NumberOfItemsShipped']) ? $data['NumberOfItemsShipped'] : null;
            $this->numberOfItemsUnshipped = isset($data['NumberOfItemsUnshipped']) ? $data['NumberOfItemsUnshipped'] : null;
            $this->paymentMethod        = isset($data['PaymentMethod']) ? $data['PaymentMethod'] : null;
            $this->marketplaceId        = $data['MarketplaceId'];
            $this->buyerName            = isset($data['BuyerName']) ? $data['BuyerName'] : null;
            $this->BuyerEmail           = isset($data['BuyerEmail']) ? $data['BuyerEmail'] : null;
            $this->orderType            = isset($data['OrderType']) ? $data['OrderType'] : null;
            $this->FulfillmentChannel   = isset($data['FulfillmentChannel']) ? $data['FulfillmentChannel'] : null;
            $this->IsReplacementOrder   = isset($data['IsReplacementOrder']) ? ($data['IsReplacementOrder'] == 'false' ? false : true) : false;
            $this->replacedOrderId   = isset($data['ReplacedOrderId']) ? $data['ReplacedOrderId'] : null;
            $this->IsBusinessOrder   = isset($data['IsBusinessOrder']) ? ($data['IsBusinessOrder'] == 'false' ? false : true) : false;
            $this->IsPrime   = isset($data['IsPrime']) ? ($data['IsPrime'] == 'false' ? false : true) : false;
            $this->IsPremiumOrder   = isset($data['IsPremiumOrder']) ? ($data['IsPremiumOrder'] == 'false' ? false : true) : false;
            $this->shipmentServiceLevelCategory = isset($data['ShipmentServiceLevelCategory']) ? $data['ShipmentServiceLevelCategory'] : null;
            $this->facture = false;
            $this->facturePending = false;
        }
    }

    public function setFulfillementData($data)
    {
        if(!is_null($data)) {
            $this->buyerName            = isset($data['Details']['DestinationAddress']) ? $data['Details']['DestinationAddress']['Name'] : null;
            $this->shipmentId           = isset($data['Shipments']) ? $data['Shipments'][0]["AmazonShipmentId"] : null;
            $this->estimateArrivalDate  = isset($data['Shipments']) ? new \DateTime($data['Shipments'][0]['EstimatedArrivalDateTime']) : null;
            $this->fulfillmentOrderStatus  = isset($data['Details']['FulfillmentOrderStatus']) ? $data['Details']['FulfillmentOrderStatus'] : null;

            if(isset($data['ReceivedDateTime'])) {
                $this->addStatus(new OrderStatus('Shipped', new \DateTime($data['ReceivedDateTime'])));
            }
        }

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amazonOrderId
     *
     * @param string $amazonOrderId
     *
     * @return Orders
     */
    public function setAmazonOrderId($amazonOrderId)
    {
        $this->amazonOrderId = $amazonOrderId;

        return $this;
    }

    /**
     * Get amazonOrderId
     *
     * @return string
     */
    public function getAmazonOrderId()
    {
        return $this->amazonOrderId;
    }

    /**
     * Set purchaseDate
     *
     * @param \DateTime $purchaseDate
     *
     * @return Orders
     */
    public function setPurchaseDate($purchaseDate)
    {
        if($purchaseDate instanceof \DateTime) {
            $this->purchaseDate = $purchaseDate;
        } else {
            $isValid = $this->validateDate($purchaseDate, \DateTime::ISO8601);
            $this->purchaseDate = $isValid ? new \DateTime($purchaseDate) : null;
        }
        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * Set lastUpdateDate
     *
     * @param \DateTime $lastUpdateDate
     *
     * @return Orders
     */
    public function setLastUpdateDate($lastUpdateDate)
    {
        if($lastUpdateDate instanceof \DateTime) {
            $this->lastUpdateDate = $lastUpdateDate;
        } else {
            $isValid = $this->validateDate($lastUpdateDate, \DateTime::ISO8601);
            $this->lastUpdateDate = $isValid ? new \DateTime($lastUpdateDate) : null;
        }

        return $this;
    }

    /**
     * Get lastUpdateDate
     *
     * @return \DateTime
     */
    public function getLastUpdateDate()
    {
        return $this->lastUpdateDate;
    }

    /**
     * Set orderStatus
     *
     * @param string $orderStatus
     *
     * @return Orders
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return string
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set salesChannel
     *
     * @param string $salesChannel
     *
     * @return Orders
     */
    public function setSalesChannel($salesChannel)
    {
        $this->salesChannel = $salesChannel;

        return $this;
    }

    /**
     * Get salesChannel
     *
     * @return string
     */
    public function getSalesChannel()
    {
        return $this->salesChannel;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Orders
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set orderType
     *
     * @param string $orderType
     *
     * @return Orders
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;

        return $this;
    }

    /**
     * Get orderType
     *
     * @return string
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * Set orderTotal
     *
     * @param float $orderTotal
     *
     * @return Orders
     */
    public function setOrderTotal($orderTotal)
    {
        $this->orderTotal = $orderTotal;

        return $this;
    }

    /**
     * Get orderTotal
     *
     * @return float
     */
    public function getOrderTotal()
    {
        return $this->orderTotal;
    }

    /**
     * Set numberOfItemsUnshipped
     *
     * @param integer $numberOfItemsUnshipped
     *
     * @return Orders
     */
    public function setNumberOfItemsUnshipped($numberOfItemsUnshipped)
    {
        $this->numberOfItemsUnshipped = $numberOfItemsUnshipped;

        return $this;
    }

    /**
     * Get numberOfItemsUnshipped
     *
     * @return integer
     */
    public function getNumberOfItemsUnshipped()
    {
        return $this->numberOfItemsUnshipped;
    }

    /**
     * Set numberOfItemsShipped
     *
     * @param integer $numberOfItemsShipped
     *
     * @return Orders
     */
    public function setNumberOfItemsShipped($numberOfItemsShipped)
    {
        $this->numberOfItemsShipped = $numberOfItemsShipped;

        return $this;
    }

    /**
     * Get numberOfItemsShipped
     *
     * @return integer
     */
    public function getNumberOfItemsShipped()
    {
        return $this->numberOfItemsShipped;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     *
     * @return Orders
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set buyerName
     *
     * @param string $buyerName
     *
     * @return Orders
     */
    public function setBuyerName($buyerName)
    {
        $this->buyerName = $buyerName;

        return $this;
    }

    /**
     * Get buyerName
     *
     * @return string
     */
    public function getBuyerName()
    {
        return $this->buyerName;
    }

    /**
     * Set marketplaceId
     *
     * @param string $marketplaceId
     *
     * @return Orders
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return string
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set store
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store $store
     *
     * @return Orders
     */
    public function setStore(\AMZcockpitDoctrine\Entity\MWS\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set buyerEmail
     *
     * @param string $buyerEmail
     *
     * @return Orders
     */
    public function setBuyerEmail($buyerEmail)
    {
        $this->BuyerEmail = $buyerEmail;

        return $this;
    }

    /**
     * Get buyerEmail
     *
     * @return string
     */
    public function getBuyerEmail()
    {
        return $this->BuyerEmail;
    }

    /**
     * Set shippingAddress
     *
     * @param array $shippingAddress
     *
     * @return Orders
     */
    public function setShippingAddress($shippingAddress)
    {
        $this->ShippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * Get shippingAddress
     *
     * @return array
     */
    public function getShippingAddress()
    {
        return $this->ShippingAddress;
    }

    /**
     * Add item
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\OrderItem $item
     *
     * @return Orders
     */
    public function addItem(\AMZcockpitDoctrine\Entity\MWS\OrderItem $item)
    {
        $itemsId = [];
        if($this->getItems()) {
            foreach ($this->getItems() as $orderItem)
                $itemsId[] = $orderItem->getOrderItemId();
        }
        if(!in_array($item->getOrderItemId(),$itemsId)) {
            $item->setOrder($this);
            $this->items[] = $item;
        }
        return $this;
    }

    /**
     * Remove item
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\OrderItem $item
     */
    public function removeItem(\AMZcockpitDoctrine\Entity\MWS\OrderItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set customer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $customer
     *
     * @return Orders
     */
    public function setCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add status
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\OrderStatus $status
     *
     * @return Orders
     */
    public function addStatus(\AMZcockpitDoctrine\Entity\MWS\OrderStatus $status)
    {
        $status->setOrder($this);
        $this->status[] = $status;

        return $this;
    }

    /**
     * Remove status
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\OrderStatus $status
     */
    public function removeStatus(\AMZcockpitDoctrine\Entity\MWS\OrderStatus $status)
    {
        $this->status->removeElement($status);
    }

    /**
     * Get status
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatus()
    {
        if($this->status) {
            $status = new ArrayCollection();
            /** @var OrderStatus $stat */
            foreach ($this->status as $stat) {
                $exist = false;
                foreach ($status as $existStat) {
                    if($stat->getStatus() == $existStat->getStatus()) {
                        $exist = true;
                        break;
                    }
                }
                if(!$exist)
                    $status->add($stat);
            }
            return $status;
        }
        return $this->status;
    }

    /**
     * Set shipmentServiceLevelCategory
     *
     * @param string $shipmentServiceLevelCategory
     *
     * @return Orders
     */
    public function setShipmentServiceLevelCategory($shipmentServiceLevelCategory)
    {
        $this->shipmentServiceLevelCategory = $shipmentServiceLevelCategory;

        return $this;
    }

    /**
     * Get shipmentServiceLevelCategory
     *
     * @return string
     */
    public function getShipmentServiceLevelCategory()
    {
        return $this->shipmentServiceLevelCategory;
    }

    /**
     * Set sellerFulfillmentOrderId
     *
     * @param string $sellerFulfillmentOrderId
     *
     * @return Orders
     */
    public function setSellerFulfillmentOrderId($sellerFulfillmentOrderId)
    {
        $this->sellerFulfillmentOrderId = $sellerFulfillmentOrderId;

        return $this;
    }

    /**
     * Get sellerFulfillmentOrderId
     *
     * @return string
     */
    public function getSellerFulfillmentOrderId()
    {
        return $this->sellerFulfillmentOrderId;
    }

    /**
     * Set fulfillmentOrderStatus
     *
     * @param string $fulfillmentOrderStatus
     *
     * @return Orders
     */
    public function setFulfillmentOrderStatus($fulfillmentOrderStatus)
    {
        $this->fulfillmentOrderStatus = $fulfillmentOrderStatus;

        return $this;
    }

    /**
     * Get fulfillmentOrderStatus
     *
     * @return string
     */
    public function getFulfillmentOrderStatus()
    {
        return $this->fulfillmentOrderStatus;
    }

    /**
     * Set fulfillmentAction
     *
     * @param string $fulfillmentAction
     *
     * @return Orders
     */
    public function setFulfillmentAction($fulfillmentAction)
    {
        $this->fulfillmentAction = $fulfillmentAction;

        return $this;
    }

    /**
     * Get fulfillmentAction
     *
     * @return string
     */
    public function getFulfillmentAction()
    {
        return $this->fulfillmentAction;
    }

    /**
     * Set fulfillmentPolicy
     *
     * @param string $fulfillmentPolicy
     *
     * @return Orders
     */
    public function setFulfillmentPolicy($fulfillmentPolicy)
    {
        $this->fulfillmentPolicy = $fulfillmentPolicy;

        return $this;
    }

    /**
     * Get fulfillmentPolicy
     *
     * @return string
     */
    public function getFulfillmentPolicy()
    {
        return $this->fulfillmentPolicy;
    }

    /**
     * Set fulfillmentMethod
     *
     * @param string $fulfillmentMethod
     *
     * @return Orders
     */
    public function setFulfillmentMethod($fulfillmentMethod)
    {
        $this->fulfillmentMethod = $fulfillmentMethod;

        return $this;
    }

    /**
     * Get fulfillmentMethod
     *
     * @return string
     */
    public function getFulfillmentMethod()
    {
        return $this->fulfillmentMethod;
    }

    /**
     * Set receivedDateTime
     *
     * @param \DateTime $receivedDateTime
     *
     * @return Orders
     */
    public function setReceivedDateTime($receivedDateTime)
    {
        if($receivedDateTime instanceof \DateTime ) {
            $this->receivedDateTime = $receivedDateTime;
        } else {
            $isValid = $this->validateDate($receivedDateTime, \DateTime::ISO8601);
            $this->receivedDateTime = $isValid ? new \DateTime($receivedDateTime) : null;
        }
        return $this;
    }

    /**
     * Get receivedDateTime
     *
     * @return \DateTime
     */
    public function getReceivedDateTime()
    {
        return $this->receivedDateTime;
    }

    /**
     * Set statusUpdatedDateTime
     *
     * @param \DateTime $statusUpdatedDateTime
     *
     * @return Orders
     */
    public function setStatusUpdatedDateTime($statusUpdatedDateTime)
    {
        $isValid = $this->validateDate($statusUpdatedDateTime, \DateTime::ISO8601);
        $this->statusUpdatedDateTime = $isValid ? new \DateTime($statusUpdatedDateTime) : null;

        return $this;
    }

    /**
     * Get statusUpdatedDateTime
     *
     * @return \DateTime
     */
    public function getStatusUpdatedDateTime()
    {
        return $this->statusUpdatedDateTime;
    }

    /**
     * Set carrier
     *
     * @param string $carrier
     *
     * @return Orders
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set trackingNumber
     *
     * @param string $trackingNumber
     *
     * @return Orders
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    /**
     * Get trackingNumber
     *
     * @return string
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * Set giftWrapPrice
     *
     * @param string $giftWrapPrice
     *
     * @return Orders
     */
    public function setGiftWrapPrice($giftWrapPrice)
    {
        $this->giftWrapPrice = $giftWrapPrice;

        return $this;
    }

    /**
     * Get giftWrapPrice
     *
     * @return string
     */
    public function getGiftWrapPrice()
    {
        return $this->giftWrapPrice;
    }

    /**
     * Set giftWrapTax
     *
     * @param string $giftWrapTax
     *
     * @return Orders
     */
    public function setGiftWrapTax($giftWrapTax)
    {
        $this->giftWrapTax = $giftWrapTax;

        return $this;
    }

    /**
     * Get giftWrapTax
     *
     * @return string
     */
    public function getGiftWrapTax()
    {
        return $this->giftWrapTax;
    }

    /**
     * Set billAddress1
     *
     * @param string $billAddress1
     *
     * @return Orders
     */
    public function setBillAddress1($billAddress1)
    {
        $this->billAddress1 = $billAddress1;

        return $this;
    }

    /**
     * Get billAddress1
     *
     * @return string
     */
    public function getBillAddress1()
    {
        return $this->billAddress1;
    }

    /**
     * Set billAddress2
     *
     * @param string $billAddress2
     *
     * @return Orders
     */
    public function setBillAddress2($billAddress2)
    {
        $this->billAddress2 = $billAddress2;

        return $this;
    }

    /**
     * Get billAddress2
     *
     * @return string
     */
    public function getBillAddress2()
    {
        return $this->billAddress2;
    }

    /**
     * Set billAddress3
     *
     * @param string $billAddress3
     *
     * @return Orders
     */
    public function setBillAddress3($billAddress3)
    {
        $this->billAddress3 = $billAddress3;

        return $this;
    }

    /**
     * Get billAddress3
     *
     * @return string
     */
    public function getBillAddress3()
    {
        return $this->billAddress3;
    }

    /**
     * Set billCity
     *
     * @param string $billCity
     *
     * @return Orders
     */
    public function setBillCity($billCity)
    {
        $this->billCity = $billCity;

        return $this;
    }

    /**
     * Get billCity
     *
     * @return string
     */
    public function getBillCity()
    {
        return $this->billCity;
    }

    /**
     * Set billState
     *
     * @param string $billState
     *
     * @return Orders
     */
    public function setBillState($billState)
    {
        $this->billState = $billState;

        return $this;
    }

    /**
     * Get billState
     *
     * @return string
     */
    public function getBillState()
    {
        return $this->billState;
    }

    /**
     * Set billPostalCode
     *
     * @param string $billPostalCode
     *
     * @return Orders
     */
    public function setBillPostalCode($billPostalCode)
    {
        $this->billPostalCode = $billPostalCode;

        return $this;
    }

    /**
     * Get billPostalCode
     *
     * @return string
     */
    public function getBillPostalCode()
    {
        return $this->billPostalCode;
    }

    /**
     * Set billCountry
     *
     * @param string $billCountry
     *
     * @return Orders
     */
    public function setBillCountry($billCountry)
    {
        $this->billCountry = $billCountry;

        return $this;
    }

    /**
     * Get billCountry
     *
     * @return string
     */
    public function getBillCountry()
    {
        return $this->billCountry;
    }

    /**
     * Set shipmentId
     *
     * @param string $shipmentId
     *
     * @return Orders
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;

        return $this;
    }

    /**
     * Get shipmentId
     *
     * @return string
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * Set shipmentDate
     *
     * @param \DateTime $shipmentDate
     *
     * @return Orders
     */
    public function setShipmentDate($shipmentDate)
    {
        $isValid = $this->validateDate($shipmentDate, \DateTime::ISO8601);
        $this->shipmentDate = $isValid ? new \DateTime($shipmentDate) : null;

        return $this;
    }

    /**
     * Get shipmentDate
     *
     * @return \DateTime
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }

    /**
     * Set estimateArrivalDate
     *
     * @param \DateTime $estimateArrivalDate
     *
     * @return Orders
     */
    public function setEstimateArrivalDate($estimateArrivalDate)
    {
        $isValid = $this->validateDate($estimateArrivalDate, \DateTime::ISO8601);
        $this->estimateArrivalDate = $isValid ? new \DateTime($estimateArrivalDate) : null;

        return $this;
    }

    /**
     * Get estimateArrivalDate
     *
     * @return \DateTime
     */
    public function getEstimateArrivalDate()
    {
        return $this->estimateArrivalDate;
    }

    /**
     * Add review
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Review $review
     *
     * @return Orders
     */
    public function addReview(\AMZcockpitDoctrine\Entity\MWS\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Review $review
     */
    public function removeReview(\AMZcockpitDoctrine\Entity\MWS\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set returnDate
     *
     * @param \DateTime $returnDate
     *
     * @return Orders
     */
    public function setReturnDate($returnDate)
    {
        if($returnDate instanceof \DateTime) {
            $this->returnDate = $returnDate;
        } else {
            $this->returnDate = new \DateTime($returnDate);
        }

        return $this;
    }

    /**
     * Get returnDate
     *
     * @return \DateTime
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * Set returnReason
     *
     * @param string $returnReason
     *
     * @return Orders
     */
    public function setReturnReason($returnReason)
    {
        $this->returnReason = $returnReason;

        return $this;
    }

    /**
     * Get returnReason
     *
     * @return string
     */
    public function getReturnReason()
    {
        return $this->returnReason;
    }

    /**
     * Set returnStatus
     *
     * @param string $returnStatus
     *
     * @return Orders
     */
    public function setReturnStatus($returnStatus)
    {
        $this->returnStatus = $returnStatus;

        return $this;
    }

    /**
     * Get returnStatus
     *
     * @return string
     */
    public function getReturnStatus()
    {
        return $this->returnStatus;
    }

    /**
     * Set refundId
     *
     * @param integer $refundId
     *
     * @return Orders
     */
    public function setRefundId($refundId)
    {
        $this->refundId = $refundId;

        return $this;
    }

    /**
     * Get refundId
     *
     * @return integer
     */
    public function getRefundId()
    {
        return $this->refundId;
    }

    /**
     * Set shippingPrice
     *
     * @param string $shippingPrice
     *
     * @return Orders
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    /**
     * Get shippingPrice
     *
     * @return integer
     */
    public function getShippingPrice()
    {
        if(is_null($this->shippingPrice)) {
            $this->shippingPrice = 0;
            /** @var \AMZcockpitDoctrine\Entity\MWS\OrderItem $item */
            foreach ($this->items as $item) {
                $this->shippingPrice += $item->getShippingPriceAmount();
            }
        }
        return $this->shippingPrice;
    }

    /**
     * Set shippingTax
     *
     * @param string $shippingTax
     *
     * @return Orders
     */
    public function setShippingTax($shippingTax)
    {
        $this->shippingTax = $shippingTax;

        return $this;
    }

    /**
     * Get shippingTax
     *
     * @return integer
     */
    public function getShippingTax()
    {
        if(is_null($this->shippingTax)) {
            $this->shippingTax = 0;
            /** @var \AMZcockpitDoctrine\Entity\MWS\OrderItem $item */
            foreach ($this->items as $item) {
                $this->shippingTax += $item->getItemTaxPriceAmount();
            }
        }

        return $this->shippingTax;
    }

    /**
     * Set promotionDiscount
     *
     * @param string $promotionDiscount
     *
     * @return Orders
     */
    public function setPromotionDiscount($promotionDiscount)
    {
        $this->promotionDiscount = $promotionDiscount;

        return $this;
    }

    /**
     * Get promotionDiscount
     *
     * @return string
     */
    public function getPromotionDiscount()
    {
        if(is_null($this->promotionDiscount)) {
            $this->promotionDiscount = 0;
            /** @var \AMZcockpitDoctrine\Entity\MWS\OrderItem $item */
            foreach ($this->items as $item) {
                $this->promotionDiscount += $item->getPromotionDiscount();
            }
        }

        return $this->promotionDiscount;
    }

    /**
     * Set shippingDiscount
     *
     * @param string $shippingDiscount
     *
     * @return Orders
     */
    public function setShippingDiscount($shippingDiscount)
    {
        $this->shippingDiscount = $shippingDiscount;

        return $this;
    }

    /**
     * Get shippingDiscount
     *
     * @return string
     */
    public function getShippingDiscount()
    {
        if(is_null($this->shippingDiscount)) {
            $this->shippingDiscount = 0;
            /** @var \AMZcockpitDoctrine\Entity\MWS\OrderItem $item */
            foreach ($this->items as $item) {
                $this->shippingDiscount += $item->getShippingDiscount();
            }
        }

        return $this->shippingDiscount;
    }

    /**
     * Set facture
     *
     * @param boolean $facture
     *
     * @return Orders
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;
//        $this->factureDate = $facture ? new \DateTime('now') : null;

        return $this;
    }

    /**
     * Get facture
     *
     * @return boolean
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * Set fulfillmentChannel
     *
     * @param string $fulfillmentChannel
     *
     * @return Orders
     */
    public function setFulfillmentChannel($fulfillmentChannel)
    {
        $this->FulfillmentChannel = $fulfillmentChannel;

        return $this;
    }

    /**
     * Get fulfillmentChannel
     *
     * @return string
     */
    public function getFulfillmentChannel()
    {
        return $this->FulfillmentChannel;
    }

    /**
     * Add emailsJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailsJob
     *
     * @return Orders
     */
    public function addEmailsJob(\AMZcockpitDoctrine\Entity\EmailJob $emailsJob)
    {
        $this->emailsJobs[] = $emailsJob;

        return $this;
    }

    /**
     * Remove emailsJob
     *
     * @param \AMZcockpitDoctrine\Entity\EmailJob $emailsJob
     */
    public function removeEmailsJob(\AMZcockpitDoctrine\Entity\EmailJob $emailsJob)
    {
        $this->emailsJobs->removeElement($emailsJob);
    }

    /**
     * Get emailsJobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailsJobs()
    {
        return $this->emailsJobs;
    }

    /**
     * Set facturePending
     *
     * @param boolean $facturePending
     *
     * @return Orders
     */
    public function setFacturePending($facturePending)
    {
        $this->facturePending = $facturePending;

        return $this;
    }

    /**
     * Get facturePending
     *
     * @return boolean
     */
    public function getFacturePending()
    {
        return $this->facturePending;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Orders
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set factureSent
     *
     * @param boolean $factureSent
     *
     * @return Orders
     */
    public function setFactureSent($factureSent)
    {
        $this->factureSent = $factureSent;

        return $this;
    }

    /**
     * Get factureSent
     *
     * @return boolean
     */
    public function getFactureSent()
    {
        return $this->factureSent;
    }

    /**
     * Set orderFinalTotal
     *
     * @param float $orderFinalTotal
     *
     * @return Orders
     */
    public function setOrderFinalTotal($orderFinalTotal)
    {
        $this->orderFinalTotal = $orderFinalTotal;

        return $this;
    }

    /**
     * Get orderFinalTotal
     *
     * @return float
     */
    public function getOrderFinalTotal()
    {
        return $this->orderFinalTotal;
    }

    /**
     * Set updated
     *
     * @param boolean $updated
     *
     * @return Orders
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return boolean
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set shippingPostalCode
     *
     * @param string $shippingPostalCode
     *
     * @return Orders
     */
    public function setShippingPostalCode($shippingPostalCode)
    {
        $this->ShippingPostalCode = $shippingPostalCode;

        return $this;
    }

    /**
     * Get shippingPostalCode
     *
     * @return string
     */
    public function getShippingPostalCode()
    {
        return $this->ShippingPostalCode;
    }

    /**
     * Set shippingCountryCode
     *
     * @param string $shippingCountryCode
     *
     * @return Orders
     */
    public function setShippingCountryCode($shippingCountryCode)
    {
        $this->ShippingCountryCode = $shippingCountryCode;

        return $this;
    }

    /**
     * Get shippingCountryCode
     *
     * @return string
     */
    public function getShippingCountryCode()
    {
        return $this->ShippingCountryCode;
    }

    /**
     * Set shippingAddress1
     *
     * @param string $shippingAddress1
     *
     * @return Orders
     */
    public function setShippingAddress1($shippingAddress1)
    {
        $this->ShippingAddress1 = $shippingAddress1;

        return $this;
    }

    /**
     * Get shippingAddress1
     *
     * @return string
     */
    public function getShippingAddress1()
    {
        return $this->ShippingAddress1;
    }

    /**
     * Set shippingAddress2
     *
     * @param string $shippingAddress2
     *
     * @return Orders
     */
    public function setShippingAddress2($shippingAddress2)
    {
        $this->ShippingAddress2 = $shippingAddress2;

        return $this;
    }

    /**
     * Get shippingAddress2
     *
     * @return string
     */
    public function getShippingAddress2()
    {
        return $this->ShippingAddress2;
    }

    /**
     * Set shippingCity
     *
     * @param string $shippingCity
     *
     * @return Orders
     */
    public function setShippingCity($shippingCity)
    {
        $this->ShippingCity = $shippingCity;

        return $this;
    }

    /**
     * Get shippingCity
     *
     * @return string
     */
    public function getShippingCity()
    {
        return $this->ShippingCity;
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Set factureDate
     *
     * @param \DateTime $factureDate
     *
     * @return Orders
     */
    public function setFactureDate($factureDate)
    {
        $this->factureDate = $factureDate;

        return $this;
    }

    /**
     * Get factureDate
     *
     * @return \DateTime
     */
    public function getFactureDate()
    {
        return $this->factureDate;
    }

    /**
     * Add fee
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Fee $fee
     *
     * @return Orders
     */
    public function addFee(\AMZcockpitDoctrine\Entity\MWS\Fee $fee)
    {
        $fee->setOrder($this);
        if(count($this->fees) > 0) {
            /** @var \AMZcockpitDoctrine\Entity\MWS\Fee $cFee */
            foreach ($this->fees as $cFee) {
                if($cFee->getType() == $fee->getType())
                    return $this;
            }
        }
        $this->fees[] = $fee;

        return $this;
    }

    /**
     * Remove fee
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Fee $fee
     */
    public function removeFee(\AMZcockpitDoctrine\Entity\MWS\Fee $fee)
    {
        $this->fees->removeElement($fee);
    }

    /**
     * Get fees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * Set isManuelFulfillment
     *
     * @param boolean $isManuelFulfillment
     *
     * @return Orders
     */
    public function setIsManuelFulfillment($isManuelFulfillment)
    {
        $this->isManuelFulfillment = $isManuelFulfillment;

        return $this;
    }

    /**
     * Get isManuelFulfillment
     *
     * @return boolean
     */
    public function getIsManuelFulfillment()
    {
        return $this->isManuelFulfillment;
    }

    /**
     * Set isCreditNote
     *
     * @param boolean $isCreditNote
     *
     * @return Orders
     */
    public function setIsCreditNote($isCreditNote)
    {
        $this->isCreditNote = $isCreditNote;

        return $this;
    }

    /**
     * Get isCreditNote
     *
     * @return boolean
     */
    public function getIsCreditNote()
    {
        return $this->isCreditNote;
    }

    /**
     * Set dateCreditNote
     *
     * @param \DateTime $dateCreditNote
     *
     * @return Orders
     */
    public function setDateCreditNote($dateCreditNote)
    {
        $this->dateCreditNote = $dateCreditNote;

        return $this;
    }

    /**
     * Get dateCreditNote
     *
     * @return \DateTime
     */
    public function getDateCreditNote()
    {
        return $this->dateCreditNote;
    }

    /**
     * Set originalOrder
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $originalOrder
     *
     * @return Orders
     */
    public function setOriginalOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $originalOrder = null)
    {
        $this->originalOrder = $originalOrder;

        return $this;
    }

    /**
     * Get originalOrder
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOriginalOrder()
    {
        return $this->originalOrder;
    }

    /**
     * Add charge
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Charge $charge
     *
     * @return Orders
     */
    public function addCharge(\AMZcockpitDoctrine\Entity\MWS\Charge $charge)
    {
        $charge->setOrder($this);
        $this->charges[] = $charge;

        return $this;
    }

    /**
     * Remove charge
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Charge $charge
     */
    public function removeCharge(\AMZcockpitDoctrine\Entity\MWS\Charge $charge)
    {
        $this->charges->removeElement($charge);
    }

    /**
     * Get charges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * Set manualBill
     *
     * @param boolean $manualBill
     *
     * @return Orders
     */
    public function setManualBill($manualBill)
    {
        $this->manualBill = $manualBill;

        return $this;
    }

    /**
     * Get manualBill
     *
     * @return boolean
     */
    public function getManualBill()
    {
        return $this->manualBill;
    }

    /**
     * Set manualBillCompany
     *
     * @param string $manualBillCompany
     *
     * @return Orders
     */
    public function setManualBillCompany($manualBillCompany)
    {
        $this->manualBillCompany = $manualBillCompany;

        return $this;
    }

    /**
     * Get manualBillCompany
     *
     * @return string
     */
    public function getManualBillCompany()
    {
        return $this->manualBillCompany;
    }

    /**
     * Set manualBillBuyerName
     *
     * @param string $manualBillBuyerName
     *
     * @return Orders
     */
    public function setManualBillBuyerName($manualBillBuyerName)
    {
        $this->manualBillBuyerName = $manualBillBuyerName;

        return $this;
    }

    /**
     * Get manualBillBuyerName
     *
     * @return string
     */
    public function getManualBillBuyerName()
    {
        return $this->manualBillBuyerName;
    }

    /**
     * Set manualBillAddress
     *
     * @param string $manualBillAddress
     *
     * @return Orders
     */
    public function setManualBillAddress($manualBillAddress)
    {
        $this->manualBillAddress = $manualBillAddress;

        return $this;
    }

    /**
     * Get manualBillAddress
     *
     * @return string
     */
    public function getManualBillAddress()
    {
        return $this->manualBillAddress;
    }

    /**
     * Set manualBillCountry
     *
     * @param string $manualBillCountry
     *
     * @return Orders
     */
    public function setManualBillCountry($manualBillCountry)
    {
        $this->manualBillCountry = $manualBillCountry;

        return $this;
    }

    /**
     * Get manualBillCountry
     *
     * @return string
     */
    public function getManualBillCountry()
    {
        return $this->manualBillCountry;
    }

    /**
     * Set manualBillCity
     *
     * @param string $manualBillCity
     *
     * @return Orders
     */
    public function setManualBillCity($manualBillCity)
    {
        $this->manualBillCity = $manualBillCity;

        return $this;
    }

    /**
     * Get manualBillCity
     *
     * @return string
     */
    public function getManualBillCity()
    {
        return $this->manualBillCity;
    }

    /**
     * Set manualBillPostalCode
     *
     * @param string $manualBillPostalCode
     *
     * @return Orders
     */
    public function setManualBillPostalCode($manualBillPostalCode)
    {
        $this->manualBillPostalCode = $manualBillPostalCode;

        return $this;
    }

    /**
     * Get manualBillPostalCode
     *
     * @return string
     */
    public function getManualBillPostalCode()
    {
        return $this->manualBillPostalCode;
    }

    /**
     * Set manualBillTvaIntracom
     *
     * @param string $manualBillTvaIntracom
     *
     * @return Orders
     */
    public function setManualBillTvaIntracom($manualBillTvaIntracom)
    {
        $this->manualBillTvaIntracom = $manualBillTvaIntracom;

        return $this;
    }

    /**
     * Get manualBillTvaIntracom
     *
     * @return string
     */
    public function getManualBillTvaIntracom()
    {
        return $this->manualBillTvaIntracom;
    }

    /**
     * Set isMultisite
     *
     * @param boolean $isMultisite
     *
     * @return Orders
     */
    public function setIsMultisite($isMultisite)
    {
        $this->isMultisite = $isMultisite;

        return $this;
    }

    /**
     * Get isMultisite
     *
     * @return boolean
     */
    public function getIsMultisite()
    {
        return $this->isMultisite;
    }

    /**
     * Set isPrime
     *
     * @param boolean $isPrime
     *
     * @return Orders
     */
    public function setIsPrime($isPrime)
    {
        $this->IsPrime = $isPrime;

        return $this;
    }

    /**
     * Get isPrime
     *
     * @return boolean
     */
    public function getIsPrime()
    {
        return $this->IsPrime;
    }

    /**
     * Set isPremiumOrder
     *
     * @param boolean $isPremiumOrder
     *
     * @return Orders
     */
    public function setIsPremiumOrder($isPremiumOrder)
    {
        $this->IsPremiumOrder = $isPremiumOrder;

        return $this;
    }

    /**
     * Get isPremiumOrder
     *
     * @return boolean
     */
    public function getIsPremiumOrder()
    {
        return $this->IsPremiumOrder;
    }

    /**
     * Set isBusinessOrder
     *
     * @param boolean $isBusinessOrder
     *
     * @return Orders
     */
    public function setIsBusinessOrder($isBusinessOrder)
    {
        $this->IsBusinessOrder = $isBusinessOrder;

        return $this;
    }

    /**
     * Get isBusinessOrder
     *
     * @return boolean
     */
    public function getIsBusinessOrder()
    {
        return $this->IsBusinessOrder;
    }

    /**
     * Set isReplacementOrder
     *
     * @param boolean $isReplacementOrder
     *
     * @return Orders
     */
    public function setIsReplacementOrder($isReplacementOrder)
    {
        $this->IsReplacementOrder = $isReplacementOrder;

        return $this;
    }

    /**
     * Get isReplacementOrder
     *
     * @return boolean
     */
    public function getIsReplacementOrder()
    {
        return $this->IsReplacementOrder;
    }

    /**
     * Set replacedOrderId
     *
     * @param string $replacedOrderId
     *
     * @return Orders
     */
    public function setReplacedOrderId($replacedOrderId)
    {
        $this->replacedOrderId = $replacedOrderId;

        return $this;
    }

    /**
     * Get replacedOrderId
     *
     * @return string
     */
    public function getReplacedOrderId()
    {
        return $this->replacedOrderId;
    }

    /**
     * Set fulfillmentCenterId
     *
     * @param string $fulfillmentCenterId
     *
     * @return Orders
     */
    public function setFulfillmentCenterId($fulfillmentCenterId)
    {
        $this->fulfillmentCenterId = $fulfillmentCenterId;

        return $this;
    }

    /**
     * Get fulfillmentCenterId
     *
     * @return string
     */
    public function getFulfillmentCenterId()
    {
        return $this->fulfillmentCenterId;
    }

    /**
     * Set tvaAmount.
     *
     * @param float|null $tvaAmount
     *
     * @return Orders
     */
    public function setTvaAmount($tvaAmount = null)
    {
        $this->tvaAmount = $tvaAmount;

        return $this;
    }

    /**
     * Get tvaAmount.
     *
     * @return float|null
     */
    public function getTvaAmount()
    {
        return $this->tvaAmount;
    }

    /**
     * Set income.
     *
     * @param float|null $income
     *
     * @return Orders
     */
    public function setIncome($income = null)
    {
        $this->income = $income;

        return $this;
    }

    /**
     * Get income.
     *
     * @return float|null
     */
    public function getIncome()
    {
        return $this->income;
    }

    /**
     * Set exchangeRate.
     *
     * @param float|null $exchangeRate
     *
     * @return Orders
     */
    public function setExchangeRate($exchangeRate = null)
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }

    /**
     * Get exchangeRate.
     *
     * @return float|null
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set incomeGain.
     *
     * @param float|null $incomeGain
     *
     * @return Orders
     */
    public function setIncomeGain($incomeGain = null)
    {
        $this->incomeGain = $incomeGain;

        return $this;
    }

    /**
     * Get incomeGain.
     *
     * @return float|null
     */
    public function getIncomeGain()
    {
        return $this->incomeGain;
    }

    /**
     * Set fulfillmentCenterCountryCode.
     *
     * @param string|null $fulfillmentCenterCountryCode
     *
     * @return Orders
     */
    public function setFulfillmentCenterCountryCode($fulfillmentCenterCountryCode = null)
    {
        $this->fulfillmentCenterCountryCode = $fulfillmentCenterCountryCode;

        return $this;
    }

    /**
     * Get fulfillmentCenterCountryCode.
     *
     * @return string|null
     */
    public function getFulfillmentCenterCountryCode()
    {
        return $this->fulfillmentCenterCountryCode;
    }
}
