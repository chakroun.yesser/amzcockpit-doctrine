<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductCategory
 *
 * @ORM\Table(name="product_category")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\ProductCategoryRepository")
 */
class ProductCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="categoryId", type="string")
     */
    private $categoryId;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $categoryName;

    /**
     * @ORM\Column(name="rank", type="string", nullable=true)
     */
    private $rank;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductCategoryRank", mappedBy="productCategory", cascade={"persist","remove"})
     */
    protected $rankings;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductCategory", mappedBy="parentCategory", cascade={"persist"},  fetch="EAGER")
     */
    protected $childsCategory;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductCategory", inversedBy="childsCategory", cascade={"persist"},  fetch="EAGER")
     */
    protected $parentCategory;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductInfo", mappedBy="productInfosCategorie", cascade={"persist"})
     */
    protected $productsInfos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childsCategory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryId
     *
     * @param string $categoryId
     *
     * @return ProductCategory
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return string
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     *
     * @return ProductCategory
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * Add childsCategory
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategory $childsCategory
     *
     * @return ProductCategory
     */
    public function addChildsCategory(\AMZcockpitDoctrine\Entity\MWS\ProductCategory $childsCategory)
    {
        if(!$this->getChildsCategory() ||  !$this->getChildsCategory()->contains($childsCategory)) {
            $childsCategory->setParentCategory($this);
            $this->childsCategory[] = $childsCategory;
        }
        return $this;
    }

    /**
     * Remove childsCategory
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategory $childsCategory
     */
    public function removeChildsCategory(\AMZcockpitDoctrine\Entity\MWS\ProductCategory $childsCategory)
    {
        $this->childsCategory->removeElement($childsCategory);
    }

    /**
     * Get childsCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildsCategory()
    {
        return $this->childsCategory;
    }

    /**
     * Set parentCategory
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategory $parentCategory
     *
     * @return ProductCategory
     */
    public function setParentCategory(\AMZcockpitDoctrine\Entity\MWS\ProductCategory $parentCategory = null)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * Get parentCategory
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\ProductCategory
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * Set rank
     *
     * @param string $rank
     *
     * @return ProductCategory
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Add ranking
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategoryRank $ranking
     *
     * @return ProductCategory
     */
    public function addRanking(\AMZcockpitDoctrine\Entity\MWS\ProductCategoryRank $ranking)
    {
        $ranking->setProductCategory($this);
        $this->rankings[] = $ranking;

        return $this;
    }

    /**
     * Remove ranking
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategoryRank $ranking
     */
    public function removeRanking(\AMZcockpitDoctrine\Entity\MWS\ProductCategoryRank $ranking)
    {
        $this->rankings->removeElement($ranking);
    }

    /**
     * Get rankings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRankings()
    {
        return $this->rankings;
    }

    /**
     * Add productsInfo
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductInfo $productsInfo
     *
     * @return ProductCategory
     */
    public function addProductsInfo(\AMZcockpitDoctrine\Entity\MWS\ProductInfo $productsInfo)
    {
        $this->productsInfos[] = $productsInfo;
        return $this;
    }

    /**
     * Remove productsInfo
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductInfo $productsInfo
     */
    public function removeProductsInfo(\AMZcockpitDoctrine\Entity\MWS\ProductInfo $productsInfo)
    {
        $this->productsInfos->removeElement($productsInfo);
    }

    /**
     * Get productsInfos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsInfos()
    {
        return $this->productsInfos;
    }

    public function getUpOrDownRanking() {
        $count = count($this->getRankings());
        if($count <= 1)
            return null;

        $lastRank       = $this->getRankings()[$count - 1 ];
        $beforeLastRank = $this->getRankings()[$count - 2];

        if($lastRank->getValue() > $beforeLastRank->getvalue()) return true;
        if($lastRank->getValue() < $beforeLastRank->getvalue()) return false;
        return 'equal';
    }
}
