<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductCategoryRank
 *
 * @ORM\Table(name="product_category_rank")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\ProductCategoryRankRepository")
 */
class ProductCategoryRank
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductCategory", inversedBy="rankings", cascade={"persist"})
     */
    protected $productCategory;

    /**
     * @ORM\Column(name="rank", type="integer", length=50)
     */
    private $value;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct($value)
    {
        $this->date  = new \DateTime('today');
        $this->value = $value;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return ProductCategoryRank
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ProductCategoryRank
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set productCategory
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategory $productCategory
     *
     * @return ProductCategoryRank
     */
    public function setProductCategory(\AMZcockpitDoctrine\Entity\MWS\ProductCategory $productCategory = null)
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * Get productCategory
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\ProductCategory
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }
}
