<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Feedback
 *
 * @ORM\Table(name="feedbacks")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\FeedbackRepository")
 */
class Feedback
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="amazon_order_id", type="string")
     */
    private $amazonOrderId;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @ORM\Column(name="comment_answer", type="text")
     */
    private $commentAnswer;

    /**
     * @ORM\Column(name="shipped_in_time", type="boolean", nullable=true)
     */
    private $shippedInTime;

    /**
     * @ORM\Column(name="product_as_described", type="boolean", nullable=true)
     */
    private $productAsDescribed;

    /**
     * @ORM\Column(name="client_service", type="string", length=255, nullable=true)
     */
    private $clientService;

    /**
     * @ORM\Column(name="buyer_email", type="string", length=255, nullable=true)
     */
    private $buyerEmail;

    /**
     * @ORM\Column(name="buyer_role", type="string", length=255, nullable=true)
     */
    private $buyerRole;

    /**
     * @ORM\Column(name="marketplace_Id", type="string", nullable=true)
     */
    private $marketplaceId;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Customer", inversedBy="feedback", cascade={"persist"})
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", inversedBy="feedbacks")
     */
    protected $store;

    public function __construct($data)
    {
        if(!is_null($data)) {
            $this->date                 = \DateTime::createFromFormat('d/m/y',$data[0]) ? \DateTime::createFromFormat('d/m/y',$data[0]) : \DateTime::createFromFormat('d.m.y',$data[0]);
            $this->rating               = $data[1];
            $this->comment              = $data[2];
            $this->commentAnswer        = $data[3];
            $this->shippedInTime        = in_array($data[4],["Oui","Sì","Yes"]) ? true :false;
            $this->productAsDescribed   = in_array($data[5],["Oui","Sì","Yes"]) ? true :false;
            $this->clientService        = $data[6];
            $this->amazonOrderId        = $data[7];
            $this->buyerEmail           = $data[8];
            $this->buyerRole            = isset($data[9]) ? $data[9] : null;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amazonOrderId
     *
     * @param string $amazonOrderId
     *
     * @return Feedback
     */
    public function setAmazonOrderId($amazonOrderId)
    {
        $this->amazonOrderId = $amazonOrderId;

        return $this;
    }

    /**
     * Get amazonOrderId
     *
     * @return string
     */
    public function getAmazonOrderId()
    {
        return $this->amazonOrderId;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Feedback
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Feedback
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set commentAnswer
     *
     * @param string $commentAnswer
     *
     * @return Feedback
     */
    public function setCommentAnswer($commentAnswer)
    {
        $this->commentAnswer = $commentAnswer;

        return $this;
    }

    /**
     * Get commentAnswer
     *
     * @return string
     */
    public function getCommentAnswer()
    {
        return $this->commentAnswer;
    }

    /**
     * Set shippedInTime
     *
     * @param boolean $shippedInTime
     *
     * @return Feedback
     */
    public function setShippedInTime($shippedInTime)
    {
        $this->shippedInTime = $shippedInTime;

        return $this;
    }

    /**
     * Get shippedInTime
     *
     * @return boolean
     */
    public function getShippedInTime()
    {
        return $this->shippedInTime;
    }

    /**
     * Set productAsDescribed
     *
     * @param boolean $productAsDescribed
     *
     * @return Feedback
     */
    public function setProductAsDescribed($productAsDescribed)
    {
        $this->productAsDescribed = $productAsDescribed;

        return $this;
    }

    /**
     * Get productAsDescribed
     *
     * @return boolean
     */
    public function getProductAsDescribed()
    {
        return $this->productAsDescribed;
    }

    /**
     * Set clientService
     *
     * @param string $clientService
     *
     * @return Feedback
     */
    public function setClientService($clientService)
    {
        $this->clientService = $clientService;

        return $this;
    }

    /**
     * Get clientService
     *
     * @return string
     */
    public function getClientService()
    {
        return $this->clientService;
    }

    /**
     * Set buyerEmail
     *
     * @param string $buyerEmail
     *
     * @return Feedback
     */
    public function setBuyerEmail($buyerEmail)
    {
        $this->buyerEmail = $buyerEmail;

        return $this;
    }

    /**
     * Get buyerEmail
     *
     * @return string
     */
    public function getBuyerEmail()
    {
        return $this->buyerEmail;
    }

    /**
     * Set buyerRole
     *
     * @param string $buyerRole
     *
     * @return Feedback
     */
    public function setBuyerRole($buyerRole)
    {
        $this->buyerRole = $buyerRole;

        return $this;
    }

    /**
     * Get buyerRole
     *
     * @return string
     */
    public function getBuyerRole()
    {
        return $this->buyerRole;
    }

    /**
     * Set marketplaceId
     *
     * @param string $marketplaceId
     *
     * @return Feedback
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return string
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return Feedback
     */
    public function setOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set customer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $customer
     *
     * @return Feedback
     */
    public function setCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Feedback
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set store
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store $store
     *
     * @return Feedback
     */
    public function setStore(\AMZcockpitDoctrine\Entity\MWS\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
