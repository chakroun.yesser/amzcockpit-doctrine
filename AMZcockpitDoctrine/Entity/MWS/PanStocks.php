<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="pan_europeen_stocks")
 */
class PanStocks
{
    const COUNTRY_LIST = [
        'Amazon.eu' => ['FR', 'UK', 'IT', 'DE', 'ES'],
        'Amazon.us' => ['CA', 'COM', 'MX'],
        'Amazon.jp' => ['JP'],
        'Amazon.au' => ['AU'],
        'Amazon.in' => ['IN']
    ];

    const COUNTRY_MARKETPLACE_LIST_NAMES = [
        'Amazon.co.uk' => 'UK',
        'Amazon.fr' => 'FR',
        'Amazon.de' => 'DE',
        'Amazon.es' => 'ES',
        'Amazon.it' => 'IT',
        'Amazon.ca' => 'CA',
        'Amazon.com' => 'COM',
        'Amazon.com.mx' => 'MX',
        'Amazon.co.jp' => 'JP',
        'Amazon.in' => 'IN',
        'Amazon.br' => 'BR',
        'Amazon.cb' => 'CX'
    ];

    const COUNTRY_MARKETPLACE_LIST = [
        'A1F83G8C2ARO7P' => 'UK',
        'A13V1IB3VIYZZH' => 'FR',
        'A1PA6795UKMFR9' => 'DE',
        'A1RKKUPIHCS9HS' => 'ES',
        'APJ6JRA9NG5V4' => 'IT',
        'A2EUQ1WTGCTBG2' => 'CA',
        'ATVPDKIKX0DER' => 'COM',
        'A1AM78C64UM0Y8' => 'MX',
        'A1VC38T7YXB528' => 'JP',
        'A39IBJ37TRP1C6' => 'AU',
        'A21TJRUUN4KGV' => 'IN'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Store", inversedBy="panStocks", cascade={"persist"})
     */
    protected $store;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", cascade={"persist"})
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="marketplace_id", type="string", nullable=true)
     */
    private $marketPlaceId;

    /**
     * @ORM\Column(name="marketplace_name", type="string", nullable=true)
     */
    private $marketplaceName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $countryCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tvaIntracom;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled = false;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tva;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tvaFBM;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SIREN;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyTradeRegister;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\CompanyStatutType")
     * @ORM\Column(name="statut", type="CompanyStatutType", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(name="adresse_complement", type="string", length=255, nullable=true)
     */
    private $adresseComplement;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(name="code_postal", type="string", length=10, nullable=true)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $pays;

    public function __construct($countryCode, $marketplaceName, $marketplaceId)
    {
        $this->enabled = false;
        $this->countryCode = $countryCode;
        $this->marketPlaceId = $marketplaceId;
        $this->marketplaceName = $marketplaceName;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tvaIntracom
     *
     * @param string $tvaIntracom
     *
     * @return PanStocks
     */
    public function setTvaIntracom($tvaIntracom)
    {
        $this->tvaIntracom = $tvaIntracom;

        return $this;
    }

    /**
     * Get tvaIntracom
     *
     * @return string
     */
    public function getTvaIntracom()
    {
        return $this->tvaIntracom;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return PanStocks
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return PanStocks
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set tva
     *
     * @param float $tva
     *
     * @return PanStocks
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return float
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set store.
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Store|null $store
     *
     * @return PanStocks
     */
    public function setStore(\AMZcockpitDoctrine\Entity\MWS\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Store|null
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set user.
     *
     * @param \AMZcockpitDoctrine\Entity\User|null $user
     *
     * @return PanStocks
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AMZcockpitDoctrine\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PanStocks
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set marketPlaceId
     *
     * @param string $marketPlaceId
     *
     * @return PanStocks
     */
    public function setMarketPlaceId($marketPlaceId)
    {
        $this->marketPlaceId = $marketPlaceId;

        return $this;
    }

    /**
     * Get marketPlaceId
     *
     * @return string
     */
    public function getMarketPlaceId()
    {
        return $this->marketPlaceId;
    }

    /**
     * Set tvaFBM
     *
     * @param float $tvaFBM
     *
     * @return PanStocks
     */
    public function setTvaFBM($tvaFBM)
    {
        $this->tvaFBM = $tvaFBM;

        return $this;
    }

    /**
     * Get tvaFBM
     *
     * @return float
     */
    public function getTvaFBM()
    {
        return $this->tvaFBM;
    }

    /**
     * Set sIREN
     *
     * @param string $sIREN
     *
     * @return PanStocks
     */
    public function setSIREN($sIREN)
    {
        $this->SIREN = $sIREN;

        return $this;
    }

    /**
     * Get sIREN
     *
     * @return string
     */
    public function getSIREN()
    {
        return $this->SIREN;
    }

    /**
     * Set statut
     *
     * @param CompanyStatutType $statut
     *
     * @return PanStocks
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return CompanyStatutType
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set marketplaceName
     *
     * @param string $marketplaceName
     *
     * @return PanStocks
     */
    public function setMarketplaceName($marketplaceName)
    {
        $this->marketplaceName = $marketplaceName;

        return $this;
    }

    /**
     * Get marketplaceName
     *
     * @return string
     */
    public function getMarketplaceName()
    {
        return $this->marketplaceName;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return PanStocks
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return PanStocks
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return PanStocks
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return PanStocks
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set adresseComplement
     *
     * @param string $adresseComplement
     *
     * @return PanStocks
     */
    public function setAdresseComplement($adresseComplement)
    {
        $this->adresseComplement = $adresseComplement;

        return $this;
    }

    /**
     * Get adresseComplement
     *
     * @return string
     */
    public function getAdresseComplement()
    {
        return $this->adresseComplement;
    }

    /**
     * Set companyTradeRegister
     *
     * @param string $companyTradeRegister
     *
     * @return PanStocks
     */
    public function setCompanyTradeRegister($companyTradeRegister)
    {
        $this->companyTradeRegister = $companyTradeRegister;

        return $this;
    }

    /**
     * Get companyTradeRegister
     *
     * @return string
     */
    public function getCompanyTradeRegister()
    {
        return $this->companyTradeRegister;
    }
}
