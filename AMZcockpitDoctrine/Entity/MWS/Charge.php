<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Fee
 *
 * @ORM\Table(name="charges")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\ChargeRepository")
 */
class Charge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="charge_type", type="string", length=128)
     */
    private $type;

    /**
     * @ORM\Column(name="currency", type="string", length=6)
     */
    private $currencyCode;

    /**
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", inversedBy="charges", cascade={"persist"})
     */
    protected $order;

    public function __construct($data)
    {
        if(!is_null($data)) {
            $this->type               = $data["ChargeType"];
            $this->amount              = $data["Amount"];
            $this->currencyCode        = $data["CurrencyCode"];
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Charge
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Charge
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Charge
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return Charge
     */
    public function setOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }
}
