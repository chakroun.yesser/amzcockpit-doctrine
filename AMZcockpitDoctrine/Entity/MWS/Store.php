<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use AMZcockpitDoctrine\Entity\Jobs;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Store
 *
 * @ORM\Table(name="stores")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\StoreRepository")
 * @UniqueEntity(
 *     fields={"name", "user"},
 *     errorPath="name",
 *     message="Cette valeur est déjà utiliser par un autre store."
 * )
 * @UniqueEntity(
 *     fields={"token","sellerId"},
 *     errorPath="sellerId",
 *     message="Cette valeur est déjà utiliser par un autre client."
 * )
 */
class Store
{

    const PENDING    = 'pending';
    const CANCELED   = 'canceled';
    const VALIDATED  = 'validated';
    const DEFAULT_MARKETPLACE = 'A13V1IB3VIYZZH';

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255)
     */
    private $slug;

    /**
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank(message="Ce champs est obligatoire.")
     */
    private $name;

    /**
     * @ORM\Column(name="market_place", type="string")
     * @Assert\NotBlank(message="Ce champs est obligatoire.")
     */
    private $marketPlace;

    /**
     * @ORM\Column(name="default_market_place", type="string", nullable=true)
     */
    private $defaultMarketPlace;

    /**
     * @ORM\Column(name="seller_id", type="string")
     * @Assert\NotBlank(message="Ce champs est obligatoire.")
     */
    private $sellerId;

    /**
     * @ORM\Column(name="token", type="string")
     * @Assert\NotBlank(message="Ce champs est obligatoire.")
     */
    private $token;

    /**
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(name="sender_name", type="string", nullable=true)
     */
    private $senderName;

    /**
     * @var boolean
     * @ORM\Column(name="email_verified", type="boolean")
     */
    private $emailVerified = false;

    /**
     * @var boolean
     * @ORM\Column(name="validated", type="boolean")
     */
    private $validated = false;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = false;

    /**
     * @var boolean
     * @ORM\Column(name="problem_is_notified", type="boolean")
     */
    private $problemNotified = false;

    /**
     * @var boolean
     * @ORM\Column(name="status", type="string")
     */
    private $status = self::PENDING;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="stores")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", mappedBy="store", cascade={"persist","remove"}, fetch="EXTRA_LAZY")
     */
    protected $orders;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Feedback", mappedBy="store", cascade={"persist","remove"})
     */
    protected $feedbacks;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", mappedBy="store", cascade={"persist","remove"})
     */
    protected $products;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\ZipRequest", mappedBy="store", cascade={"persist","remove"})
     */
    protected $zipRequests;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File",cascade={"persist"})
     */
    protected $logo;

    /**
     * @var boolean
     * @ORM\Column(name="email_sending", type="boolean")
     */
    private $emailSending = true;

    /**
     * @var boolean
     * @ORM\Column(name="notificate_feedbacks", type="boolean")
     */
    private $notificationFeedback = false;

    /**
     * @var boolean
     * @ORM\Column(name="notificate_reviews", type="boolean")
     */
    private $notificationReviews = true;

    /**
     * @ORM\Column(name="ImportDateEnd", type="datetime", nullable=true)
     */
    private $importDateEnd;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $nomEntreprise;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\CompanyStatutType")
     * @ORM\Column(name="statut", type="CompanyStatutType", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mentionTva;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mentionTvaMarge;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $mentionEscompte = true;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $checkTva;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tvaIntracom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SIREN;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SIRET;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $RCS;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $capitalSocial;

    /**
     * @ORM\Column(type="string", options={"default" : "EUR"}, nullable=true)
     */
    private $capitalSocialCurrency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tva;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 20})
     */
    private $tvaShippingFBA;

    /**
     * @ORM\Column(type="string", nullable=true, length=10)
     */
    private $paysStockageFBA;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tvaFBM;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tvaShipping;

    /**
     * @ORM\Column(type="string", nullable=true, length=10)
     */
    private $paysStockageFBM;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $panActive = false;

    /**
     * @ORM\OneToMany(targetEntity="PanStocks", mappedBy="store", cascade={"persist","remove"})
     */
    protected $panStocks;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $applyAllProducts = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(name="adresse_complement", type="string", length=255, nullable=true)
     */
    private $adresseComplement;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(name="code_postal", type="string", length=10, nullable=true)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", nullable=true, length=70)
     */
    private $pays;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $scrappingReviewRequestId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $scrappingReviewRequestStatut;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 0})
     */
    private $hasCustomMention = false;

    /**
     * @ORM\Column(type="text", nullable=true, length=250)
     */
    private $customMentionsText;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 0})
     */
    private $hasNote = false;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\StoreNotePositionType")
     * @ORM\Column(name="note_position", type="StoreNotePositionType", length=255, nullable=true)
     */
    private $notePosition;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $noteEmail;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $notePhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $noteFax;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $noteUrls;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $noteServices;

    /**
     * @ORM\OneToOne(targetEntity="AMZcockpitDoctrine\Entity\File", cascade={"persist", "remove"})
     */
    protected $noteLogo;

    /**
     * @ORM\Column(name="last_product_import_date", type="datetime", nullable=true)
     */
    private $lastProductImportDate;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\Jobs", mappedBy="store", cascade={"persist"})
     */
    protected $jobs;

    public function __construct()
    {
        $this->logo = null;
        $this->notificationReviews = true;
        $this->applyAllProducts = true;
        $this->mentionTvaMarge = false;
        $this->mentionEscompte = true;
        $this->hasNote = false;
        $this->tvaShipping = 20;
        $this->noteUrls = new ArrayCollection();
        $this->noteServices = new ArrayCollection();
        return $this->status = self::PENDING;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Store
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set marketPlace
     *
     * @param string $marketPlace
     *
     * @return Store
     */
    public function setMarketPlace($marketPlace)
    {
        $this->marketPlace = $marketPlace;

        return $this;
    }

    /**
     * Get marketPlace
     *
     * @return string
     */
    public function getMarketPlace()
    {
        return $this->marketPlace;
    }

    /**
     * Set sellerId
     *
     * @param string $sellerId
     *
     * @return Store
     */
    public function setSellerId($sellerId)
    {
        $this->sellerId = $sellerId;

        return $this;
    }

    /**
     * Get sellerId
     *
     * @return string
     */
    public function getSellerId()
    {
        return $this->sellerId;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Store
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set emailVerified
     *
     * @param boolean $emailVerified
     *
     * @return Store
     */
    public function setEmailVerified($emailVerified)
    {
        $this->emailVerified = $emailVerified;

        return $this;
    }

    /**
     * Get emailVerified
     *
     * @return boolean
     */
    public function getEmailVerified()
    {
        return $this->emailVerified;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Store
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return Store
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Store
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Store
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Add order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return Store
     */
    public function addOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order)
    {
        $order->setStore($this);
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     */
    public function removeOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return Store
     */
    public function addProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $product->setStore($this);
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     */
    public function removeProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add feedback
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Feedback $feedback
     *
     * @return Store
     */
    public function addFeedback(\AMZcockpitDoctrine\Entity\MWS\Feedback $feedback)
    {
        $this->feedbacks[] = $feedback;

        return $this;
    }

    /**
     * Remove feedback
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Feedback $feedback
     */
    public function removeFeedback(\AMZcockpitDoctrine\Entity\MWS\Feedback $feedback)
    {
        $this->feedbacks->removeElement($feedback);
    }

    /**
     * Get feedbacks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeedbacks()
    {
        return $this->feedbacks;
    }

    /**
     * Set defaultMarketPlace
     *
     * @param string $defaultMarketPlace
     *
     * @return Store
     */
    public function setDefaultMarketPlace($defaultMarketPlace)
    {
        $this->defaultMarketPlace = $defaultMarketPlace;

        return $this;
    }

    /**
     * Get defaultMarketPlace
     *
     * @return string
     */
    public function getDefaultMarketPlace()
    {
        return $this->defaultMarketPlace;
    }

    /**
     * Set emailSending
     *
     * @param boolean $emailSending
     *
     * @return Store
     */
    public function setEmailSending($emailSending)
    {
        $this->emailSending = $emailSending;

        return $this;
    }

    /**
     * Get emailSending
     *
     * @return boolean
     */
    public function getEmailSending()
    {
        return $this->emailSending;
    }

    /**
     * Set notificationFeedback
     *
     * @param boolean $notificationFeedback
     *
     * @return Store
     */
    public function setNotificationFeedback($notificationFeedback)
    {
        $this->notificationFeedback = $notificationFeedback;

        return $this;
    }

    /**
     * Get notificationFeedback
     *
     * @return boolean
     */
    public function getNotificationFeedback()
    {
        return $this->notificationFeedback;
    }

    /**
     * Set notificationReviews
     *
     * @param boolean $notificationReviews
     *
     * @return Store
     */
    public function setNotificationReviews($notificationReviews)
    {
        $this->notificationReviews = $notificationReviews;

        return $this;
    }

    /**
     * Get notificationReviews
     *
     * @return boolean
     */
    public function getNotificationReviews()
    {
        return $this->notificationReviews;
    }

    /**
     * Set logo
     *
     * @param \AMZcockpitDoctrine\Entity\File $logo
     *
     * @return Store
     */
    public function setLogo(\AMZcockpitDoctrine\Entity\File $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set importDateEnd
     *
     * @param \DateTime $importDateEnd
     *
     * @return Store
     */
    public function setImportDateEnd($importDateEnd)
    {
        $this->importDateEnd = $importDateEnd;

        return $this;
    }

    /**
     * Get importDateEnd
     *
     * @return \DateTime
     */
    public function getImportDateEnd()
    {
        return $this->importDateEnd;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Store
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     *
     * @return Store
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;

        return $this;
    }

    /**
     * Get senderName
     *
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }


    /**
     * Add zipRequest
     *
     * @param \AMZcockpitDoctrine\Entity\ZipRequest $zipRequest
     *
     * @return Store
     */
    public function addZipRequest(\AMZcockpitDoctrine\Entity\ZipRequest $zipRequest)
    {
        $this->zipRequests[] = $zipRequest;

        return $this;
    }

    /**
     * Remove zipRequest
     *
     * @param \AMZcockpitDoctrine\Entity\ZipRequest $zipRequest
     */
    public function removeZipRequest(\AMZcockpitDoctrine\Entity\ZipRequest $zipRequest)
    {
        $this->zipRequests->removeElement($zipRequest);
    }

    /**
     * Get zipRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZipRequests()
    {
        return $this->zipRequests;
    }

    /**
     * Set problemNotified
     *
     * @param boolean $problemNotified
     *
     * @return Store
     */
    public function setProblemNotified($problemNotified)
    {
        $this->problemNotified = $problemNotified;

        return $this;
    }

    /**
     * Get problemNotified
     *
     * @return boolean
     */
    public function getProblemNotified()
    {
        return $this->problemNotified;
    }

    public function getPanStockByCountryCode($countryCode)
    {
        /** @var \AMZcockpitDoctrine\Entity\MWS\PanStocks $panEuropeenStock */
        foreach ($this->getPanStocks() as $panStock) {
            if($panStock->getEnabled() && $panStock->getCountryCode() == $countryCode && $panStock->getTva()) {
                return $panStock;
            }
        }
        return null;
    }


    /**
     * Set nomEntreprise.
     *
     * @param string|null $nomEntreprise
     *
     * @return Store
     */
    public function setNomEntreprise($nomEntreprise = null)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * Get nomEntreprise.
     *
     * @return string|null
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * Set statut.
     *
     * @param string|null $statut
     *
     * @return Store
     */
    public function setStatut($statut = null)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut.
     *
     * @return string|null
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set mentionTva.
     *
     * @param bool|null $mentionTva
     *
     * @return Store
     */
    public function setMentionTva($mentionTva = null)
    {
        $this->mentionTva = $mentionTva;

        return $this;
    }

    /**
     * Get mentionTva.
     *
     * @return bool|null
     */
    public function getMentionTva()
    {
        return $this->mentionTva;
    }

    /**
     * Set checkTva.
     *
     * @param bool|null $checkTva
     *
     * @return Store
     */
    public function setCheckTva($checkTva = null)
    {
        $this->checkTva = $checkTva;

        return $this;
    }

    /**
     * Get checkTva.
     *
     * @return bool|null
     */
    public function getCheckTva()
    {
        return $this->checkTva;
    }

    /**
     * Set tvaIntracom.
     *
     * @param string|null $tvaIntracom
     *
     * @return Store
     */
    public function setTvaIntracom($tvaIntracom = null)
    {
        $this->tvaIntracom = $tvaIntracom;

        return $this;
    }

    /**
     * Get tvaIntracom.
     *
     * @return string|null
     */
    public function getTvaIntracom()
    {
        return $this->tvaIntracom;
    }

    /**
     * Set sIREN.
     *
     * @param string|null $sIREN
     *
     * @return Store
     */
    public function setSIREN($sIREN = null)
    {
        $this->SIREN = $sIREN;

        return $this;
    }

    /**
     * Get sIREN.
     *
     * @return string|null
     */
    public function getSIREN()
    {
        return $this->SIREN;
    }

    /**
     * Set sIRET.
     *
     * @param string|null $sIRET
     *
     * @return Store
     */
    public function setSIRET($sIRET = null)
    {
        $this->SIRET = $sIRET;

        return $this;
    }

    /**
     * Get sIRET.
     *
     * @return string|null
     */
    public function getSIRET()
    {
        return $this->SIRET;
    }

    /**
     * Set rCS.
     *
     * @param string|null $rCS
     *
     * @return Store
     */
    public function setRCS($rCS = null)
    {
        $this->RCS = $rCS;

        return $this;
    }

    /**
     * Get rCS.
     *
     * @return string|null
     */
    public function getRCS()
    {
        return $this->RCS;
    }

    /**
     * Set capitalSocial.
     *
     * @param float|null $capitalSocial
     *
     * @return Store
     */
    public function setCapitalSocial($capitalSocial = null)
    {
        $this->capitalSocial = $capitalSocial;

        return $this;
    }

    /**
     * Get capitalSocial.
     *
     * @return float|null
     */
    public function getCapitalSocial()
    {
        return $this->capitalSocial;
    }

    /**
     * Set tva.
     *
     * @param float|null $tva
     *
     * @return Store
     */
    public function setTva($tva = null)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva.
     *
     * @return float|null
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set paysStockageFBA.
     *
     * @param string|null $paysStockageFBA
     *
     * @return Store
     */
    public function setPaysStockageFBA($paysStockageFBA = null)
    {
        $this->paysStockageFBA = $paysStockageFBA;

        return $this;
    }

    /**
     * Get paysStockageFBA.
     *
     * @return string|null
     */
    public function getPaysStockageFBA()
    {
        return $this->paysStockageFBA;
    }

    /**
     * Set tvaFBM.
     *
     * @param float|null $tvaFBM
     *
     * @return Store
     */
    public function setTvaFBM($tvaFBM = null)
    {
        $this->tvaFBM = $tvaFBM;

        return $this;
    }

    /**
     * Get tvaFBM.
     *
     * @return float|null
     */
    public function getTvaFBM()
    {
        return $this->tvaFBM;
    }

    /**
     * Set paysStockageFBM.
     *
     * @param string|null $paysStockageFBM
     *
     * @return Store
     */
    public function setPaysStockageFBM($paysStockageFBM = null)
    {
        $this->paysStockageFBM = $paysStockageFBM;

        return $this;
    }

    /**
     * Get paysStockageFBM.
     *
     * @return string|null
     */
    public function getPaysStockageFBM()
    {
        return $this->paysStockageFBM;
    }

    /**
     * Set panActive.
     *
     * @param bool|null $panActive
     *
     * @return Store
     */
    public function setPanActive($panActive = null)
    {
        $this->panActive = $panActive;

        return $this;
    }

    /**
     * Get panActive.
     *
     * @return bool|null
     */
    public function getPanActive()
    {
        return $this->panActive;
    }

    /**
     * Set applyAllProducts.
     *
     * @param bool|null $applyAllProducts
     *
     * @return Store
     */
    public function setApplyAllProducts($applyAllProducts = null)
    {
        $this->applyAllProducts = $applyAllProducts;

        return $this;
    }

    /**
     * Get applyAllProducts.
     *
     * @return bool|null
     */
    public function getApplyAllProducts()
    {
        return $this->applyAllProducts;
    }

    /**
     * Add panStock.
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\PanStocks $panStock
     *
     * @return Store
     */
    public function addPanStock(\AMZcockpitDoctrine\Entity\MWS\PanStocks $panStock)
    {
        $panStock->setStore($this);
        $this->panStocks[] = $panStock;

        return $this;
    }

    /**
     * Remove panStock.
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\PanStocks $panStock
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePanStock(\AMZcockpitDoctrine\Entity\MWS\PanStocks $panStock)
    {
        return $this->panStocks->removeElement($panStock);
    }

    /**
     * Get panStocks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPanStocks()
    {
        return $this->panStocks;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Store
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Store
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Store
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Store
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Store
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set adresseComplement
     *
     * @param string $adresseComplement
     *
     * @return Store
     */
    public function setAdresseComplement($adresseComplement)
    {
        $this->adresseComplement = $adresseComplement;

        return $this;
    }

    /**
     * Get adresseComplement
     *
     * @return string
     */
    public function getAdresseComplement()
    {
        return $this->adresseComplement;
    }

    /**
     * Set scrappingReviewRequestId
     *
     * @param string $scrappingReviewRequestId
     *
     * @return Store
     */
    public function setScrappingReviewRequestId($scrappingReviewRequestId)
    {
        $this->scrappingReviewRequestId = $scrappingReviewRequestId;

        return $this;
    }

    /**
     * Get scrappingReviewRequestId
     *
     * @return string
     */
    public function getScrappingReviewRequestId()
    {
        return $this->scrappingReviewRequestId;
    }

    /**
     * Set scrappingReviewRequestStatut
     *
     * @param string $scrappingReviewRequestStatut
     *
     * @return Store
     */
    public function setScrappingReviewRequestStatut($scrappingReviewRequestStatut)
    {
        $this->scrappingReviewRequestStatut = $scrappingReviewRequestStatut;

        return $this;
    }

    /**
     * Get scrappingReviewRequestStatut
     *
     * @return string
     */
    public function getScrappingReviewRequestStatut()
    {
        return $this->scrappingReviewRequestStatut;
    }

    /**
     * Set tvaShipping
     *
     * @param float $tvaShipping
     *
     * @return Store
     */
    public function setTvaShipping($tvaShipping)
    {
        $this->tvaShipping = $tvaShipping;

        return $this;
    }

    /**
     * Get tvaShipping
     *
     * @return float
     */
    public function getTvaShipping()
    {
        return $this->tvaShipping;
    }

    /**
     * Set mentionTvaMarge
     *
     * @param boolean $mentionTvaMarge
     *
     * @return Store
     */
    public function setMentionTvaMarge($mentionTvaMarge)
    {
        $this->mentionTvaMarge = $mentionTvaMarge;

        return $this;
    }

    /**
     * Get mentionTvaMarge
     *
     * @return boolean
     */
    public function getMentionTvaMarge()
    {
        return $this->mentionTvaMarge;
    }

    /**
     * Set hasNote
     *
     * @param boolean $hasNote
     *
     * @return Store
     */
    public function setHasNote($hasNote)
    {
        $this->hasNote = $hasNote;

        return $this;
    }

    /**
     * Get hasNote
     *
     * @return boolean
     */
    public function getHasNote()
    {
        return $this->hasNote;
    }

    /**
     * Set notePosition
     *
     * @param string $notePosition
     *
     * @return Store
     */
    public function setNotePosition($notePosition)
    {
        $this->notePosition = $notePosition;

        return $this;
    }

    /**
     * Get notePosition
     *
     * @return string
     */
    public function getNotePosition()
    {
        return $this->notePosition;
    }

    /**
     * Set noteEmail
     *
     * @param string $noteEmail
     *
     * @return Store
     */
    public function setNoteEmail($noteEmail)
    {
        $this->noteEmail = $noteEmail;

        return $this;
    }

    /**
     * Get noteEmail
     *
     * @return string
     */
    public function getNoteEmail()
    {
        return $this->noteEmail;
    }

    /**
     * Set notePhone
     *
     * @param string $notePhone
     *
     * @return Store
     */
    public function setNotePhone($notePhone)
    {
        $this->notePhone = $notePhone;

        return $this;
    }

    /**
     * Get notePhone
     *
     * @return string
     */
    public function getNotePhone()
    {
        return $this->notePhone;
    }

    /**
     * Set noteFax
     *
     * @param string $noteFax
     *
     * @return Store
     */
    public function setNoteFax($noteFax)
    {
        $this->noteFax = $noteFax;

        return $this;
    }

    /**
     * Get noteFax
     *
     * @return string
     */
    public function getNoteFax()
    {
        return $this->noteFax;
    }

    /**
     * Set noteUrls
     *
     * @param array $noteUrls
     *
     * @return Store
     */
    public function setNoteUrls($noteUrls)
    {
        $this->noteUrls = $noteUrls;

        return $this;
    }

    /**
     * Get noteUrls
     *
     * @return array
     */
    public function getNoteUrls()
    {
        return $this->noteUrls;
    }

    /**
     * Set noteServices
     *
     * @param array $noteServices
     *
     * @return Store
     */
    public function setNoteServices($noteServices)
    {
        $this->noteServices = $noteServices;

        return $this;
    }

    /**
     * Get noteServices
     *
     * @return array
     */
    public function getNoteServices()
    {
        return $this->noteServices;
    }

    /**
     * Set noteLogo
     *
     * @param \AMZcockpitDoctrine\Entity\File $noteLogo
     *
     * @return Store
     */
    public function setNoteLogo(\AMZcockpitDoctrine\Entity\File $noteLogo = null)
    {
        $this->noteLogo = $noteLogo;

        return $this;
    }

    /**
     * Get noteLogo
     *
     * @return \AMZcockpitDoctrine\Entity\File
     */
    public function getNoteLogo()
    {
        return $this->noteLogo;
    }

    /**
     * Set hasCustomMention
     *
     * @param boolean $hasCustomMention
     *
     * @return Store
     */
    public function setHasCustomMention($hasCustomMention)
    {
        $this->hasCustomMention = $hasCustomMention;

        return $this;
    }

    /**
     * Get hasCustomMention
     *
     * @return boolean
     */
    public function getHasCustomMention()
    {
        return $this->hasCustomMention;
    }

    /**
     * Set customMentionsText
     *
     * @param string $customMentionsText
     *
     * @return Store
     */
    public function setCustomMentionsText($customMentionsText)
    {
        $this->customMentionsText = $customMentionsText;

        return $this;
    }

    /**
     * Get customMentionsText
     *
     * @return string
     */
    public function getCustomMentionsText()
    {
        return $this->customMentionsText;
    }

    /**
     * Set mentionEscompte
     *
     * @param boolean $mentionEscompte
     *
     * @return Store
     */
    public function setMentionEscompte($mentionEscompte)
    {
        $this->mentionEscompte = $mentionEscompte;

        return $this;
    }

    /**
     * Get mentionEscompte
     *
     * @return boolean
     */
    public function getMentionEscompte()
    {
        return $this->mentionEscompte;
    }

    /**
     * Set tvaShippingFBA
     *
     * @param float $tvaShippingFBA
     *
     * @return Store
     */
    public function setTvaShippingFBA($tvaShippingFBA)
    {
        $this->tvaShippingFBA = $tvaShippingFBA;

        return $this;
    }

    /**
     * Get tvaShippingFBA
     *
     * @return float
     */
    public function getTvaShippingFBA()
    {
        return $this->tvaShippingFBA;
    }

    /**
     * Set capitalSocialCurrency
     *
     * @param string $capitalSocialCurrency
     *
     * @return Store
     */
    public function setCapitalSocialCurrency($capitalSocialCurrency)
    {
        $this->capitalSocialCurrency = $capitalSocialCurrency;

        return $this;
    }

    /**
     * Get capitalSocialCurrency
     *
     * @return string
     */
    public function getCapitalSocialCurrency()
    {
        return $this->capitalSocialCurrency;
    }

    /**
     * Set lastProductImportDate.
     *
     * @param \DateTime|null $lastProductImportDate
     *
     * @return Store
     */
    public function setLastProductImportDate($lastProductImportDate = null)
    {
        $this->lastProductImportDate = $lastProductImportDate;

        return $this;
    }

    /**
     * Get lastProductImportDate.
     *
     * @return \DateTime|null
     */
    public function getLastProductImportDate()
    {
        return $this->lastProductImportDate;
    }

    /**
     * Add job.
     *
     * @param \AMZcockpitDoctrine\Entity\Jobs $job
     *
     * @return Store
     */
    public function addJob(\AMZcockpitDoctrine\Entity\Jobs $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job.
     *
     * @param \AMZcockpitDoctrine\Entity\Jobs $job
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeJob(\AMZcockpitDoctrine\Entity\Jobs $job)
    {
        return $this->jobs->removeElement($job);
    }

    /**
     * Get jobs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    public function getJobByObject($object)
    {
        if(count($this->getJobs())) {
            /** @var Jobs $job */
            foreach ($this->getJobs() as $job) {
                if($job->getObject() === $object) {
                    return $job;
                }
            }
        }
        return null;
    }

    public function getActiveJobByObject($object)
    {
        if(count($this->getJobs())) {
            /** @var Jobs $job */
            foreach ($this->getJobs() as $job) {
                if($job->getObject() === $object and !in_array($job->getStatus(), [Jobs::COMPLETE, Jobs::ERROR])) {
                    return $job;
                }
            }
        }
        return null;
    }
}
