<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Review
 *
 * @ORM\Table(name="products_reviews", options={"collate"="utf8mb4_general_ci", "charset":"utf8mb4"})
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\ReviewRepository")
 * @UniqueEntity("idAmazon")
 */
class Review
{

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="id_amazon", type="text", nullable=false)
     */
    private $idAmazon;

    /**
     * @ORM\Column(name="buyerName", type="text", nullable=true)
     */
    private $buyerName;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(name="rating", type="float", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(name="text", type="text", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $text;

    /**
     * @ORM\Column(name="title", type="string", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $title;

    /**
     * @ORM\Column(name="marketplace_id", type="string", options={"collation":"utf8mb4_general_ci"})
     */
    private $marketplaceId;

    /**
     * @ORM\Column(name="ASIN", type="string", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $ASIN;

    /**
     * @ORM\Column(name="buyerId", type="string", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $buyerId;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="reviews")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Customer", inversedBy="reviews")
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", inversedBy="reviews")
     */
    protected $order;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAmazon
     *
     * @param string $idAmazon
     *
     * @return Review
     */
    public function setIdAmazon($idAmazon)
    {
        $this->idAmazon = $idAmazon;

        return $this;
    }

    /**
     * Get idAmazon
     *
     * @return string
     */
    public function getIdAmazon()
    {
        return $this->idAmazon;
    }

    /**
     * Set buyerName
     *
     * @param string $buyerName
     *
     * @return Review
     */
    public function setBuyerName($buyerName)
    {
        $this->buyerName = $buyerName;

        return $this;
    }

    /**
     * Get buyerName
     *
     * @return string
     */
    public function getBuyerName()
    {
        return $this->buyerName;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Review
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set rating
     *
     * @param float $rating
     *
     * @return Review
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Review
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Review
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set marketplaceId
     *
     * @param string $marketplaceId
     *
     * @return Review
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return string
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return Review
     */
    public function setProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set customer
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Customer $customer
     *
     * @return Review
     */
    public function setCustomer(\AMZcockpitDoctrine\Entity\MWS\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return Review
     */
    public function setOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set aSIN
     *
     * @param string $aSIN
     *
     * @return Review
     */
    public function setASIN($aSIN)
    {
        $this->ASIN = $aSIN;

        return $this;
    }

    /**
     * Get aSIN
     *
     * @return string
     */
    public function getASIN()
    {
        return $this->ASIN;
    }

    /**
     * Set buyerId
     *
     * @param string $buyerId
     *
     * @return Review
     */
    public function setBuyerId($buyerId)
    {
        $this->buyerId = $buyerId;

        return $this;
    }

    /**
     * Get buyerId
     *
     * @return string
     */
    public function getBuyerId()
    {
        return $this->buyerId;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Review
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Review
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
