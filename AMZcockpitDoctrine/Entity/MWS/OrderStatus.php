<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Order Status
 *
 * @ORM\Table(name="orders_status")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\OrderStatusRepository")
 */
class OrderStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", inversedBy="status")
     */
    protected $order;

    /**
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="is_negatif", type="boolean")
     */
    private $isNegatif;

    public function __construct($status, $date, $isNegatif = false)
    {
        $this->date      = $date instanceof \DateTime ? $date : new \DateTime($date);
        $this->status    = $status;
        $this->isNegatif = $isNegatif;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return OrderStatus
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return OrderStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return OrderStatus
     */
    public function setOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set isNegatif
     *
     * @param boolean $isNegatif
     *
     * @return OrderStatus
     */
    public function setIsNegatif($isNegatif)
    {
        $this->isNegatif = $isNegatif;

        return $this;
    }

    /**
     * Get isNegatif
     *
     * @return boolean
     */
    public function getIsNegatif()
    {
        return $this->isNegatif;
    }
}
