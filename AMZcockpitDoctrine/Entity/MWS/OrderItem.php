<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * OrderItems
 *
 * @ORM\Table(name="order_items")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\OrderItemRepository")
 * @UniqueEntity("amazonOrderId")
 */
class OrderItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="order_item_id", type="string", nullable=true)
     */
    private $orderItemId;

    /**
     * @ORM\Column(name="ASIN", type="string", nullable=true)
     */
    private $ASIN;

    /**
     * @ORM\Column(name="seller_SKU", type="string")
     */
    private $sellerSKU;

    /**
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="quantity_ordered", type="integer", length=10, nullable=true)
     */
    private $quantityOrdered;

    /**
     * @ORM\Column(name="quantity_shipped", type="integer", length=10, nullable=true)
     */
    private $quantityShipped;

    /**
     * @ORM\Column(name="currency", type="string", length=6, nullable=true)
     */
    private $currencyCode;

    /**
     * @ORM\Column(name="item_price_amount", type="float", length=50, nullable=true)
     */
    private $itemPriceAmount;

    /**
     * @ORM\Column(name="shipping_price_amount", type="float", length=50, nullable=true)
     */
    private $shippingPriceAmount;

    /**
     * @ORM\Column(name="item_tax_price_amount", type="float", length=50, nullable=true)
     */
    private $itemTaxPriceAmount;

    /**
     * @ORM\Column(name="shipping_tax_amount", type="float", length=50, nullable=true)
     */
    private $shippingTaxAmount;

    /**
     * @ORM\Column(name="shipping_discount_amount", type="float", length=50, nullable=true)
     */
    private $shippingDiscount;

    /**
     * @ORM\Column(name="promotion_discount_amount", type="float", length=50, nullable=true)
     */
    private $promotionDiscount;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="ordersItem")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Orders", inversedBy="items", cascade={"persist","remove"})
     */
    protected $order;


    ##FULFILLMENT
    /**
     * @ORM\Column(name="SellerFulfillmentOrderItemId", type="string", length=50, nullable=true)
     */
    private $sellerFulfillmentOrderItemId;

    /**
     * @ORM\Column(name="FulfillmentNetworkSKU", type="string", length=50, nullable=true)
     */
    private $fulfillmentNetworkSKU;

    /**
     * @ORM\Column(name="EstimatedShipDateTime", type="datetime", nullable=true)
     */
    private $estimatedShipDateTime;

    /**
     * @ORM\Column(name="EstimatedArrivalDateTime", type="datetime", nullable=true)
     */
    private $EstimatedArrivalDateTime;

    /**
     * @ORM\Column(name="shipmentId", type="string", nullable=true)
     */
    private $shipmentId;

    /**
     * @ORM\Column(name="shipmentDate", type="datetime", nullable=true)
     */
    private $shipmentDate;

    /**
     * @ORM\Column(name="gift_wrap_price", type="float", nullable=true)
     */
    private $giftWrapPrice;

    /**
     * @ORM\Column(name="gift_wrap_tax", type="float", nullable=true)
     */
    private $giftWrapTax;

    /**
     * @ORM\Column(name="carrier", type="string", nullable=true)
     */
    private $carrier;

    /**
     * @ORM\Column(name="tracking_number", type="string", nullable=true)
     */
    private $trackingNumber;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\MWS\Fee", mappedBy="orderItem", cascade={"persist","remove"})
     */
    protected $fees;

    /**
     * @ORM\Column(name="fees_data", type="json_array", nullable=true)
     */
    private $feesData;

    /**
     * @ORM\Column(name="is_refunded", type="boolean", options={"default" : 0})
     */
    private $isRefunded = false;

    public function __construct($data = null)
    {
        if(!is_null($data)) {
            $this->ASIN                 = $data['ASIN'];
            $this->sellerSKU            = $data['SellerSKU'];
            $this->orderItemId          = $data['OrderItemId'];
            $this->title                = $data['Title'];
            $this->quantityOrdered      = $data['QuantityOrdered'];
            $this->quantityShipped      = $data['QuantityShipped'];
            $this->currencyCode         = isset($data['ItemPrice']) ? $data['ItemPrice']['CurrencyCode'] : null;
            $this->itemPriceAmount      = isset($data['ItemPrice']) ? $data['ItemPrice']['Amount'] : null;
            $this->shippingPriceAmount  = isset($data['ShippingPrice']) ? $data['ShippingPrice']['Amount'] : null;
            $this->itemTaxPriceAmount   = isset($data['ItemTax']) ? $data['ItemTax']['Amount'] : null;
            $this->shippingTaxAmount    = isset($data['ShippingTax']) ? $data['ShippingTax']['Amount'] : null;
            $this->shippingDiscount     = isset($data['ShippingDiscount']) ? $data['ShippingDiscount']['Amount'] : null;
            $this->promotionDiscount    = isset($data['PromotionDiscount']) ? $data['PromotionDiscount']['Amount'] : null;
        }
    }

    public function createFromFulfillmentData($data = null)
    {
        if(!is_null($data)) {
            $this->sellerSKU            = $data['SellerSKU'];
            $this->orderItemId          = $data['SellerFulfillmentOrderItemId'];
            $this->fulfillmentNetworkSKU = $data['FulfillmentNetworkSKU'];
            $this->quantityOrdered      = $data['Quantity'];
            $this->quantityShipped      = $data['Quantity'];
            $this->currencyCode         = isset($data['PerUnitDeclaredValue']) ? $data['PerUnitDeclaredValue']['CurrencyCode'] : null;
            $this->itemPriceAmount      = isset($data['PerUnitDeclaredValue']) ? $data['PerUnitDeclaredValue']['Value'] : null;
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderItemId
     *
     * @param string $orderItemId
     *
     * @return OrderItem
     */
    public function setOrderItemId($orderItemId)
    {
        $this->orderItemId = $orderItemId;

        return $this;
    }

    /**
     * Get orderItemId
     *
     * @return string
     */
    public function getOrderItemId()
    {
        return $this->orderItemId;
    }

    /**
     * Set aSIN
     *
     * @param string $aSIN
     *
     * @return OrderItem
     */
    public function setASIN($aSIN)
    {
        $this->ASIN = $aSIN;

        return $this;
    }

    /**
     * Get aSIN
     *
     * @return string
     */
    public function getASIN()
    {
        return $this->ASIN;
    }

    /**
     * Set sellerSKU
     *
     * @param string $sellerSKU
     *
     * @return OrderItem
     */
    public function setSellerSKU($sellerSKU)
    {
        $this->sellerSKU = $sellerSKU;

        return $this;
    }

    /**
     * Get sellerSKU
     *
     * @return string
     */
    public function getSellerSKU()
    {
        return $this->sellerSKU;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return OrderItem
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set quantityOrdered
     *
     * @param integer $quantityOrdered
     *
     * @return OrderItem
     */
    public function setQuantityOrdered($quantityOrdered)
    {
        $this->quantityOrdered = $quantityOrdered;

        return $this;
    }

    /**
     * Get quantityOrdered
     *
     * @return integer
     */
    public function getQuantityOrdered()
    {
        return $this->quantityOrdered;
    }

    /**
     * Set quantityShipped
     *
     * @param integer $quantityShipped
     *
     * @return OrderItem
     */
    public function setQuantityShipped($quantityShipped)
    {
        $this->quantityShipped = $quantityShipped;

        return $this;
    }

    /**
     * Get quantityShipped
     *
     * @return integer
     */
    public function getQuantityShipped()
    {
        return $this->quantityShipped;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return OrderItem
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set itemPriceAmount
     *
     * @param float $itemPriceAmount
     *
     * @return OrderItem
     */
    public function setItemPriceAmount($itemPriceAmount)
    {
        $this->itemPriceAmount = $itemPriceAmount;

        return $this;
    }

    /**
     * Get itemPriceAmount
     *
     * @return float
     */
    public function getItemPriceAmount()
    {
        return $this->itemPriceAmount;
    }

    /**
     * Set shippingPriceAmount
     *
     * @param float $shippingPriceAmount
     *
     * @return OrderItem
     */
    public function setShippingPriceAmount($shippingPriceAmount)
    {
        $this->shippingPriceAmount = $shippingPriceAmount;

        return $this;
    }

    /**
     * Get shippingPriceAmount
     *
     * @return float
     */
    public function getShippingPriceAmount()
    {
        return $this->shippingPriceAmount;
    }

    /**
     * Set itemTaxPriceAmount
     *
     * @param float $itemTaxPriceAmount
     *
     * @return OrderItem
     */
    public function setItemTaxPriceAmount($itemTaxPriceAmount)
    {
        $this->itemTaxPriceAmount = $itemTaxPriceAmount;

        return $this;
    }

    /**
     * Get itemTaxPriceAmount
     *
     * @return float
     */
    public function getItemTaxPriceAmount()
    {
        return $this->itemTaxPriceAmount;
    }

    /**
     * Set shippingTaxAmount
     *
     * @param float $shippingTaxAmount
     *
     * @return OrderItem
     */
    public function setShippingTaxAmount($shippingTaxAmount)
    {
        $this->shippingTaxAmount = $shippingTaxAmount;

        return $this;
    }

    /**
     * Get shippingTaxAmount
     *
     * @return float
     */
    public function getShippingTaxAmount()
    {
        return $this->shippingTaxAmount;
    }

    /**
     * Set shippingDiscount
     *
     * @param float $shippingDiscount
     *
     * @return OrderItem
     */
    public function setShippingDiscount($shippingDiscount)
    {
        $this->shippingDiscount = $shippingDiscount;

        return $this;
    }

    /**
     * Get shippingDiscount
     *
     * @return float
     */
    public function getShippingDiscount()
    {
        return $this->shippingDiscount;
    }

    /**
     * Set promotionDiscount
     *
     * @param float $promotionDiscount
     *
     * @return OrderItem
     */
    public function setPromotionDiscount($promotionDiscount)
    {
        $this->promotionDiscount = $promotionDiscount;

        return $this;
    }

    /**
     * Get promotionDiscount
     *
     * @return float
     */
    public function getPromotionDiscount()
    {
        return $this->promotionDiscount;
    }

    /**
     * Set order
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Orders $order
     *
     * @return OrderItem
     */
    public function setOrder(\AMZcockpitDoctrine\Entity\MWS\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return OrderItem
     */
    public function setProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set sellerFulfillmentOrderItemId
     *
     * @param string $sellerFulfillmentOrderItemId
     *
     * @return OrderItem
     */
    public function setSellerFulfillmentOrderItemId($sellerFulfillmentOrderItemId)
    {
        $this->sellerFulfillmentOrderItemId = $sellerFulfillmentOrderItemId;

        return $this;
    }

    /**
     * Get sellerFulfillmentOrderItemId
     *
     * @return string
     */
    public function getSellerFulfillmentOrderItemId()
    {
        return $this->sellerFulfillmentOrderItemId;
    }

    /**
     * Set fulfillmentNetworkSKU
     *
     * @param string $fulfillmentNetworkSKU
     *
     * @return OrderItem
     */
    public function setFulfillmentNetworkSKU($fulfillmentNetworkSKU)
    {
        $this->fulfillmentNetworkSKU = $fulfillmentNetworkSKU;

        return $this;
    }

    /**
     * Get fulfillmentNetworkSKU
     *
     * @return string
     */
    public function getFulfillmentNetworkSKU()
    {
        return $this->fulfillmentNetworkSKU;
    }

    /**
     * Set estimatedShipDateTime
     *
     * @param \DateTime $estimatedShipDateTime
     *
     * @return OrderItem
     */
    public function setEstimatedShipDateTime($estimatedShipDateTime)
    {
        $this->estimatedShipDateTime = new \DateTime($estimatedShipDateTime);

        return $this;
    }

    /**
     * Get estimatedShipDateTime
     *
     * @return \DateTime
     */
    public function getEstimatedShipDateTime()
    {
        return $this->estimatedShipDateTime;
    }

    /**
     * Set estimatedArrivalDateTime
     *
     * @param \DateTime $estimatedArrivalDateTime
     *
     * @return OrderItem
     */
    public function setEstimatedArrivalDateTime($estimatedArrivalDateTime)
    {
        $this->EstimatedArrivalDateTime = new \DateTime($estimatedArrivalDateTime);

        return $this;
    }

    /**
     * Get estimatedArrivalDateTime
     *
     * @return \DateTime
     */
    public function getEstimatedArrivalDateTime()
    {
        return $this->EstimatedArrivalDateTime;
    }

    /**
     * Add fee
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Fee $fee
     *
     * @return OrderItem
     */
    public function addFee(\AMZcockpitDoctrine\Entity\MWS\Fee $fee)
    {
        $fee->setOrderItem($this);
        if(count($this->fees) > 0) {
            /** @var \AMZcockpitDoctrine\Entity\MWS\Fee $cFee */
            foreach ($this->fees as $cFee) {
                if($cFee->getType() == $fee->getType())
                    return $this;
            }
        }
        $this->fees[] = $fee;

        return $this;
    }

    /**
     * Remove fee
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Fee $fee
     */
    public function removeFee(\AMZcockpitDoctrine\Entity\MWS\Fee $fee)
    {
        $this->fees->removeElement($fee);
    }

    /**
     * Get fees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * Get fees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function setFees($fees)
    {
        return $this->fees = $fees;
    }

    /**
     * Set feesData
     *
     * @param array $feesData
     *
     * @return OrderItem
     */
    public function setFeesData($feesData)
    {
        $this->feesData = $feesData;

        return $this;
    }

    /**
     * Get feesData
     *
     * @return array
     */
    public function getFeesData()
    {
        return $this->feesData;
    }

    /**
     * Set carrier
     *
     * @param string $carrier
     *
     * @return OrderItem
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set shipmentId
     *
     * @param string $shipmentId
     *
     * @return OrderItem
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;

        return $this;
    }

    /**
     * Get shipmentId
     *
     * @return string
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * Set shipmentDate
     *
     * @param \DateTime $shipmentDate
     *
     * @return OrderItem
     */
    public function setShipmentDate($shipmentDate)
    {
        $isValid = $this->validateDate($shipmentDate, \DateTime::ISO8601);
        $this->shipmentDate = $isValid ? new \DateTime($shipmentDate) : null;

        return $this;
    }

    /**
     * Get shipmentDate
     *
     * @return \DateTime
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }

    /**
     * Set trackingNumber
     *
     * @param string $trackingNumber
     *
     * @return OrderItem
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    /**
     * Get trackingNumber
     *
     * @return string
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * Set giftWrapPrice
     *
     * @param float $giftWrapPrice
     *
     * @return OrderItem
     */
    public function setGiftWrapPrice($giftWrapPrice)
    {
        $this->giftWrapPrice = $giftWrapPrice;

        return $this;
    }

    /**
     * Get giftWrapPrice
     *
     * @return float
     */
    public function getGiftWrapPrice()
    {
        return $this->giftWrapPrice;
    }

    /**
     * Set giftWrapTax
     *
     * @param float $giftWrapTax
     *
     * @return OrderItem
     */
    public function setGiftWrapTax($giftWrapTax)
    {
        $this->giftWrapTax = $giftWrapTax;

        return $this;
    }

    /**
     * Get giftWrapTax
     *
     * @return float
     */
    public function getGiftWrapTax()
    {
        return $this->giftWrapTax;
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Set isRefunded
     *
     * @param boolean $isRefunded
     *
     * @return OrderItem
     */
    public function setIsRefunded($isRefunded)
    {
        $this->isRefunded = $isRefunded;

        return $this;
    }

    /**
     * Get isRefunded
     *
     * @return boolean
     */
    public function getIsRefunded()
    {
        return $this->isRefunded;
    }
}
