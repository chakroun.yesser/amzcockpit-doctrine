<?php

namespace AMZcockpitDoctrine\Entity\MWS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductInfo
 *
 * @ORM\Table(name="products_infos")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\MWS\ProductInfoRepository")
 */
class ProductInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="title", type="text", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $title;

    /**
     * @ORM\Column(name="binding", type="string", nullable=true)
     */
    private $binding;

    /**
     * @ORM\Column(name="feature", type="array", nullable=true, options={"collation":"utf8mb4_general_ci"})
     */
    private $feature;

    /**
     * @ORM\Column(name="marketplace_id", type="string")
     */
    private $marketplaceId;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tva;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tvaFBM;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\ProductCategory", inversedBy="productsInfos", cascade={"persist"}, fetch="EAGER")
     */
    protected $productInfosCategorie;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\MWS\Product", inversedBy="productInfos", cascade={"persist"})
     */
    protected $product;


    public function __construct($data = null)
    {
        if(!is_null($data)) {
            $this->title                = isset($data['AttributeSets'][0]['Title']) ? $data['AttributeSets'][0]['Title'] : null;
            $this->binding              = isset($data['AttributeSets'][0]['Binding']) ? $data['AttributeSets'][0]['Binding'] : null;
            $this->feature              = isset($data['AttributeSets'][0]['Feature']) ? $data['AttributeSets'][0]['Feature'] : null;
            $this->marketplaceId        = isset($data['Identifiers']['MarketplaceASIN']['MarketplaceId']) ? $data['Identifiers']['MarketplaceASIN']['MarketplaceId'] : null;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProductInfo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set binding
     *
     * @param string $binding
     *
     * @return ProductInfo
     */
    public function setBinding($binding)
    {
        $this->binding = $binding;

        return $this;
    }

    /**
     * Get binding
     *
     * @return string
     */
    public function getBinding()
    {
        return $this->binding;
    }

    /**
     * Set feature
     *
     * @param array $feature
     *
     * @return ProductInfo
     */
    public function setFeature($feature)
    {
        $this->feature = $feature;

        return $this;
    }

    /**
     * Get feature
     *
     * @return array
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * Set marketplaceId
     *
     * @param string $marketplaceId
     *
     * @return ProductInfo
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return string
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set product
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\Product $product
     *
     * @return ProductInfo
     */
    public function setProduct(\AMZcockpitDoctrine\Entity\MWS\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AMZcockpitDoctrine\Entity\MWS\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Get productInfosCategorie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductInfosCategories()
    {
        $return = new \Doctrine\Common\Collections\ArrayCollection();
        if($this->productInfosCategorie) {
            /** @var ProductCategory $item */
            foreach ($this->productInfosCategorie as $item) {
                if($item->getRank()) {
                    $return->add($item);
                }
            }
            return $return;
        }
        return null;
    }

    /**
     * Set productInfosCategorie
     *
     * @param \AMZcockpitDoctrine\Entity\MWS\ProductCategory $productInfosCategorie
     *
     * @return ProductInfo
     */
    public function setProductInfosCategorie(\AMZcockpitDoctrine\Entity\MWS\ProductCategory $productInfosCategorie = null)
    {
        $this->productInfosCategorie = $productInfosCategorie;

        return $this;
    }

    /**
     * Get productInfosCategorie
     *
     * @return ProductInfo
     */
    public function getProductInfosCategorie()
    {
        return $this->productInfosCategorie;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductInfo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tva.
     *
     * @param float|null $tva
     *
     * @return ProductInfo
     */
    public function setTva($tva = null)
    {
        $this->tva = $tva;
        return $this;
    }

    /**
     * Get tva.
     *
     * @return float|null
     */
    public function getTva()
    {
        return str_replace('.',',',$this->tva);
    }

    /**
     * Set tvaFBM.
     *
     * @param float|null $tvaFBM
     *
     * @return ProductInfo
     */
    public function setTvaFBM($tvaFBM = null)
    {
        $this->tvaFBM = $tvaFBM;

        return $this;
    }

    /**
     * Get tvaFBM.
     *
     * @return float|null
     */
    public function getTvaFBM()
    {
        return str_replace('.',',',$this->tvaFBM);
    }
}
