<?php

namespace AMZcockpitDoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Email
 *
 * @ORM\Table(name="emails_variables")
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\EmailVariableRepository")
 */
class EmailVariable
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="identifier", type="string")
     */
    private $identifier;

    /**
     * @ORM\ManyToOne(targetEntity="AMZcockpitDoctrine\Entity\User", inversedBy="emailsVariables")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AMZcockpitDoctrine\Entity\EmailProductVariable", mappedBy="emailVariable", cascade={"persist", "remove"})
     */
    protected $emailsProductsVariables;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     *
     * @return EmailVariable
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set user
     *
     * @param \AMZcockpitDoctrine\Entity\User $user
     *
     * @return EmailVariable
     */
    public function setUser(\AMZcockpitDoctrine\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AMZcockpitDoctrine\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->emailsProductsVariables = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add emailsProductsVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductsVariable
     *
     * @return EmailVariable
     */
    public function addEmailsProductsVariable(\AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductsVariable)
    {
        $emailsProductsVariable->setEmailVariable($this);
        $this->emailsProductsVariables[] = $emailsProductsVariable;

        return $this;
    }

    /**
     * Remove emailsProductsVariable
     *
     * @param \AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductsVariable
     */
    public function removeEmailsProductsVariable(\AMZcockpitDoctrine\Entity\EmailProductVariable $emailsProductsVariable)
    {
        $this->emailsProductsVariables->removeElement($emailsProductsVariable);
    }

    /**
     * Get emailsProductsVariables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailsProductsVariables()
    {
        return $this->emailsProductsVariables;
    }
}
