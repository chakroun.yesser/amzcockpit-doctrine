<?php

namespace AMZcockpitDoctrine\Entity;

use AMZcockpitDoctrine\DBAL\Types\SupportHelpdeskCategoryType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * HelpdeskQuestion
 *
 * @ORM\Table(name="helpdesk_question", options={"collate":"utf8mb4_general_ci", "charset":"utf8mb4"})
 * @ORM\Entity(repositoryClass="AMZcockpitDoctrine\Repository\HelpdeskQuestionRepository")
 */
class HelpdeskQuestion
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="subtitle", type="string", nullable=true)
     */
    private $subtitle;

    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = false;

    /**
     * @ORM\Column(name="shared", type="boolean", options={"default" : 1})
     */
    private $shared = false;

    /**
     * @ORM\Column(name="body", type="text", options={"collation":"utf8mb4_general_ci"})
     */
    private $body;

    /**
     * @ORM\ManyToMany(targetEntity="AMZcockpitDoctrine\Entity\User")
     * @ORM\JoinTable(name="helpdesk_useful")
     */
    protected $useful;

    /**
     * @DoctrineAssert\Enum(entity="AMZcockpitDoctrine\DBAL\Types\SupportHelpdeskCategoryType")
     * @ORM\Column(name="page", type="SupportHelpdeskCategoryType", length=255, nullable=true)
     */
    private $page;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->shared = true;
        $this->useful = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return HelpdeskQuestion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return HelpdeskQuestion
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return HelpdeskQuestion
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return HelpdeskQuestion
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Add useful
     *
     * @param \AMZcockpitDoctrine\Entity\User $useful
     *
     * @return HelpdeskQuestion
     */
    public function addUseful(\AMZcockpitDoctrine\Entity\User $useful)
    {
        $this->useful[] = $useful;

        return $this;
    }

    /**
     * Remove useful
     *
     * @param \AMZcockpitDoctrine\Entity\User $useful
     */
    public function removeUseful(\AMZcockpitDoctrine\Entity\User $useful)
    {
        $this->useful->removeElement($useful);
    }

    /**
     * Get useful
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUseful()
    {
        return $this->useful;
    }

    /**
     * Set page
     *
     * @param SupportHelpdeskCategoryType|string $page
     *
     * @return HelpdeskQuestion
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return SupportHelpdeskCategoryType|string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set shared
     *
     * @param boolean $shared
     *
     * @return HelpdeskQuestion
     */
    public function setShared($shared)
    {
        $this->shared = $shared;

        return $this;
    }

    /**
     * Get shared
     *
     * @return boolean
     */
    public function getShared()
    {
        return $this->shared;
    }
}
