<?php

namespace AMZcockpitDoctrine\Repository\MWS;

use AMZcockpitDoctrine\Entity\MWS\Product;
use Doctrine\ORM\QueryBuilder;
use AMZcockpitDoctrine\Entity\User;

/**
 * ReviewRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReviewRepository extends \Doctrine\ORM\EntityRepository
{
    public function getReviewByUser(User $user) {

        $qb = $this->createQueryBuilder('r')
            ->leftJoin('r.product','p')
            ->leftJoin('p.store','s');
        $qb->select('r.rating, COUNT(r.id)')
            ->where($qb->expr()->eq('s.user',':user'))
            ->setParameter('user',$user)
            ->groupBy('r.rating');

        $items = $qb->getQuery()->getResult();

        $colors = [
            1   => '#990000',
            2   => '#C38600',
            3   => '#FCD202',
            4   => '#1FBC77',
            5   => '#1E9D5A'
        ];
        $return = [];
        foreach ($items as $item) {
            $return[] = [
                'rating' => '(Rating '.$item['rating'].')',
                'value'  => $item['1'],
                'color'  => $colors[$item['rating']]
            ];
        }
        return $return;
    }

    public function getReviewCount(User $user, $filters = null) {

        $qb = $this->createQueryBuilder('r');
        $qb->select('COUNT(r.id) as reviewsCount')
            ->leftJoin('r.product','p')
            ->leftJoin('p.store','s');
        $qb->where($qb->expr()->eq('s.user',':user'))
            ->setParameter('user',$user);

        if($filters) {
            if(!empty($filters['from'])) {
                $from = $filters['from'];
                if(!empty($filters['to'])) {
                    $to   = $filters['to'];
                    /** @var QueryBuilder $qb */
                    $qb
                        ->andWhere('r.date BETWEEN :from AND :to')
                        ->setParameter('from', $from )
                        ->setParameter('to', $to);
                } else {

                    /** @var QueryBuilder $qb */
                    $qb
                        ->andWhere('r.date >= :from')
                        ->setParameter('from', $from );
                }
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getReviewCountByProduct(Product $product) {

        $qb = $this->createQueryBuilder('r');
        $qb->select('COUNT(r) as reviewsCount');
        $qb->where($qb->expr()->eq('r.product',':product'))
            ->setParameter('product',$product);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getReviewAverageByProduct(Product $product) {

        $qb = $this->createQueryBuilder('r');
        $qb->select('AVG(r.rating) as reviewsAverage');
        $qb->where($qb->expr()->eq('r.product',':product'))
            ->setParameter('product',$product);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getReviewsSnapshotPositif(User $user) {
        $return = [];
        $days = [7, 30, 90, 180, 365];
        $i = 0;
        while(isset($days[$i])) {
            $date = (new \DateTime('now'))->sub(new \DateInterval('P'.$days[$i].'D'));
            $qb = $this->createQueryBuilder('r');
            $qb->select('COUNT(r) as reviewsCount');
            $qb->leftJoin('r.product','p')
                ->leftJoin('p.store','s')
                ->where('s.user = :user')
                ->setParameter('user', $user);

            $qb->andWhere($qb->expr()->gte('r.date',':date'))->setParameter('date',$date);
            $qb->andWhere($qb->expr()->gt('r.rating',':rating'))->setParameter('rating',3);
            $orderCounts = $qb->getQuery()->getSingleScalarResult();
            $return[$days[$i]] = $orderCounts;
            $i++;
        }
        return $return;
    }

    public function getReviewsSnapshotNegatif(User $user) {
        $return = [];
        $days = [7, 30, 90, 180, 365];
        $i = 0;
        while(isset($days[$i])) {
            $date = (new \DateTime('now'))->sub(new \DateInterval('P'.$days[$i].'D'));
            $qb = $this->createQueryBuilder('r');
            $qb->select('COUNT(r) as reviewsCount');
            $qb->leftJoin('r.product','p')
                ->leftJoin('p.store','s')
                ->where('s.user = :user')
                ->setParameter('user', $user);

            $qb->andWhere($qb->expr()->gte('r.date',':date'))->setParameter('date',$date);
            $qb->andWhere($qb->expr()->lte('r.rating',':rating'))->setParameter('rating',3);
            $orderCounts = $qb->getQuery()->getSingleScalarResult();
            $return[$days[$i]] = $orderCounts;
            $i++;
        }
        return $return;
    }

    /**
     * @return array
     */
    public function getDailyStatisticsReport(User $user = null) {

        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('r');
        $qb->select('r.createdAt, DAY(r.createdAt) AS gDay, MONTH(r.createdAt) AS gDmonth, YEAR(r.createdAt) AS gDyear, COUNT(r.id) as _count');
        $qb->where($qb->expr()->isNotNull('r.createdAt'));
        $qb->addGroupBy('gDay');
        $qb->addGroupBy('gDmonth');
        $qb->addGroupBy('gDyear');
        $qb->orderBy('gDyear','ASC');
        $qb->addOrderBy('gDmonth','ASC');
        $qb->addOrderBy('gDay','ASC');

        $items = $qb->getQuery()->getResult();

        $return = [];
        foreach ($items as $item) {
            $return[] = [
                'date' => $item['createdAt']->format('Y-m-d'),
                'value' => (int)$item['_count']
            ];
        }

        return $return;
    }

    public function getDuplicatedReviews() {

        $qb = $this->createQueryBuilder('r');
        $qb->select('r')->groupBy('r.idAmazon');
        $qb->having('COUNT(r) > 1');

        return $qb->getQuery()->getResult();
    }
}
