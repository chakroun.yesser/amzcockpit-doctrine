<?php

namespace AMZcockpitDoctrine\Repository\MWS;

use AMZcockpitDoctrine\Entity\MWS\Store;
use Doctrine\ORM\QueryBuilder;
use AMZcockpitDoctrine\Entity\User;

/**
 * ProductInfoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductInfoRepository extends \Doctrine\ORM\EntityRepository
{
    public function getDuplicatedProductsInfos(Store $store = null) {

        $qb = $this->createQueryBuilder('pi');
        $qb->select('pi')
            ->groupBy('pi.marketplaceId')
            ->addGroupBy('pi.product');

        if($store) {
            $qb->leftJoin('pi.product','p');
            $qb->andWhere($qb->expr()->eq('p.store',':store'))
                ->setParameter('store',$store);
        }

        $qb->having('COUNT(pi) > 1');
        return $qb->getQuery()->getResult();
    }

}
