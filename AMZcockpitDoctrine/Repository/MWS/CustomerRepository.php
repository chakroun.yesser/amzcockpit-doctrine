<?php

namespace AMZcockpitDoctrine\Repository\MWS;

use AMZcockpitDoctrine\Entity\MWS\Store;

/**
 * CustomerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CustomerRepository extends \Doctrine\ORM\EntityRepository
{

    public function getCustomersByStore(Store $store) {

        $qb = $this->createQueryBuilder('c');
        $qb->select('c')->distinct(true);
        $qb
            ->leftJoin('c.orders','o')
            ->where($qb->expr()->eq('o.store',':store'))
            ->setParameter('store',$store);

        return $qb->getQuery()->getResult();
    }
}
