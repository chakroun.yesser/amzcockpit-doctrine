<?php

namespace AMZcockpitDoctrine\Repository;
use AMZcockpitDoctrine\Entity\MWS\Orders;
use AMZcockpitDoctrine\Entity\Email;
use Doctrine\Common\Collections\ArrayCollection;
use AMZcockpitDoctrine\Entity\User;

/**
 * EmailBroadcastRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EmailBroadcastRepository extends \Doctrine\ORM\EntityRepository
{

}
