<?php

namespace AMZcockpitDoctrine\Repository;
use AMZcockpitDoctrine\DBAL\Types\FrequencyType;
use AMZcockpitDoctrine\Entity\ComptabilityPurchase;
use Doctrine\ORM\QueryBuilder;
use AMZcockpitDoctrine\Entity\User;

/**
 * ComptabilityPurchaseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ComptabilityPurchaseRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return array
     */
    public function getComptabilitySimplifiedIncome(User $user, $year = null) {
        if(is_null($year)) {
            $from = new \DateTime('first day of this year 00:00:00');
            $to = new \DateTime('last day of this month');
        } else {
            $from = \DateTime::createFromFormat('d/m/Y H:i:s','01/01/'.$year.' 00:00:00');
            $to = \DateTime::createFromFormat('d/m/Y H:i:s','31/12/'.$year.' 23:59:59');
        }
        $mois = [ "Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Aout", "Sep", "Oct", "Nov", "Déc" ];

        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->where($qb->expr()->eq('p.user',':user'));

        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('p.frequency', ':none_frequency'),
                $qb->expr()->andX(
                    $qb->expr()->neq('p.frequency', ':none_frequency'),
                    $qb->expr()->gt('p.dateEnd', ':from')
                )
            )
        )
        ->setParameter('none_frequency', FrequencyType::NONE )
        ->setParameter('user', $user)
        ->setParameter('from', $from );

        $qb->orderBy('p.date','ASC');
        $comptabilityPurchases = $qb->getQuery()->getResult();

        $return = [];
        foreach ($mois as $index)
            $return[$index] = 0;

        /** @var ComptabilityPurchase $comptabilityPurchase */
        foreach ($comptabilityPurchases as $comptabilityPurchase) {
            if(is_null($year)) {
                $from = new \DateTime('first day of this year 00:00:00');
                $to = new \DateTime('last day of this month');
            } else {
                $from = \DateTime::createFromFormat('d/m/Y H:i:s','01/01/'.$year.' 00:00:00');
                $to = \DateTime::createFromFormat('d/m/Y H:i:s','31/12/'.$year.' 23:59:59');
            }

            if($comptabilityPurchase->getFrequency() == FrequencyType::NONE) {
                $monthNumber = $comptabilityPurchase->getDate()->format('m');
                $return[$mois[$monthNumber-1]] += (float)$comptabilityPurchase->getAmount();
            } else {
                $maxDate = clone $comptabilityPurchase->getDateEnd();
                if($maxDate > $to) {
                    $maxDate = $to;
                    $maxDate->setDate($to->format('Y'), $to->format('m'), $maxDate->format('d'));
                }
                $minDate = clone $comptabilityPurchase->getDate();
                if($minDate < $from) {
                    $minDate = $from;
                    $minDate->setDate($from->format('Y'), $from->format('m'), $minDate->format('d'));
                }
                if($comptabilityPurchase->getFrequency() == FrequencyType::MONTHLY) {
                    while ($minDate <= $maxDate) {
                        $return[$mois[$minDate->format('m')-1]] += (float)$comptabilityPurchase->getAmount();
                        $minDate->add(new \DateInterval('P1M'));
                    }
                } elseif($comptabilityPurchase->getFrequency() == FrequencyType::DAILY) {
                    while ($minDate <= $maxDate) {
                        $return[$mois[$minDate->format('m') - 1]] += (float)$comptabilityPurchase->getAmount();
                        $minDate->add(new \DateInterval('P1D'));
                    }
                } else {
                    $return[$mois[$minDate->format('m')-1]] += (float)$comptabilityPurchase->getAmount();
                }
            }
        }

        return $return;
    }

    /**
     * @return array
     */
    public function getComptabilityTvaProjection(User $user, $year = null) {
        if(is_null($year)) {
            $from = new \DateTime('first day of this year 00:00:00');
            $to = new \DateTime('last day of this month');
        } else {
            $from = \DateTime::createFromFormat('d/m/Y H:i:s','01/01/'.$year.' 00:00:00');
            $to = \DateTime::createFromFormat('d/m/Y H:i:s','31/12/'.$year.' 23:59:59');
        }
        $mois = [ "Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Aout", "Sep", "Oct", "Nov", "Déc" ];

        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->where($qb->expr()->eq('p.user',':user'));

        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('p.frequency', ':none_frequency'),
                $qb->expr()->andX(
                    $qb->expr()->neq('p.frequency', ':none_frequency'),
                    $qb->expr()->gt('p.dateEnd', ':from')
                )
            )
        )
        ->setParameter('none_frequency', FrequencyType::NONE )
        ->setParameter('user', $user)
        ->setParameter('from', $from );

        $qb->orderBy('p.date','ASC');
        $comptabilityPurchases = $qb->getQuery()->getResult();

        $return = [];
        foreach ($mois as $index)
            $return[$index] = [
                'amount' => 0,
                'tva_deductible' => 0
            ];

        /** @var ComptabilityPurchase $comptabilityPurchase */
        foreach ($comptabilityPurchases as $comptabilityPurchase) {
            if(is_null($year)) {
                $from = new \DateTime('first day of this year 00:00:00');
                $to = new \DateTime('last day of this month');
            } else {
                $from = \DateTime::createFromFormat('d/m/Y H:i:s','01/01/'.$year.' 00:00:00');
                $to = \DateTime::createFromFormat('d/m/Y H:i:s','31/12/'.$year.' 23:59:59');
            }
            if($comptabilityPurchase->getFrequency() == FrequencyType::NONE) {
                $monthNumber = $comptabilityPurchase->getDate()->format('m');
                $return[$mois[$monthNumber-1]]['amount'] += -1*(float)$comptabilityPurchase->getAmount();
                $return[$mois[$monthNumber-1]]['tva_deductible'] += (float)$comptabilityPurchase->getAmount() - (float)$comptabilityPurchase->getAmount()/(1 + $comptabilityPurchase->getSupplier()->getTva()/100);
            } else {
                $maxDate = $comptabilityPurchase->getDateEnd();
                if($maxDate > $to) {
                    $maxDate = $to;
                    $maxDate->setDate($to->format('Y'), $to->format('m'), $maxDate->format('d'));
                }
                $minDate = $comptabilityPurchase->getDate();
                if($minDate < $from) {
                    $minDate = $from;
                    $minDate->setDate($from->format('Y'), $from->format('m'), $minDate->format('d'));
                }
                if($comptabilityPurchase->getFrequency() == FrequencyType::MONTHLY) {
                    while ($minDate <= $maxDate) {
                        $return[$mois[$minDate->format('m')-1]]['amount'] += -1*(float)$comptabilityPurchase->getAmount();
                        $return[$mois[$minDate->format('m')-1]]['tva_deductible'] += (float)$comptabilityPurchase->getAmount() - (float)$comptabilityPurchase->getAmount()/( 1+ $comptabilityPurchase->getSupplier()->getTva()/100);
                        $minDate->add(new \DateInterval('P1M'));
                    }
                } elseif($comptabilityPurchase->getFrequency() == FrequencyType::DAILY) {
                    while ($minDate <= $maxDate) {
                        $return[$mois[$minDate->format('m')-1]]['amount'] += -1*(float)$comptabilityPurchase->getAmount();
                        $return[$mois[$minDate->format('m')-1]]['tva_deductible'] += (float)$comptabilityPurchase->getAmount() - (float)$comptabilityPurchase->getAmount()/(1 + $comptabilityPurchase->getSupplier()->getTva()/100);
                        $minDate->add(new \DateInterval('P1D'));
                    }
                } else {
                    $return[$mois[$comptabilityPurchase->getDate()->format('m')-1]]['amount'] += -1*(float)$comptabilityPurchase->getAmount();
                    $return[$mois[$comptabilityPurchase->getDate()->format('m')-1]]['tva_deductible'] += (float)$comptabilityPurchase->getAmount() - (float)$comptabilityPurchase->getAmount()/ (1 + $comptabilityPurchase->getSupplier()->getTva()/100);
                }
            }
        }

        return $return;
    }

    /**
     * @return array
     */
    public function getComptabilityByDate(User $user, $date) {

        $date = \DateTime::createFromFormat('d-m-Y',$date);
        $date->setTime(0, 0, 0);
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->where($qb->expr()->eq('p.user',':user'));

        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->eq('p.frequency', ':none_frequency'),
                    $qb->expr()->eq('p.date', ':dateStart')
                ),
                $qb->expr()->andX(
                    $qb->expr()->neq('p.frequency', ':none_frequency'),
                    $qb->expr()->lte('p.date', ':dateStart'),
                    $qb->expr()->gte('p.dateEnd', ':dateStart'),
                    $qb->expr()->eq('DAY(p.date)', ':dateDay')
                )
            )
        )
        ->setParameter('none_frequency', FrequencyType::NONE )
        ->setParameter('user', $user)
        ->setParameter('dateStart', $date)
        ->setParameter('dateDay', $date->format('d'));

        $qb->orderBy('p.date','ASC');
        return $qb->getQuery()->getResult();
    }

    public function getComptabilityByDateRange(User $user, array $filters)
    {

        if (!empty($filters['from'])) $filters['from']->setTime(0, 0, 0);
        if (!empty($filters['to'])) $filters['to']->setTime(23, 59, 59);

        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');

        $qb->select('p.date AS _date, p.amount, DAY(p.date) AS gDay, MONTH(p.date) AS gDmonth, YEAR(p.date) AS gDyear');
        $qb->where($qb->expr()->eq('p.user',':user'));

        $qb->andWhere($qb->expr()->eq('p.frequency', ':none_frequency'))
        ->setParameter('none_frequency', FrequencyType::NONE )
        ->setParameter('user', $user);

        if ($filters) {
            if (!empty($filters)) {
                if (!empty($filters['from'])) {
                    $from = $filters['from'];
                    if (!empty($filters['to'])) {
                        $to = $filters['to'];
                        /** @var QueryBuilder $qb */
                        $qb
                            ->andWhere('p.date BETWEEN :from AND :to')
                            ->setParameter('from', $from)
                            ->setParameter('to', $to);
                    } else {

                        /** @var QueryBuilder $qb */
                        $qb
                            ->andWhere('p.date >= :from')
                            ->setParameter('from', $from);
                    }
                }
            }
        }

        $qb->orderBy('p.date','ASC');
        $items =  $qb->getQuery()->getResult();
        $return = [];

        if(count($items)) {
            foreach ($items as $item) {
                $key = $item['_date']->format('d-m-Y');
                if(!isset($return[$key]))
                    $return[$key] = 0;
                $return[$key] += $item['amount'];
            }
        }

        return $return;
    }



    /**
     * @return array
     */
    public function getComptabilityByDateRangeReport(User $user, array $filters) {
        if (!empty($filters['from'])) $filters['from']->setTime(0, 0, 0);
        if (!empty($filters['to'])) $filters['to']->setTime(23, 59, 59);
        $from = $filters['from'];
        $to = $filters['to'];

        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->where($qb->expr()->eq('p.user',':user'));

        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->eq('p.frequency', ':none_frequency'),
                $qb->expr()->andX(
                    $qb->expr()->neq('p.frequency', ':none_frequency'),
                    $qb->expr()->gt('p.dateEnd', ':from')
                )
            )
        )
            ->setParameter('none_frequency', FrequencyType::NONE )
            ->setParameter('user', $user)
            ->setParameter('from', $from );

        $qb->orderBy('p.date','ASC');
        $comptabilityPurchases = $qb->getQuery()->getResult();

        $return = [];

        /** @var ComptabilityPurchase $comptabilityPurchase */
        foreach ($comptabilityPurchases as $comptabilityPurchase) {
            if($comptabilityPurchase->getFrequency() == FrequencyType::NONE) {
                $timestamp = $comptabilityPurchase->getDate()->getTimestamp();
                while (isset($return[$timestamp])) {
                    $timestamp++;
                }
                $return[$timestamp] = [
                    'date' => $comptabilityPurchase->getDate(),
                    'reference' => $comptabilityPurchase->getReference(),
                    'category' => $comptabilityPurchase->getCategory(),
                    'libelle' => $comptabilityPurchase->getTitle(),
                    'supplier' => $comptabilityPurchase->getSupplier()->getName(),
                    'amount' => $comptabilityPurchase->getAmount(),
                    'comment' => $comptabilityPurchase->getComment()
                ];
            } else {
                $maxDate = $comptabilityPurchase->getDateEnd();
                if($maxDate > $to) {
                    $maxDate = $to;
                    $maxDate->setDate($to->format('Y'), $to->format('m'), $maxDate->format('d'));
                }
                $minDate = $comptabilityPurchase->getDate();
                if($minDate < $from) {
                    $minDate = $from;
                    $minDate->setDate($from->format('Y'), $from->format('m'), $minDate->format('d'));
                }
                if($comptabilityPurchase->getFrequency() == FrequencyType::MONTHLY) {
                    while ($minDate <= $maxDate) {
                        $return[$minDate->getTimestamp()] = [
                            'date' => $minDate,
                            'reference' => $comptabilityPurchase->getReference(),
                            'category' => $comptabilityPurchase->getCategory(),
                            'supplier' => $comptabilityPurchase->getSupplier()->getName(),
                            'libelle' => $comptabilityPurchase->getTitle(),
                            'amount' => $comptabilityPurchase->getAmount(),
                            'comment' => $comptabilityPurchase->getComment()
                        ];
                        $minDate->add(new \DateInterval('P1M'));
                    }
                } elseif($comptabilityPurchase->getFrequency() == FrequencyType::DAILY) {
                    while ($minDate <= $maxDate) {
                        $return[$minDate->getTimestamp()] = [
                            'date' => $minDate,
                            'reference' => $comptabilityPurchase->getReference(),
                            'category' => $comptabilityPurchase->getCategory(),
                            'supplier' => $comptabilityPurchase->getSupplier()->getName(),
                            'libelle' => $comptabilityPurchase->getTitle(),
                            'amount' => $comptabilityPurchase->getAmount(),
                            'comment' => $comptabilityPurchase->getComment()
                        ];
                        $minDate->add(new \DateInterval('P1D'));
                    }
                } else {
                    while ($minDate <= $maxDate) {
                        $return[$minDate->getTimestamp()] = [
                            'date' => $minDate,
                            'reference' => $comptabilityPurchase->getReference(),
                            'category' => $comptabilityPurchase->getCategory(),
                            'supplier' => $comptabilityPurchase->getSupplier()->getName(),
                            'libelle' => $comptabilityPurchase->getTitle(),
                            'amount' => $comptabilityPurchase->getAmount(),
                            'comment' => $comptabilityPurchase->getComment()
                        ];
                        $minDate->add(new \DateInterval('P1Y'));
                    }
                }
            }
        }

        return $return;
    }

}
