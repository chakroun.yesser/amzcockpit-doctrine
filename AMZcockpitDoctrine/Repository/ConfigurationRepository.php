<?php

namespace AMZcockpitDoctrine\Repository;

/**
 * ConfigurationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ConfigurationRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return \AMZcockpitDoctrine\Entity\Configuration
     */
    public function getConfiguration() {
        $qb = $this->createQueryBuilder('c');
        $qb->setFirstResult(0);
        $qb->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
