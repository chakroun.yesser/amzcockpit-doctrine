<?php
/**
 * Created by PhpStorm.
 * User: yesser
 * Date: 24/07/18
 * Time: 05:59 م
 */

namespace AMZcockpitDoctrine\Doctrine;

use Doctrine\ORM\Id\AbstractIdGenerator;

class RandomCouponGenerator extends AbstractIdGenerator
{
    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
        $entity_name = $em->getClassMetadata(get_class($entity))->getName();

        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
        $charactersLength = strlen($characters);
        $exist = true;
        $max_attempts = 500;
        $attempt = 0;
        while ($exist) {
            $id = '';
            for ($i = 0; $i < 10; $i++) {
                $id .= $characters[rand(0, $charactersLength - 1)];
            }
            if(!$item = $em->find($entity_name, $id)) {
                return $id;
            }
            $attempt++;
            if ($attempt > $max_attempts) {
                throw new \Exception('RandomIdGenerator worked hardly, but failed to generate unique ID :(');
            }
        }
    }
}